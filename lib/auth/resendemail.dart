import 'dart:convert';
import 'dart:io';

import 'package:buddhist_educenter/auth/login.dart';
import 'package:buddhist_educenter/env.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:sweetalert/sweetalert.dart';

class ResendEmail extends StatefulWidget {
  final String email, password;
  ResendEmail({this.email, this.password});
  @override
  _ResendEmailState createState() => _ResendEmailState();
}

class _ResendEmailState extends State<ResendEmail> {
  bool _isLoading = false;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  void showInSnackBar(String value, {SnackBarAction action}) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(value),
      action: action,
    ));
  }

  resendEmail() async {
    setState(() {
      _isLoading = true;
    });
    try {
      final getToken = await http.post(url('api/resendverif'), body: {
        "email": widget.email,
        "password": widget.password,
      });
      debugPrint(getToken.statusCode.toString());

      var getTokenDecode = json.decode(getToken.body);

      if (getToken.statusCode == 200) {
        SweetAlert.show(context, onPress: (isConfirm) {
          if (isConfirm) {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(
                    builder: (BuildContext context) => LoginPage()),
                (Route<dynamic> route) => false);
          } else {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(
                    builder: (BuildContext context) => LoginPage()),
                (Route<dynamic> route) => false);
          }
          return false;
        }, title: 'Verifikasi Terkirim', style: SweetAlertStyle.success);
      } else if (getToken.statusCode == 401) {
        setState(() {
          _isLoading = false;
        });
        showInSnackBar(getTokenDecode['message']);
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (BuildContext context) => ResendEmail()));
      } else {
        showInSnackBar('Request failed with status: ${getToken.statusCode}');
      }
      // debugPrint(datajson.toString());

    } on SocketException catch (_) {
      setState(() {
        _isLoading = false;
      });
      showInSnackBar('Connection Timed Out');
    } catch (e) {
      debugPrint(e);
      setState(() {
        _isLoading = false;
      });
      // showInSnackBar(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    MediaQueryData mediaQD = MediaQuery.of(context);
    ThemeData theme = Theme.of(context);
    return WillPopScope(
      onWillPop: () => Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
          (Route<dynamic> route) => false),
      child: Scaffold(
        backgroundColor: Colors.white,
        body: _isLoading == false
            ? SafeArea(
                child: Stack(
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: mediaQD.size.width / 1.5,
                          padding: const EdgeInsets.only(top: 60.0),
                          child: Text(
                            'Your account not active yet',
                            textAlign: TextAlign.center,
                            style: GoogleFonts.quattrocentoSans(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        Container(
                          width: mediaQD.size.width / 1.5,
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            'Check your email & click the link to activate your account',
                            textAlign: TextAlign.center,
                            style: GoogleFonts.quattrocentoSans(
                              fontSize: 16,
                            ),
                          ),
                        ),
                        SizedBox(
                          width: mediaQD.size.width / 1.2,
                          child: Image(image: AssetImage('assets/img/email.png'))
                        ),
                        Expanded(
                          child: Container(
                              padding: EdgeInsets.only(top: 23, bottom: 13),
                              child: Align(
                                alignment: Alignment.center,
                                child: InkWell(
                                  onTap: (){
                                    resendEmail();
                                  },
                                  child: Container(
                                    height: 52,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(mediaQD.size.width / 28),
                                      color: theme.primaryColor,
                                    ),
                                    width:
                                        MediaQuery.of(context).size.width / 1.4,
                                    child: Align(
                                        alignment: Alignment.center,
                                        child: Text(
                                          'Resend Email',
                                          style: GoogleFonts.quattrocentoSans(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 20
                                          ),
                                        ))),
                                ),
                              )),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 10.0, right: 10.0),
                      child: Row(
                        children: [
                          IconButton(
                            icon: Icon(
                              Platform.isIOS ? Icons.arrow_back_ios : Icons.arrow_back
                            ),
                            onPressed: () {
                              Navigator.of(context).pop();
                            }
                          )
                        ],
                      ),
                    )
                  ],
                ),
              )
            : Container(
                child: Center(
                    child: CircularProgressIndicator(),
                )
              ),
      ),
    );
  }
}
