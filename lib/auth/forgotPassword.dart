import 'package:buddhist_educenter/auth/login.dart';
import 'package:buddhist_educenter/locale/my_localization.dart';
import 'package:flutter/material.dart';
import 'package:sweetalert/sweetalert.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:io';
import 'package:buddhist_educenter/env.dart';

class ForgotPage extends StatefulWidget {
  @override
  _ForgotPageState createState() => _ForgotPageState();
}

Map<String, String> requestHeaders = Map();

class _ForgotPageState extends State<ForgotPage> {
  final FocusNode myFocusNodeEmail = FocusNode();
  final FocusNode myFocusNodePhone = FocusNode();
  final FocusNode myFocusNodePassword = FocusNode();
  final FocusNode myFocusNodeFullName = FocusNode();
  final FocusNode myFocusNodeConfirmPassword = FocusNode();

  TextEditingController fullNameController = new TextEditingController();
  TextEditingController emailController = new TextEditingController();
  TextEditingController phoneController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  TextEditingController confirmPasswordController = new TextEditingController();

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  // bool _autovalidate = false;
  String msg = '';
  bool _isLoading;

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  void showInSnackBar(String value, {SnackBarAction action}) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(value),
      action: action,
    ));
  }

  void initState() {
    _isLoading = false;
    super.initState();
  }

  _signup() async {
    setState(() {
      _isLoading = true;
    });
    try {
      Map jsonMap = {
        'email': emailController.text,
      };
      final getToken = await http.post(url('api/create'),
          body: json.encode(jsonMap), headers: {'Accept': 'application/json',"Content-Type": "application/json"});

      var getTokenDecode = json.decode(getToken.body);

      if (getTokenDecode['status'] == 'error') {
        showInSnackBar(getTokenDecode['message']);
        msg = getTokenDecode['message'];
        setState(() {
          _isLoading = false;
        });
      } else if (getTokenDecode['status'] == 'success') {
        setState(() {
          _isLoading = false;
        });
        SweetAlert.show(context, onPress: (isConfirm) {
          if (isConfirm) {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(
                    builder: (BuildContext context) => LoginPage()),
                (Route<dynamic> route) => false);
          } else {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(
                    builder: (BuildContext context) => LoginPage()),
                (Route<dynamic> route) => false);
          }
          return false;
        }, title: getTokenDecode['message'], style: SweetAlertStyle.success);
      }

      // debugPrint(datajson.toString());

    } on SocketException catch (_) {
      showInSnackBar('Connection Timed Out');
      setState(() {
        _isLoading = false;
      });
    } catch (e) {
      debugPrint(e.toString());
      setState(() {
        _isLoading = false;
      });
      // showInSnackBar(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    MediaQueryData mediaQD = MediaQuery.of(context);
    ThemeData theme = Theme.of(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: theme.backgroundColor,
        brightness: Brightness.light,
        elevation: 0,
        iconTheme: IconThemeData(color: theme.primaryColor),
      ),
      body: ListView(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(
                left: mediaQD.size.width / 4,
                right: mediaQD.size.width / 4,
                top: 30),
            child: Image.asset('assets/img/logo_bec.png'),
          ),
          Container(
            width: mediaQD.size.width / 1.4,
            padding:
                EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 30),
            child: Column(
              children: [
                Text(
                  MyLocalization.of(context).forgotPasswordTitle,
                  textAlign: TextAlign.center,
                  style: GoogleFonts.quattrocentoSans(
                      fontSize: 24, fontWeight: FontWeight.bold),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      top: 13.0, left: 8.0, right: 8.0),
                  child: Text(
                    MyLocalization.of(context).forgotPasswordDesc,
                    textAlign: TextAlign.center,
                    style: GoogleFonts.quattrocentoSans(fontSize: 12),
                  ),
                ),
              ],
            ),
          ),
          Container(
            child: Align(
              alignment: Alignment.center,
              child: Card(
                elevation: 2.0,
                color: Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius:
                      BorderRadius.circular(mediaQD.size.width / 28),
                ),
                child: Container(
                  width: mediaQD.size.width / 1.4,
                  child: Form(
                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        // Container(
                        //   width: mediaQD.size.width / 1.4,
                        //   height: 1.0,
                        //   color: Colors.grey[400],
                        // ),
                        Padding(
                          padding: EdgeInsets.only(
                              top: 0.0,
                              bottom: 0.0,
                              left: 10.0,
                              right: 10.0),
                          child: TextFormField(
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            focusNode: myFocusNodeEmail,
                            controller: emailController,
                            keyboardType: TextInputType.emailAddress,
                            style: GoogleFonts.quattrocentoSans(
                                fontSize: 12.0, color: Colors.black),
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              // icon: Icon(
                              //   FontAwesomeIcons.envelope,
                              //   color: Colors.black,
                              //   size: 22.0,
                              // ),
                              hintText: MyLocalization.of(context).loginFormEmail,
                              hintStyle: GoogleFonts.quattrocentoSans(
                                  fontSize: 14.0),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
          Container(
              padding: EdgeInsets.only(top: 23, bottom: 13),
              child: Align(
                alignment: Alignment.center,
                child: InkWell(
                  onTap: () async {
                    final formState = _formKey.currentState;
                    if (formState.validate()) {
                      _signup();
                      // if (_image == null) {
                      //   showDialog(
                      //     context: context,
                      //     builder: (BuildContext context) {
                      //       return AlertDialog(
                      //         shape: RoundedRectangleBorder(
                      //             borderRadius:
                      //                 new BorderRadius
                      //                     .circular(10.0)),
                      //         content: new Text(
                      //           'Silahkan Mengisi Foto Profil Anda ',
                      //           textAlign: TextAlign.left,
                      //         ),
                      //         actions: <Widget>[
                      //           FlatButton(
                      //             child: Text("Oke"),
                      //             onPressed: () {
                      //               Navigator.of(context)
                      //                   .pop();
                      //             },
                      //           ),
                      //         ],
                      //       );
                      //     },
                      //   );
                      // } else {
                      //   uploadImage(_image);
                      // }
                    }
                  },
                  // () async {
                  //   if (_isLoading) {
                  //     return null;
                  //   } else {
                  //     _login();
                  //   }
                  // },
                  child: Container(
                      decoration: BoxDecoration(
                        borderRadius:
                            BorderRadius.circular(mediaQD.size.width / 28),
                        color: theme.primaryColor,
                      ),
                      height: 52,
                      width: mediaQD.size.width / 1.4,
                      child: Align(
                          alignment: Alignment.center,
                          child: Text(
                            _isLoading
                                ? MyLocalization.of(context).loginPleasewait
                                : MyLocalization.of(context).forgotPasswordButton,
                            style: GoogleFonts.quattrocentoSans(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 20),
                          ))),
                ),
              )),
        ],
      ),
    );
  }
}
