import 'package:buddhist_educenter/auth/forgotPassword.dart';
import 'package:buddhist_educenter/auth/resendemail.dart';
import 'package:buddhist_educenter/auth/signup.dart';
import 'package:buddhist_educenter/locale/my_localization.dart';
import 'package:buddhist_educenter/pages/home.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:io';
import 'package:buddhist_educenter/env.dart';
import 'package:buddhist_educenter/storage/storage.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

Map<String, String> requestHeaders = Map();

class _LoginPageState extends State<LoginPage> {
  final FocusNode myFocusNodeEmailLogin = FocusNode();
  final FocusNode myFocusNodePasswordLogin = FocusNode();

  TextEditingController loginEmailController = new TextEditingController();
  TextEditingController loginPasswordController = new TextEditingController();

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  // bool _autovalidate = false;
  String msg = '';
  bool _isLoading;

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  void showInSnackBar(String value, {SnackBarAction action}) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(value),
      action: action,
    ));
  }

  bool _obscureTextLogin = true;
  void _toggleLogin() {
    setState(() {
      _obscureTextLogin = !_obscureTextLogin;
    });
  }

  void initState() {
    _isLoading = false;
    super.initState();
  }

  _login() async {
    setState(() {
      _isLoading = true;
    });
    try {
      final getToken = await http.post(url('api/login'), body: {
        "email": loginEmailController.text,
        "password": loginPasswordController.text,
      });
      debugPrint(getToken.body);
      debugPrint(getToken.statusCode.toString());
      var getTokenDecode = json.decode(getToken.body);

      if (getToken.statusCode == 200) {
        if (getTokenDecode['error'] == 'Unautorized') {
          setState(() {
            _isLoading = false;
          });
          showInSnackBar(getTokenDecode['message']);
          msg = getTokenDecode['message'];
        } else if (getTokenDecode['status'] == 'success') {
          DataStore().setDataString('access_token', getTokenDecode['token']);
          DataStore().setDataString('token_type', getTokenDecode['token_type']);
        }
        dynamic tokenType = getTokenDecode['token_type'];
        dynamic accessToken = getTokenDecode['token'];
        requestHeaders['Accept'] = 'application/json';
        requestHeaders['Authorization'] = '$tokenType $accessToken';

        try {
          final getUser =
              await http.get(url("api/detail"), headers: requestHeaders);

          // debugPrint('getUser ' + getUser.body + getTokenDecode['token']);

          if (getUser.statusCode == 200) {
            dynamic datauser = json.decode(getUser.body);

            DataStore store = new DataStore();

            store.setDataString("id", datauser['success']['id'].toString());
            store.setDataString("name", datauser['success']['name']);
            store.setDataString("email", datauser['success']['email']);
            store.setDataString("phone", datauser['success']['phone']);
            store.setDataString(
                "photo", datauser['success']['photo'].toString());
            store.setDataString(
                "userPointer", datauser['success']['row_pointer']);
            store.setDataString("access_token", getTokenDecode['token']);
            store.setDataString("token_type", getTokenDecode['token_type']);

            // debugPrint('statement else is true');
            // debugPrint(datauser);
            setState(() {
              _isLoading = false;
              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => HomePage()));
            });
          } else {
            setState(() {
              _isLoading = false;
            });
            showInSnackBar('Request failed with status: ${getUser.statusCode}');
          }
        } on SocketException catch (_) {
          setState(() {
            _isLoading = false;
          });
          showInSnackBar('Connection Timed Out');
        } catch (e) {
          setState(() {
            _isLoading = false;
          });
          debugPrint(e);
          // showInSnackBar(e);
        }
      } else if (getToken.statusCode == 401) {
        setState(() {
          _isLoading = false;
        });
        // showInSnackBar(getTokenDecode['message']);
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (BuildContext context) => ResendEmail(
                      email: loginEmailController.text,
                      password: loginPasswordController.text,
                    )));
      } else if (getToken.statusCode == 402) {
        setState(() {
          _isLoading = false;
        });
        showInSnackBar(getTokenDecode['message']);
      } else {
        showInSnackBar('Request failed with status: ${getToken.statusCode}');
      }
      // debugPrint(datajson.toString());

    } on SocketException catch (_) {
      setState(() {
        _isLoading = false;
      });
      showInSnackBar('Connection Timed Out');
    } catch (e) {
      debugPrint(e);
      setState(() {
        _isLoading = false;
      });
      // showInSnackBar(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    MediaQueryData mediaQD = MediaQuery.of(context);
    ThemeData theme = Theme.of(context);
    return Scaffold(
      key: _scaffoldKey,
      body: Container(
        child: ListView(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(
                  left: mediaQD.size.width / 5,
                  right: mediaQD.size.width / 5,
                  top: mediaQD.size.height / 6),
              child: Image.asset('assets/img/logo_bec.png'),
            ),
            Container(
              height: mediaQD.size.height / 9,
            ),
            Container(
              child: Align(
                alignment: Alignment.center,
                child: Card(
                  elevation: 2.0,
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius:
                        BorderRadius.circular(mediaQD.size.width / 28),
                  ),
                  child: Container(
                    width: mediaQD.size.width / 1.4,
                    child: Form(
                      key: _formKey,
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(
                                top: 0.0, bottom: 0.0, left: 10.0, right: 10.0),
                            child: TextField(
                              focusNode: myFocusNodeEmailLogin,
                              controller: loginEmailController,
                              keyboardType: TextInputType.emailAddress,
                              style: GoogleFonts.quattrocentoSans(
                                  fontSize: 16.0, color: Colors.black),
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                // icon: Icon(
                                //   FontAwesomeIcons.envelope,
                                //   color: Colors.black,
                                //   size: 22.0,
                                // ),
                                hintText:
                                    MyLocalization.of(context).loginFormEmail,
                                hintStyle: GoogleFonts.quattrocentoSans(
                                    fontSize: 14.0),
                              ),
                            ),
                          ),
                          Container(
                            width: mediaQD.size.width / 1.4,
                            height: 1.0,
                            color: Colors.grey[400],
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                                top: 0.0, bottom: 0.0, left: 10.0, right: 10.0),
                            child: TextField(
                              focusNode: myFocusNodePasswordLogin,
                              controller: loginPasswordController,
                              obscureText: _obscureTextLogin,
                              style: GoogleFonts.quattrocentoSans(
                                  fontSize: 16.0, color: Colors.black),
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                // icon: Icon(
                                //   FontAwesomeIcons.lock,
                                //   size: 22.0,
                                //   color: Colors.black,
                                // ),
                                hintText: MyLocalization.of(context)
                                    .loginFormPassword,
                                hintStyle: GoogleFonts.quattrocentoSans(
                                    fontSize: 14.0),
                                suffixIcon: GestureDetector(
                                  onTap: _toggleLogin,
                                  child: Icon(
                                    _obscureTextLogin
                                        ? FontAwesomeIcons.eye
                                        : FontAwesomeIcons.eyeSlash,
                                    size: 16.0,
                                    color: Colors.black,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
            InkWell(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => ForgotPage()));
              },
              child: Container(
                  child: Align(
                alignment: Alignment.center,
                child: Container(
                  width: mediaQD.size.width / 1.4,
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: Text(
                      // 'Forgot Password?',
                      MyLocalization.of(context).loginForgotPassword+' ?',
                      style: GoogleFonts.quattrocentoSans(
                          color: theme.accentColor, fontSize: 13),
                    ),
                  ),
                ),
              )),
            ),
            Container(
                padding: EdgeInsets.only(top: 23, bottom: 13),
                child: Align(
                  alignment: Alignment.center,
                  child: InkWell(
                    onTap: () async {
                      if (_isLoading) {
                        return null;
                      } else {
                        _login();
                      }
                    },
                    child: Container(
                        decoration: BoxDecoration(
                          borderRadius:
                              BorderRadius.circular(mediaQD.size.width / 28),
                          color: theme.primaryColor,
                        ),
                        height: 52,
                        width: mediaQD.size.width / 1.4,
                        child: Align(
                            alignment: Alignment.center,
                            child: Text(
                              _isLoading ? MyLocalization.of(context).loginPleasewait : MyLocalization.of(context).loginTextSignIn,
                              style: GoogleFonts.quattrocentoSans(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20),
                            ))),
                  ),
                )),
            Container(
              width: mediaQD.size.width / 1.4,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    MyLocalization.of(context).loginSignup + ' ? ',
                    style: GoogleFonts.quattrocentoSans(
                        color: Colors.black54, fontSize: 16),
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) => SignUpPage()));
                    },
                    child: Text(
                      MyLocalization.of(context).loginTextSignup,
                      style: GoogleFonts.quattrocentoSans(
                          color: theme.accentColor,
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
