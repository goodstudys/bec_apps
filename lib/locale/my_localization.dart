import 'package:buddhist_educenter/l10n/messages_all.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class MyLocalization {
  static Future<MyLocalization> load(Locale locale) {
    final String name =
        locale.countryCode.isEmpty ? locale.languageCode : locale.toString();
    final String localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      return MyLocalization();
    });
  }

  static MyLocalization of(BuildContext context) {
    return Localizations.of<MyLocalization>(context, MyLocalization);
  }

  String get closeAppQuestion {
    return Intl.message(
      'Are you sure you want to close this application?',
      name: 'closeAppQuestion',
      desc: 'pertanyaan close app',
    );
  }

  String get yes {
    return Intl.message(
      'Yes',
      name: 'yes',
      desc: 'yes',
    );
  }

  String get no {
    return Intl.message(
      'No',
      name: 'no',
      desc: 'no',
    );
  }

  String get loginSignup {
    return Intl.message(
      'Don\'t have an account',
      name: 'loginSignup',
      desc: 'tidak mempunyai akun',
    );
  }

  String get loginFormEmail {
    return Intl.message('Email Address',
        name: 'loginFormEmail', desc: 'Form Email');
  }

  String get loginFormPassword {
    return Intl.message('Password',
        name: 'loginFormPassword', desc: 'Form Password');
  }

  String get loginForgotPassword {
    return Intl.message('Forgot Password',
        name: 'loginForgotPassword', desc: 'text forgot password');
  }

  String get loginTextSignup {
    return Intl.message('Sign Up',
        name: 'loginTextSignup', desc: 'Text Signup');
  }

  String get loginTextSignIn {
    return Intl.message('Sign In',
        name: 'loginTextSignIn', desc: 'Text Sign In');
  }

  String get loginPleasewait {
    return Intl.message('Please Wait',
        name: 'loginPleasewait', desc: 'Text Please Wait');
  }

  String get logoutQuestion {
    return Intl.message('Are you sure you want to log out?',
        name: 'logoutQuestion', desc: 'logout question');
  }

  String get forgotPasswordTitle {
    return Intl.message('Forgot Your Password',
        name: 'forgotPasswordTitle', desc: 'Text title in Forgot Password');
  }

  String get forgotPasswordDesc {
    return Intl.message("Don't worry",
        name: 'forgotPasswordDesc',
        desc: 'Text Description in Forgot Password');
  }

  String get forgotPasswordButton {
    return Intl.message("Reset Password",
        name: 'forgotPasswordButton', desc: 'Text button in Forgot Password');
  }

  String get signupTitle {
    return Intl.message("Signup Title",
        name: 'signupTitle', desc: 'Text title in Signup');
  }

  String get signupDesc1 {
    return Intl.message("Signup Desc1",
        name: 'signupDesc1', desc: 'Text desc in Signup1');
  }

  String get signupDesc2 {
    return Intl.message("Signup Desc2",
        name: 'signupDesc2', desc: 'Text desc in Signup2');
  }

  String get signupDesc3 {
    return Intl.message("Signup Desc3",
        name: 'signupDesc3', desc: 'Text desc in Signup3');
  }

  String get signupFormName {
    return Intl.message("Name",
        name: 'signupFormName', desc: 'hint text sign up name');
  }

  String get signupFormPhone {
    return Intl.message("Phone Number",
        name: 'signupFormPhone', desc: 'hint text sign up phone Number');
  }

  String get signupFormConfirmPassword {
    return Intl.message("Confirm Password",
        name: 'signupFormConfirmPassword',
        desc: 'hint text sign up confirm password');
  }

  String get homeStartReadingbook {
    return Intl.message("Start reading a book",
        name: 'homeStartReadingbook', desc: 'home text start reading a book');
  }

  String get homeEvents {
    return Intl.message("Events", name: 'homeEvents', desc: 'home text Events');
  }

  String get homeAll {
    return Intl.message("All", name: 'homeAll', desc: 'home text All');
  }

  String get homeRecomended {
    return Intl.message("Recomended",
        name: 'homeRecomended', desc: 'home text Recomended');
  }

  String get homePopularBooks {
    return Intl.message("Popular Books",
        name: 'homePopularBooks', desc: 'home text Popular Books');
  }

  String get homeMayAlsoLike {
    return Intl.message("You may also like",
        name: 'homeMayAlsoLike', desc: 'home text You may also like');
  }

  String get adsDetailMore {
    return Intl.message("More Info",
        name: 'adsDetailMore', desc: 'more detail');
  }

  String get bottomNavigationBooks {
    return Intl.message("Books",
        name: 'bottomNavigationBooks', desc: 'Bottom navigation Books');
  }

  String get bottomNavigationRead {
    return Intl.message("Read",
        name: 'bottomNavigationRead', desc: 'Bottom navigation Read');
  }

  String get bottomNavigationAccount {
    return Intl.message("Account",
        name: 'bottomNavigationAccount', desc: 'Bottom navigation Account');
  }

  String get bottomNavigationDonate {
    return Intl.message("Donate",
        name: 'bottomNavigationDonate', desc: 'Bottom navigation Donate');
  }

  String get profileChangePassword {
    return Intl.message("Change Password",
        name: 'profileChangePassword', desc: 'profile Change Password');
  }

  String get profileSelectLanguage {
    return Intl.message("Select Language",
        name: 'profileSelectLanguage', desc: 'Profile Select Language');
  }

  //   String get profileTerms {
  //   return Intl.message("Donate",
  //       name: 'profileTerms', desc: 'Profile Terms');
  // }
  String get profilePrivacy {
    return Intl.message("Privacy Policy",
        name: 'profilePrivacy', desc: 'Profile Privacy Policy');
  }

  String get profileSignout {
    return Intl.message("Sign out",
        name: 'profileSignout', desc: 'Profile Sign Out');
  }

  String get appbarContinue {
    return Intl.message("Continue Reading",
        name: 'appbarContinue', desc: 'Appbar Continue Reading');
  }

  String get donateAppbar {
    return Intl.message("Donate",
        name: 'donateAppbar', desc: 'Appbar Donate');
  }

  String get donateTitle {
    return Intl.message("Thank you for visiting our Donate page",
        name: 'donateTitle', desc: 'Title Donate');
  }

  String get donateDesc {
    return Intl.message("Any contribution, big or small, towards helping us accomplish our goals of the Dharma, is greatly appreciated.",
        name: 'donateDesc', desc: 'Description Donate');
  }

  String get donateAmount {
    return Intl.message("Select Amount",
        name: 'donateAmount', desc: 'Amount Donate');
  }

  String get donateTransfer {
    return Intl.message("Bank Transfer",
        name: 'donateTransfer', desc: 'Amount Transfer');
  }

  String get textSearch {
    return Intl.message("Search", name: 'textSearch', desc: 'Text Search');
  }

  String get textTerms {
    return Intl.message("Terms & Conditions",
        name: 'textTerms', desc: 'Text Terms & conditions');
  }

  String get textConfirm {
    return Intl.message("Confirm", name: "textConfirm", desc: "text Confirm");
  }

  String get textChooseMedia {
    return Intl.message("Please choose media to select", name: "textChooseMedia", desc: "text Choose Media");
  }
  String get textFromGallery {
    return Intl.message("From Gallery",name: "textFromGallery",desc: "text From Gallery");
  }
    String get textFromCamera {
    return Intl.message("From Camera",name: "textFromCamera",desc: "text From Camera");
  }
}
