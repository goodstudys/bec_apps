class LanguageModel {
  bool isSelected;
  final String text, locale1, locale2;

  LanguageModel(this.isSelected, this.text, this.locale1, this.locale2);
}

List<LanguageModel> listRadio = [
  LanguageModel(false, 'Bahasa Indonesia', 'id', 'ID'),
  LanguageModel(true, 'English', 'en', 'US'),
  LanguageModel(false, '中文', 'cn', 'CN')
];
