class AdsModel {
  int id;
  String title;
  String descShort;
  String descFull;
  String image;
  String link;
  String street;
  String rowPointer;

  AdsModel({
    this.id,
    this.title,
    this.descShort,
    this.descFull,
    this.image,
    this.link,
    this.street,
    this.rowPointer,
  });

  AdsModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    descShort = json['description_short'];
    descFull = json['description_full'];
    image = json['image'];
    link = json['link'];
    street = json['street'];
    rowPointer = json['row_pointer'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['description_short'] = this.descShort;
    data['description_full'] = this.descFull;
    data['image'] = this.image;
    data['link'] = this.link;
    data['street'] = this.street;
    data['row_pointer'] = this.rowPointer;
    return data;
  }
}