class BookModel {
  final int id;
  final String img;
  final String title;
  final String author;
  final String booklink;
  final String description;
  final String pointer;

  BookModel({this.id, this.img, this.title, this.author, this.booklink, this.description,this.pointer});
}

// List<BookModel> listBookModel = [
//   BookModel(
//     id: 1,
//     img: "assets/img/book1.jpg",
//     title: "The Four Noble Truths",
//     booklink: "http://www.buddhanet.net/pdf_file/4nobltru.pdf",
//     description:
//         "The Four Noble Truths are the central Teaching of the Buddha. This booklet was compiled and edited from talks given by Venerable Ajahn Sumedho on the teaching of the Buddha: that the unhappiness of humanity can be overcome through spiritual means. The teaching is conveyed through the Buddha’s Four Noble Truths, first expounded in 528 BC in the Deer Park at Sarnath near Varanasi, India and kept alive in the Buddhist world ever since.",
//   ),
//   BookModel(
//     id: 2,
//     img: "assets/img/book2.jpg",
//     title: "Intuitive Awareness",
//     booklink: "http://www.buddhanet.net/pdf_file/intuitive-awareness.pdf",
//     description:
//         "This book is a small sample of the talks that Ajahn Sumedho offered during the winter retreat of 2001. The aim of the editors in compiling this book has been explicitly to maintain the style and spirit of the spoken word. As Ajahn Sumedho himself commented, \"The book is meant to be suggestions of ways to investigate conscious experience. It\'s not meant to be a didactic treatise on Pali Buddhism.\"",
//   ),
//   BookModel(
//     id: 3,
//     img: "assets/img/book3.jpg",
//     title: "The Eightfold Path for the Householdere",
//     booklink: "http://www.buddhanet.net/pdf_file/ritepath.pdf",
//     description:
//         "This text is a transcript of teachings given by Jack Kornfeld on the Eightfold Path. These teachings are aimed at the householder. Each part of the Eightfold Path is explained in a separate chapter. The tone of the teaching is contemporary and non-technical. The universality and relevance of the Buddha's teaching are illustrated by numerous quotations from more recent luminaries. There are also some useful exercises which enable the reader to experience the truth of these teachings.",
//   ),
//   BookModel(
//     id: 4,
//     img: "assets/img/book4.jpg",
//     title: "The Buddha, His Life and Teachings ",
//     booklink: "http://www.buddhanet.net/pdf_file/lifebuddha.pdf",
//     description:
//         "This is a comprehensive and authentic book on the Buddha and his Teachings by Piyadassi. The author, Venerable Mahathera Piyadassi is one of the world's most eminent Buddhist monks, a highly revered teacher of great renown, a indefatigable worker for the Buddha Dhamma. \"The ages roll by and the Buddha seems not so far away after all; his voice whispers in our ears and tells us not to run away from the struggle but, calm-eyed, to face it, and to see in life ever greater opportunities for growth and advancement\".",
//   ),
//   BookModel(
//     id: 5,
//     img: "assets/img/book5.jpg",
//     title: "Reading the Mind",
//     booklink: "http://www.buddhanet.net/pdf_file/mindread.pdf",
//     description:
//         "These are insightful teachings by a Lay Thai women teacher, Kee Nanayon (1901-1978); who established a Dhamma centre, Khao-Suan-Luang in 1945. Upasika Kee attracted Dhamma students, and residents came to include both female lay devotees and white-robed nuns. These Dhamma talks were mainly given to the women who stayed at her centre to practice meditation. After listening with calm and centred mind, they would all sit in meditation together.",
//   ),
//   BookModel(
//     id: 6,
//     img: "assets/img/book6.jpg",
//     title: "Buddhism as a Religion",
//     booklink: "http://www.buddhanet.net/pdf_file/budrelig.pdf",
//     description:
//         "The contents of this popular publication are a simple exposition of Buddhism as a modern way of life. This highly qualified Sri Lankan Buddhist scholar has a special gift of interpreting the Buddha's Teachings for people from every walk of life. His whole approach to the exposition of the Dhamma is governed by his deep concern for giving the ancient teachings a contemporary relevance, and has a meaning that cuts across the boundaries of time, space, race, culture and even religious beliefs.",
//   ),
//   BookModel(
//     id: 7,
//     img: "assets/img/book7.jpg",
//     title: "The Buddhist Way",
//     booklink: "http://www.buddhanet.net/pdf_file/buddhistway.pdf",
//     description:
//         "Dr K. Sri Dhammananda explains some Buddhist cultural practices: Going for Refuges, Religious Rites, Alms Giving, Marriage, Buddhist Education and Cultural Practices, Images, Holy Water, Holy Thread, Talismans and Amulets, Blessing Services for Children, Death, Post Mortem, Funerals, Burial and Cremation, Disposal of the Ashes, Period of Mourning, Post-Funeral Rites and Memorial Services.",
//   ),
//   BookModel(
//     id: 8,
//     img: "assets/img/book8.jpg",
//     title: "Handbook For Mankind",
//     booklink: "http://www.buddhanet.net/pdf_file/buddasa.pdf",
//     description:
//         "The Principles of Buddhism explained by Buddhadasa, Bhikkhu. As a guide for newcomers to the Buddha Dhamma (the Truth which the Buddha awakened to and subsequently taught), this book is an invaluable guide. In it are contained the essential teachings of Buddhism. The Handbook is especially useful for those who approach the Buddha's teaching not as a subject for scholarly study but as a means to understand and ennoble their lives. It includes chapters on 'Looking at Buddhism' and the 'True Nature of Things'.",
//   ),
// ];
