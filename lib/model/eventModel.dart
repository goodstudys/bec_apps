class EventModel {
  int id;
  String title;
  String description;
  String image;
  String location;
  String rowPointer;

  EventModel({
    this.id,
    this.title,
    this.description,
    this.image,
    this.rowPointer,
  });

  EventModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    description = json['description'];
    image = json['image'];
    location = json['location'];
    rowPointer = json['row_pointer'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['description'] = this.description;
    data['image'] = this.image;
    data['location'] = this.location;
    data['row_pointer'] = this.rowPointer;
    return data;
  }
}