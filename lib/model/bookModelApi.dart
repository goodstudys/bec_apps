class BookModelApi {
  int id;
  String title;
  String thumbnail;
  String description;
  String author;
  String book;
  String rowPointer;


  BookModelApi(
      {this.id,
      this.title,
      this.thumbnail,
      this.description,
      this.author,
      this.book,
      this.rowPointer,});

  BookModelApi.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    thumbnail = json['thumbnail'];
    description = json['description'];
    author = json['author'];
    book = json['book'];
    rowPointer = json['row_pointer'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['thumbnail'] = this.thumbnail;
    data['description'] = this.description;
    data['author'] = this.author;
    data['book'] = this.book;
    data['row_pointer'] = this.rowPointer;
    return data;
  }
}