class ReadingBookModel {
   int id;
   String img;
   String title;
   String author;
   String booklink;
   String description;
   String rowPointer;
   int pageRead;
   int totalPage;

  ReadingBookModel({this.id, this.img, this.title, this.author, this.booklink, this.description, this.pageRead ,this.totalPage, this.rowPointer});
    ReadingBookModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    author = json['author'];
    img = json['img'];
    description = json['description'];
    pageRead = json['pageRead'];
    totalPage = json['totalPage'];
    booklink = json['booklink'];
    rowPointer = json['row_pointer'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['author'] = this.author;
    data['img'] = this.img;
    data['description'] = this.description;
    data['pageRead'] = this.pageRead;
    data['totalPage'] = this.totalPage;
    data['booklink'] = this.booklink;
    data['row_pointer'] = this.rowPointer;
    return data;
  }
}