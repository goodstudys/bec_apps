// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a id locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'id';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "closeAppQuestion" : MessageLookupByLibrary.simpleMessage("Apakah anda ingin keluar dari aplikasi ini"),
    "yes" : MessageLookupByLibrary.simpleMessage("Ya"),
    "no" : MessageLookupByLibrary.simpleMessage("Tidak"),
    "loginForgotPassword" : MessageLookupByLibrary.simpleMessage("Tidak ingat kata sandi"),
    "loginFormEmail" : MessageLookupByLibrary.simpleMessage("Alamat email"),
    "loginFormPassword" : MessageLookupByLibrary.simpleMessage("Kata sandi"),
    "loginPleasewait" : MessageLookupByLibrary.simpleMessage("Mohon Tunggu"),
    "loginSignup" : MessageLookupByLibrary.simpleMessage("Tidak punya akun"),
    "loginTextSignIn" : MessageLookupByLibrary.simpleMessage("Masuk"),
    "loginTextSignup" : MessageLookupByLibrary.simpleMessage("Daftar"),
    "logoutQuestion" : MessageLookupByLibrary.simpleMessage("Apakah anda yakin untuk keluar dari akun Anda?"),
    "forgotPasswordTitle" : MessageLookupByLibrary.simpleMessage("Lupa Kata Sandi Anda"),
    "forgotPasswordDesc" : MessageLookupByLibrary.simpleMessage("Jangan khawatir! Cukup isi email Anda dan kami akan mengirimkan tautan untuk mengatur ulang kata sandi Anda"),
    "forgotPasswordButton" : MessageLookupByLibrary.simpleMessage("Setel ulang"),
    "signupTitle" : MessageLookupByLibrary.simpleMessage("Daftar untuk mengakses buku kami"),
    "signupDesc1" : MessageLookupByLibrary.simpleMessage("Dengan mengetuk \"Buat Akun\" Anda"),
    "signupDesc2" : MessageLookupByLibrary.simpleMessage("menyetujui"),
    "signupDesc3" : MessageLookupByLibrary.simpleMessage("Syarat & Ketentuan"),
    "signupFormName" : MessageLookupByLibrary.simpleMessage("Nama"),
    "signupFormPhone" : MessageLookupByLibrary.simpleMessage("Nomor Telepon"),
    "signupFormConfirmPassword" : MessageLookupByLibrary.simpleMessage("Konfirmasi Sandi"),
    "homeStartReadingbook" : MessageLookupByLibrary.simpleMessage("Mulailah membaca buku"),
    "homeEvents" : MessageLookupByLibrary.simpleMessage("Acara"),
    "homeAll" : MessageLookupByLibrary.simpleMessage("Semua"),
    "homeRecomended" : MessageLookupByLibrary.simpleMessage("Direkomendasikan"),
    "homePopularBooks" : MessageLookupByLibrary.simpleMessage("Buku Populer"),
    "homeMayAlsoLike" : MessageLookupByLibrary.simpleMessage("Anda mungkin juga suka"),
    "adsDetailMore" : MessageLookupByLibrary.simpleMessage("Selengkapnya"),
    "bottomNavigationBooks" : MessageLookupByLibrary.simpleMessage("Buku"),
    "bottomNavigationRead" : MessageLookupByLibrary.simpleMessage("Baca"),
    "bottomNavigationAccount" : MessageLookupByLibrary.simpleMessage("Akun"),
    "bottomNavigationDonate" : MessageLookupByLibrary.simpleMessage("Donasi"),
    "profileChangePassword" : MessageLookupByLibrary.simpleMessage("Ganti kata sandi"),
    "profileSelectLanguage" : MessageLookupByLibrary.simpleMessage("Pilih Bahasa"),
    "profilePrivacy" : MessageLookupByLibrary.simpleMessage("Kebijakan Pribadi"),
    "profileSignout" : MessageLookupByLibrary.simpleMessage("Keluar"),
    "appbarContinue" : MessageLookupByLibrary.simpleMessage("Lanjut membaca"),
    "donateAppbar" : MessageLookupByLibrary.simpleMessage("Donasi"),
    "donateTitle" : MessageLookupByLibrary.simpleMessage("Terima kasih telah mengunjungi halaman donasi"),
    "donateDesc" : MessageLookupByLibrary.simpleMessage("Sebagai organisasi nirlaba dan independen, Buddhist Education Centre Surabaya bergantung pada kemurahan hati anggota dan donatir kami. Kami sangat menghargai setiap kontribusi baik itu besar atau kecil, untuk membantu mencapai tujuan Dharma kita"),
    "donateAmount" : MessageLookupByLibrary.simpleMessage("Pilih Nominal"),
    "donateTransfer" : MessageLookupByLibrary.simpleMessage("Transfer ke Bank"),
    "textSearch" : MessageLookupByLibrary.simpleMessage("Cari"),
    "textTerms" : MessageLookupByLibrary.simpleMessage("Syarat & Ketentuan"),
    "textConfirm" : MessageLookupByLibrary.simpleMessage("Konfirmasi"),
    "textChooseMedia" : MessageLookupByLibrary.simpleMessage("Pilih media untuk dipilih"),
    "textFromGallery" : MessageLookupByLibrary.simpleMessage("Dari Galeri"),
    "textFromCamera" : MessageLookupByLibrary.simpleMessage("Dari Kamera"),
  };
}
