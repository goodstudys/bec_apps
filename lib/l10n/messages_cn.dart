// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a cn locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'cn';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "closeAppQuestion" : MessageLookupByLibrary.simpleMessage("你想退出這個應用程序?"),
    "yes" : MessageLookupByLibrary.simpleMessage("是"),
    "no" : MessageLookupByLibrary.simpleMessage("拒絕"),
    "loginForgotPassword" : MessageLookupByLibrary.simpleMessage("忘记密码"),
    "loginFormEmail" : MessageLookupByLibrary.simpleMessage("电子邮件地址"),
    "loginFormPassword" : MessageLookupByLibrary.simpleMessage("密码"),
    "loginPleasewait" : MessageLookupByLibrary.simpleMessage("请耐心等待"),
    "loginSignup" : MessageLookupByLibrary.simpleMessage("没有帐号"),
    "loginTextSignIn" : MessageLookupByLibrary.simpleMessage("登入"),
    "loginTextSignup" : MessageLookupByLibrary.simpleMessage("注册"),
    "logoutQuestion" : MessageLookupByLibrary.simpleMessage("你確定要註銷?"),
    "forgotPasswordTitle" : MessageLookupByLibrary.simpleMessage("忘记密码了吗"),
    "forgotPasswordDesc" : MessageLookupByLibrary.simpleMessage("不用担心！只需填写您的电子邮件，我们将向您发送一个链接以重置密码"),
    "forgotPasswordButton" : MessageLookupByLibrary.simpleMessage("重设密码"),
    "signupTitle" : MessageLookupByLibrary.simpleMessage("注册以访问我们的书"),
    "signupDesc1" : MessageLookupByLibrary.simpleMessage("点击“创建帐户”即表示您同意"),
    "signupDesc2" : MessageLookupByLibrary.simpleMessage("到"),
    "signupDesc3" : MessageLookupByLibrary.simpleMessage("条款及细则"),
    "signupFormName" : MessageLookupByLibrary.simpleMessage("名称"),
    "signupFormPhone" : MessageLookupByLibrary.simpleMessage("电话号码"),
    "signupFormConfirmPassword" : MessageLookupByLibrary.simpleMessage("确认密码"),
    "homeStartReadingbook" : MessageLookupByLibrary.simpleMessage("開始讀書"),
    "homeEvents" : MessageLookupByLibrary.simpleMessage("活動"),
    "homeAll" : MessageLookupByLibrary.simpleMessage("全部"),
    "homeRecomended" : MessageLookupByLibrary.simpleMessage("推薦"),
    "homePopularBooks" : MessageLookupByLibrary.simpleMessage("熱門"),
    "homeMayAlsoLike" : MessageLookupByLibrary.simpleMessage("您也許會喜歡"),
    "adsDetailMore" : MessageLookupByLibrary.simpleMessage("閱讀更多"),
    "bottomNavigationBooks" : MessageLookupByLibrary.simpleMessage("書"),
    "bottomNavigationRead" : MessageLookupByLibrary.simpleMessage("已讀"),
    "bottomNavigationAccount" : MessageLookupByLibrary.simpleMessage("帳戶"),
    "bottomNavigationDonate" : MessageLookupByLibrary.simpleMessage("捐款"),
    "profileChangePassword" : MessageLookupByLibrary.simpleMessage("更改密码"),
    "profileSelectLanguage" : MessageLookupByLibrary.simpleMessage("选择语言"),
    "profilePrivacy" : MessageLookupByLibrary.simpleMessage("隐私政策"),
    "profileSignout" : MessageLookupByLibrary.simpleMessage("登出"),
    "appbarContinue" : MessageLookupByLibrary.simpleMessage("继续阅读"),
    "donateAppbar" : MessageLookupByLibrary.simpleMessage("捐贈"),
    "donateTitle" : MessageLookupByLibrary.simpleMessage("感謝您訪問我們的捐贈頁面"),
    "donateDesc" : MessageLookupByLibrary.simpleMessage("作為一個獨立的非盈利佛教組織，泗水佛教教育中心依靠我們會員和捐助者的慷慨解囊。我們非常感謝為幫助我們實現佛法目標而做出的任何大小貢獻。"),
    "donateAmount" : MessageLookupByLibrary.simpleMessage("選擇金額"),
    "donateTransfer" : MessageLookupByLibrary.simpleMessage("銀行轉帳"),
    "textSearch" : MessageLookupByLibrary.simpleMessage("搜索"),
    "textTerms" : MessageLookupByLibrary.simpleMessage("条款及细则"),
    "textConfirm" : MessageLookupByLibrary.simpleMessage("确认"),
    "textChooseMedia" : MessageLookupByLibrary.simpleMessage("请选择媒体进行选择"),
    "textFromGallery" : MessageLookupByLibrary.simpleMessage("从画廊"),
    "textFromCamera" : MessageLookupByLibrary.simpleMessage("从相机"),
  };
}
