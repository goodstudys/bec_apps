// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a messages locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'messages';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "closeAppQuestion" : MessageLookupByLibrary.simpleMessage("Are you sure you want to close this application?"),
    "yes" : MessageLookupByLibrary.simpleMessage("Yes"),
    "no" : MessageLookupByLibrary.simpleMessage("No"),
    "loginForgotPassword" : MessageLookupByLibrary.simpleMessage("Forgot Password"),
    "loginFormEmail" : MessageLookupByLibrary.simpleMessage("Email Address"),
    "loginFormPassword" : MessageLookupByLibrary.simpleMessage("Password"),
    "loginPleasewait" : MessageLookupByLibrary.simpleMessage("Please Wait"),
    "loginSignup" : MessageLookupByLibrary.simpleMessage("Don\'t have an account"),
    "loginTextSignIn" : MessageLookupByLibrary.simpleMessage("Sign In"),
    "loginTextSignup" : MessageLookupByLibrary.simpleMessage("Sign Up"),
    "logoutQuestion" : MessageLookupByLibrary.simpleMessage("Are you sure you want to logout ?"),
    "forgotPasswordTitle" : MessageLookupByLibrary.simpleMessage("Forgot Your Passsword"),
    "forgotPasswordDesc" : MessageLookupByLibrary.simpleMessage("Don't worry"),
    "forgotPasswordButton" : MessageLookupByLibrary.simpleMessage("Reset Password"),
    "signupTitle" : MessageLookupByLibrary.simpleMessage("Signup Title"),
    "signupDesc1" : MessageLookupByLibrary.simpleMessage("Signup Desc1"),
    "signupDesc2" : MessageLookupByLibrary.simpleMessage("Signup Desc2"),
    "signupDesc3" : MessageLookupByLibrary.simpleMessage("Signup Desc3"),
    "signupFormName" : MessageLookupByLibrary.simpleMessage("Name"),
    "signupFormPhone" : MessageLookupByLibrary.simpleMessage("Phone Number"),
    "signupFormConfirmPassword" : MessageLookupByLibrary.simpleMessage("Confirm Password"),
    "homeStartReadingbook" : MessageLookupByLibrary.simpleMessage("Start reading a book"),
    "homeEvents" : MessageLookupByLibrary.simpleMessage("Events"),
    "homeAll" : MessageLookupByLibrary.simpleMessage("All"),
    "homeRecomended" : MessageLookupByLibrary.simpleMessage("Recomended"),
    "homePopularBooks" : MessageLookupByLibrary.simpleMessage("Popular Books"),
    "homeMayAlsoLike" : MessageLookupByLibrary.simpleMessage("You may also like"),
    "adsDetailMore" : MessageLookupByLibrary.simpleMessage("More Info"),
    "bottomNavigationBooks" : MessageLookupByLibrary.simpleMessage("Books"),
    "bottomNavigationRead" : MessageLookupByLibrary.simpleMessage("Read"),
    "bottomNavigationAccount" : MessageLookupByLibrary.simpleMessage("Account"),
    "bottomNavigationDonate" : MessageLookupByLibrary.simpleMessage("Donate"),
    "profileChangePassword" : MessageLookupByLibrary.simpleMessage("Change Password"),
    "profileSelectLanguage" : MessageLookupByLibrary.simpleMessage("Select Language"),
    "profilePrivacy" : MessageLookupByLibrary.simpleMessage("Privacy Policy"),
    "profileSignout" : MessageLookupByLibrary.simpleMessage("Sign out"),
    "appbarContinue" : MessageLookupByLibrary.simpleMessage("Continue Reading"),
    "donateAppbar" : MessageLookupByLibrary.simpleMessage("Donate"),
    "textSearch" : MessageLookupByLibrary.simpleMessage("Search"),
    "textTerms" : MessageLookupByLibrary.simpleMessage("Terms & Conditions"),
    "textConfirm" : MessageLookupByLibrary.simpleMessage("Confirm"),
    "textChooseMedia" : MessageLookupByLibrary.simpleMessage("Please choose media to select"),
    "textFromGallery" : MessageLookupByLibrary.simpleMessage("From Gallery"),
    "textFromCamera" : MessageLookupByLibrary.simpleMessage("From Camera"),
  };
}
