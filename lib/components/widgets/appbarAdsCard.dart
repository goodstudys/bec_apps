import 'package:buddhist_educenter/components/widgets/appbarAdsDetails.dart';
import 'package:buddhist_educenter/model/adsModel.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:buddhist_educenter/env.dart';

class AppbarAdsCard extends StatelessWidget {
  final AdsModel data;

  AppbarAdsCard({
    @required this.data,
  });

  @override
  Widget build(BuildContext context) {
    MediaQueryData mediaQD = MediaQuery.of(context);
    return InkWell(
      onTap: () async {
        Navigator.push(context,
          MaterialPageRoute(builder: (BuildContext context) => AdsDetail(
            data: data,
          ))
        );
      },
      child: Container(
        padding: EdgeInsets.only(bottom: 15.0),
        child: Card(
          elevation: 3.0,
          color: Colors.white,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          child: Container(
            height: 80,
            width: mediaQD.size.width / 1.5,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      bottomLeft: Radius.circular(10),
                    ),
                    image: DecorationImage(
                      image: AssetImage('assets/img/placeholder.png'),
                      fit: BoxFit.cover,
                      alignment: Alignment.center,
                    )
                  ),
                  height: mediaQD.size.width / 4,
                  width: mediaQD.size.width / 5.5,
                  child: ClipRRect(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      bottomLeft: Radius.circular(10),
                    ),
                    child: CachedNetworkImage(
                      imageUrl: host + 'upload/ads/' + data.image,
                      placeholder: (context, url) => Container(child: Center(child: CircularProgressIndicator()),),
                      errorWidget: (context, url, error) => Image.asset('assets/img/placeholder.png'),
                      fit: BoxFit.cover,
                      alignment: Alignment.center,
                    )
                  )
                ),
                Container(
                  width: mediaQD.size.width / 2.1,
                  padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
                  alignment: Alignment.bottomCenter,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        data.title,
                        maxLines: 2,
                        softWrap: true,
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.left,
                        style: GoogleFonts.quattrocentoSans(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          color: Colors.black,
                          height: 1
                        ),
                      ),
                      Text(
                        data.descShort,
                        maxLines: 3,
                        softWrap: true,
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.left,
                        style: GoogleFonts.quattrocentoSans(
                          fontSize: 13,
                          color: Colors.black54,
                          letterSpacing: 0.2,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
