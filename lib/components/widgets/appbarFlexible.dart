/* import 'package:buddhist_educenter/bloc/book/book.dart';
import 'package:buddhist_educenter/components/widgets/appbarFlexibleCard.dart';
import 'package:buddhist_educenter/locale/my_localization.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

// import '../../bloc/cart/book.dart';
import '../../bloc/global/BlocProvider.dart';
import '../../bloc/global/GlobalBloc.dart';
import '../../model/bookList.dart';

class MyFlexiableAppBar extends StatelessWidget {
  final double appBarHeight = 66.0;

  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);
    final double statusBarHeight = MediaQuery.of(context).padding.top;
    return Container(
      padding: EdgeInsets.only(top: statusBarHeight),
      height: statusBarHeight + appBarHeight,
      child: Center(
        child: StreamBuilder(
          stream: BlocProvider.of< GlobalBloc>(context).bookBloc.bookStream,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              BookData dataBook = snapshot.data;
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(left: 15.0),
                    child: Text("Continue Reading ...",
                      style: GoogleFonts.quattrocentoSans(
                        color: Colors.white,
                        fontSize: 14.0
                      )
                    )
                  ),
                  Container(
                    height: 120,
                    child: ListView.builder(
                      padding: EdgeInsets.only(left: 10.0),
                      scrollDirection: Axis.horizontal,
                      itemCount: dataBook.listContinue.length,
                      itemBuilder: (BuildContext context, int index) {
                        BookModel data;
                        // listBookModel.forEach((a) {
                        //   if (a.id == dataBook.listContinue[index].id) {
                        //     data = a;
                        //   }
                        // });
                        return AppBarCard(
                          data: data,
                          title: dataBook.listContinue[index].title,
                          percent: dataBook.listContinue[index].pageRead / dataBook.listContinue[index].totalPage,
                          percentText: (dataBook.listContinue[index].pageRead / dataBook.listContinue[index].totalPage * 100).toInt().toString() + '%',
                          image: dataBook.listContinue[index].img,
                          pointer: dataBook.listContinue[index].rowPointer,
                        );
                      }
                    )
                  )
                ],
              );
            } else {
              return Container(
                padding: EdgeInsets.only(left: 15.0),
                child: Text(MyLocalization.of(context).homeStartReadingbook,
                  style: GoogleFonts.quattrocentoSans(
                    color: Colors.white,
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold
                  )
                )
              );
            }
          }
        )
      ),
      decoration: new BoxDecoration(
        color: theme.primaryColor,
      ),
    );
  }
}
 */