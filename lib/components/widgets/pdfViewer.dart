import 'dart:async';

import 'package:buddhist_educenter/bloc/global/BlocProvider.dart';
import 'package:buddhist_educenter/bloc/global/GlobalBloc.dart';
import 'package:buddhist_educenter/model/bookModelApi.dart';
import 'package:buddhist_educenter/model/readbookModel.dart';
import 'package:buddhist_educenter/env.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cached_pdfview/flutter_cached_pdfview.dart';
// import 'package:flutter_pdfview/flutter_pdfview.dart';
import 'package:numberpicker/numberpicker.dart';

class PDFScreen extends StatefulWidget {
  final String path;
  final int halaman;
  final String idBuku;
  final BookModelApi bookModel;

  PDFScreen({
    Key key,
    @required this.path,
    @required this.halaman,
    @required this.idBuku,
    @required this.bookModel,
  }) : super(key: key);

  _PDFScreenState createState() => _PDFScreenState();
}

class _PDFScreenState extends State<PDFScreen> with WidgetsBindingObserver {
  final Completer<PDFViewController> _controller = Completer<PDFViewController>();
  int pages = 0;
  int _pageNumber = 0;
  bool isReady = false;
  String errorMessage = '';

  void _updateReading(int page, int total) async {
    if (page > 0) {
      // prefs.setInt(widget.idBuku.toString(), _pageNumber);
      BlocProvider.of<GlobalBloc>(context).bookBloc.addBookItem(
        ReadingBookModel(
          id: widget.bookModel.id,
          title: widget.bookModel.title,
          author: widget.bookModel.author,
          rowPointer: widget.bookModel.rowPointer,
          description: widget.bookModel.description,
          booklink: gcsurl + widget.bookModel.book,
          img: gcsurl + widget.bookModel.thumbnail,
          pageRead: page,
          totalPage: total
        )
      );
    }
  }

  void _pickPage() async {
    showDialog<int>(
      context: context,
      builder: (BuildContext context) {
        return NumberPickerDialog.integer(
          title: Text('Select page'),
          minValue: 1,
          cancelWidget: Container(),
          maxValue: pages,
          initialIntegerValue: _pageNumber + 1,
        );
      }).then((int value) async {
      if (value != null) {
        await _controller.future.then((e) => e.setPage(value - 1));
      }
    });
  }

  @override
  void initState() {
    setState(() {
      _pageNumber = widget.halaman;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        backgroundColor: Color(0xFFF4375A),
        elevation: 4.0,
        tooltip: 'Select page',
        child: Icon(Icons.view_carousel),
        onPressed: () {
          _pickPage();
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Expanded(
              child: IconButton(
                icon: Icon(Icons.first_page),
                tooltip: 'First',
                onPressed: () async {
                  await _controller.future.then((value) => value.setPage(0));
                },
              ),
            ),
            Expanded(
              child: IconButton(
                icon: Icon(Icons.chevron_left),
                tooltip: 'Previous',
                onPressed: () async {
                  if (_pageNumber - 1 <= 0) {
                    await _controller.future.then((value) => value.setPage(0));
                  } else {
                    await _controller.future.then((value) => value.setPage(_pageNumber - 1));
                  }
                },
              ),
            ),
            Expanded(child: Text('')),
            Expanded(
              child: IconButton(
                icon: Icon(Icons.chevron_right),
                tooltip: 'Next',
                onPressed: () async {
                  if (_pageNumber + 1 >= pages) {
                    await _controller.future.then((value) => value.setPage(pages));
                  } else {
                    await _controller.future.then((value) => value.setPage(_pageNumber + 1));
                  }
                },
              ),
            ),
            Expanded(
              child: IconButton(
                icon: Icon(Icons.last_page),
                tooltip: 'Last',
                onPressed: () async {
                  await _controller.future.then((value) => value.setPage(pages));
                },
              ),
            ),
          ],
        )
      ),
      body: Stack(
        children: <Widget>[
          PDF(
            // filePath: widget.path,
            enableSwipe: true,
            swipeHorizontal: true,
            autoSpacing: false,
            pageFling: true,
            pageSnap: true,
            defaultPage: _pageNumber,
            fitPolicy: FitPolicy.BOTH,
            preventLinkNavigation: false, // if set to true the link is handled in flutter
            onRender: (_pages) {
              setState(() {
                pages = _pages;
                isReady = true;
              });
            },
            onError: (error) {
              setState(() {
                errorMessage = error.toString();
              });
              debugPrint(error.toString());
            },
            onPageError: (page, error) {
              setState(() {
                errorMessage = '$page: ${error.toString()}';
              });
              debugPrint('$page: ${error.toString()}');
            },
            onViewCreated: (PDFViewController pdfViewController) {
              _controller.complete(pdfViewController);
            },
            /* onLinkHandler: (String uri) {
              print('goto uri: $uri');
            }, */
            onPageChanged: (int page, int total) {
              debugPrint('page change: $page/$total');
              setState(() {
                _pageNumber = page;
              });
              _updateReading(page, total);
            },
          ).cachedFromUrl(
            widget.path,
            placeholder: (progress) => Center(child: Text('$progress %')),
            errorWidget: (error) => Center(child: Text(error.toString())),
          ),
          errorMessage.isEmpty ? !isReady ? Center(
            child: CircularProgressIndicator(),
          ) : Container() : Center(
            child: Padding(
              padding: const EdgeInsets.all(15.0),
              child: Text(errorMessage),
            ),
          )
        ],
      ),
    );
  }
}