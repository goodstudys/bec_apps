import 'package:buddhist_educenter/env.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class CardBooknew extends StatefulWidget {
  final dynamic listBookModel;
  final String img, title, description, author;
  final GestureTapCallback tap;

  CardBooknew({
    Key key,
    this.listBookModel,
    this.description,
    this.tap,
    this.img,
    this.title,
    this.author,
  }) : super(key: key);
  @override
  _CardBookState createState() => _CardBookState();
}

class _CardBookState extends State<CardBooknew> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: widget.tap,
      child: Card(
        elevation: 2.0,
        color: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Stack(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10),
                  bottomLeft: Radius.circular(10)
                ),
                image: DecorationImage(
                  image: AssetImage('assets/img/placeholder.png'),
                  fit: BoxFit.cover,
                  alignment: Alignment.center,
                ),
              ),
              height: 145,
              width: MediaQuery.of(context).size.width / 3.8,
              child: ClipRRect(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10),
                  bottomLeft: Radius.circular(10)
                ),
                child: CachedNetworkImage(
                  imageUrl: gcsurl + widget.img,
                  placeholder: (context, url) => Container(child: Center(child: CircularProgressIndicator()),),
                  errorWidget: (context, url, error) => Image.asset('assets/img/placeholder.png'),
                  fit: BoxFit.cover,
                  alignment: Alignment.center,
                )
              )
            ),
            Container(
                height: 145,
                width: MediaQuery.of(context).size.width / 1.125,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(''),
                    Container(
                      width: MediaQuery.of(context).size.width / 1.6,
                      child: Padding(
                        padding: const EdgeInsets.all(13.0),
                        child: Stack(
                          children: <Widget>[
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(bottom: 2.0),
                                  child: Text(
                                    widget.title,
                                    maxLines: 2,
                                    softWrap: true,
                                    overflow: TextOverflow.ellipsis,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.quattrocentoSans(
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(bottom: 10.0),
                                  child: Text(
                                    'Author: ' + widget.author,
                                    maxLines: 1,
                                    softWrap: true,
                                    overflow: TextOverflow.ellipsis,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.quattrocentoSans(
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black54),
                                  ),
                                ),
                                Text(
                                  widget.description,
                                  maxLines: 3,
                                  overflow: TextOverflow.ellipsis,
                                  softWrap: true,
                                  style: GoogleFonts.quattrocentoSans(
                                      fontSize: 12, color: Colors.black54),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                )),
          ],
        ),
      ),
    );
  }
}
