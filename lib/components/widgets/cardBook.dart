import 'package:buddhist_educenter/pages/detailbook.dart';
import 'package:buddhist_educenter/storage/storage.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../env.dart';

class CardBook extends StatefulWidget {
  final dynamic listBookModel;

  CardBook({
    Key key,
    this.listBookModel,
  }) : super(key: key);
  @override
  _CardBookState createState() => _CardBookState();
}

class _CardBookState extends State<CardBook> {
  DataStore store = DataStore();
  
  Future<String> senddataBook(String pointer) async {
    store.setDataString('BookPointer', pointer);
    Navigator.push(context,
      MaterialPageRoute(builder: (BuildContext context) => DetailBook()));
    return 'success!';
  }
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
senddataBook(widget.listBookModel.pointer);
      },
      child: Card(
        elevation: 2.0,
        color: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Stack(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10),
                  bottomLeft: Radius.circular(10)
                ),
                image: DecorationImage(
                  image: AssetImage('assets/img/placeholder.png'),
                  fit: BoxFit.cover,
                  alignment: Alignment.center,
                )
              ),
              height: 145,
              width: MediaQuery.of(context).size.width / 3.8,
              child: ClipRRect(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10),
                  bottomLeft: Radius.circular(10)
                ),
                child: CachedNetworkImage(
                  imageUrl: gcsurl + widget.listBookModel.img,
                  placeholder: (context, url) => Container(child: Center(child: CircularProgressIndicator()),),
                  errorWidget: (context, url, error) => Image.asset('assets/img/placeholder.png'),
                  fit: BoxFit.cover,
                  alignment: Alignment.center,
                )
              ),
            ),
            Container(
                height: 145,
                width: MediaQuery.of(context).size.width / 1.125,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(''),
                    Container(
                      width: MediaQuery.of(context).size.width / 1.6,
                      child: Padding(
                        padding: const EdgeInsets.all(13.0),
                        child: Stack(
                          children: <Widget>[
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(bottom: 10.0),
                                  child: Text(
                                    widget.listBookModel.title,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.quattrocentoSans(
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black),
                                  ),
                                ),
                                Text(
                                  widget.listBookModel.description,
                                  maxLines: 4,
                                  overflow: TextOverflow.ellipsis,
                                  style: GoogleFonts.quattrocentoSans(
                                      fontSize: 9, color: Colors.black),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                )),
          ],
        ),
      ),
    );
  }
}
