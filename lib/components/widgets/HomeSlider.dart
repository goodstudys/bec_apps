import 'package:buddhist_educenter/model/eventModel.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:buddhist_educenter/env.dart';

class HomeSlider extends StatelessWidget {
  const HomeSlider({
    Key key,
    @required this.events,
    @required this.action
  }) : super(key: key);

  final EventModel events;
  final Function action;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5.0),
        color: Colors.grey[300]
      ),
      child: InkWell(
        onTap: action,
        child: ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(5.0)),
          child: CachedNetworkImage(
            imageUrl: host + 'upload/events/' + events.image,
            placeholder: (context, url) => Container(child: Center(child: CircularProgressIndicator()),),
            errorWidget: (context, url, error) => Image.asset('assets/img/placeholder-ls.png'),
            fit: BoxFit.fitHeight
          )
        ),
      ),
    );
  }
}