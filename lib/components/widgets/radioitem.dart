import 'package:buddhist_educenter/model/LanguageModel.dart';
import 'package:flutter/material.dart';

class RadioItem extends StatelessWidget {
  final LanguageModel _item;
  RadioItem(this._item);
  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);
    return Container(
      margin: EdgeInsets.only(top: 20.0, left: 0.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Row(
            children: <Widget>[
              Container(
                  padding:
                      EdgeInsets.symmetric(horizontal: 10.0, vertical: 0.0),
                  child: Image(
                    width: 50.0,
                      image: AssetImage(
                          'assets/img/flag/' + _item.locale1 + '.png'))),
              Container(
                margin: EdgeInsets.only(left: 10.0),
                child: Text(
                  _item.text,
                  style: TextStyle(
                    color: Colors.grey,
                    fontWeight: FontWeight.bold,
                    fontSize: 17.0,
                  ),
                ),
              ),
            ],
          ),
          Container(
            height: 25.0,
            width: 25.0,
            child: Center(
                child: _item.isSelected
                    ? Icon(
                        Icons.check,
                        color: Colors.white,
                        size: 20.0,
                      )
                    : Container()),
            decoration: BoxDecoration(
              color: _item.isSelected ? theme.primaryColor : Colors.transparent,
              border: Border.all(
                  width: 1.0,
                  color: _item.isSelected ? theme.primaryColor : Colors.grey),
              borderRadius: const BorderRadius.all(const Radius.circular(25.0)),
            ),
          ),
          // Container(
          //   child: _item.isSelected
          //       ? Text(
          //           'Rp. ' + _item.buttonText,
          //           style: TextStyle(
          //             color: Colors.green,
          //             fontWeight: FontWeight.bold,
          //             fontSize: 20.0,
          //           ),
          //         )
          //       : Container(),
          // )
        ],
      ),
    );
  }
}
