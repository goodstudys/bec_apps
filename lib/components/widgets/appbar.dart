import 'package:buddhist_educenter/pages/about.dart';
import 'package:buddhist_educenter/pages/calendar.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';

class MyAppBar extends StatelessWidget {
  final double barHeight = 66.0;
  final bool closed;

  const MyAppBar({
    @required this.closed
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          InkWell(
            onTap: () {
              Navigator.push(context, MaterialPageRoute(
                builder: (BuildContext context) => AboutPage())
              );
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  child: ClipRRect(
                    child: Image.asset(
                      'assets/img/logo_bec.jpg',
                      width: 30.0,
                      height: 30.0,
                      fit: BoxFit.cover,
                    ),
                    borderRadius: BorderRadius.circular(30.0),
                  )
                ),
                Container(
                  child: Padding(
                    padding: EdgeInsets.all(16.0),
                    child: AnimatedCrossFade(
                      duration: Duration(milliseconds: 300),
                      firstChild: Text(
                        'BEC App',
                        overflow: TextOverflow.ellipsis,
                        softWrap: true,
                        style: GoogleFonts.quattrocentoSans(
                          color: Colors.white,
                          fontSize: closed ? 20.0 : 16.0,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                      secondChild: Text(
                        'BUDDHIST EDUCATION CENTRE',
                        overflow: TextOverflow.ellipsis,
                        softWrap: true,
                        style: GoogleFonts.quattrocentoSans(
                          color: Colors.white,
                          fontSize: closed ? 18.0 : 14.0,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                      crossFadeState: closed ? CrossFadeState.showFirst : CrossFadeState.showSecond,
                    )
                  ),
                ),
              ],
            ),
          ),
          InkWell(
            onTap: () {
              Navigator.push(context, MaterialPageRoute(
                builder: (BuildContext context) => CalendarPage())
              );
            },
            child: FaIcon(
              FontAwesomeIcons.calendarAlt,
              color: Colors.white,
              size: 20,
            ),
          ),
        ],
      ),
    );
  }
}
