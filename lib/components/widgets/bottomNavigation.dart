import 'package:buddhist_educenter/locale/my_localization.dart';
import 'package:buddhist_educenter/pages/books.dart';
import 'package:buddhist_educenter/pages/continueReading.dart';
import 'package:buddhist_educenter/pages/donation.dart';
import 'package:buddhist_educenter/pages/profile.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';

class BottomNav extends StatelessWidget {
  const BottomNav({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Container(
          alignment: Alignment.center,
          child: Card(
            elevation: 2.0,
            color: Colors.white,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30.0),
            ),
            child: Container(
              height: 100,
              width: MediaQuery.of(context).size.width / 1.1,
              padding: EdgeInsets.only(
                  top: 13.0, bottom: 13.0, left: 23.0, right: 23.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(bottom: 8.0),
                        decoration: BoxDecoration(
                            color: theme.primaryColor,
                            borderRadius: BorderRadius.circular(20)),
                        child: IconButton(
                            icon: FaIcon(
                              FontAwesomeIcons.book,
                              color: Colors.white,
                            ),
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          BooksPage()));
                            }),
                      ),
                      Text(
                        MyLocalization.of(context).bottomNavigationBooks,
                        style: GoogleFonts.quattrocentoSans(
                            fontSize: 12, fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(bottom: 8.0),
                        decoration: BoxDecoration(
                            color: theme.primaryColor,
                            borderRadius: BorderRadius.circular(20)),
                        child: IconButton(
                            icon: FaIcon(
                              FontAwesomeIcons.bookReader,
                              color: Colors.white,
                            ),
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          ContinueReading()));
                            }),
                      ),
                      Text(
                        MyLocalization.of(context).bottomNavigationRead,
                        style: GoogleFonts.quattrocentoSans(
                            fontSize: 12, fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(bottom: 8.0),
                        decoration: BoxDecoration(
                            color: theme.primaryColor,
                            borderRadius: BorderRadius.circular(20)),
                        child: IconButton(
                            icon: FaIcon(
                              FontAwesomeIcons.userAlt,
                              color: Colors.white,
                            ),
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          ProfilePage()));
                            }),
                      ),
                      Text(
                        MyLocalization.of(context).bottomNavigationAccount,
                        style: GoogleFonts.quattrocentoSans(
                            fontSize: 12, fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(bottom: 8.0),
                        decoration: BoxDecoration(
                            color: theme.primaryColor,
                            borderRadius: BorderRadius.circular(20)),
                        child: IconButton(
                            icon: FaIcon(
                              FontAwesomeIcons.donate,
                              color: Colors.white,
                            ),
                            onPressed: () {
                              Navigator.push(context, MaterialPageRoute(
                                builder: (BuildContext context) => DonationPage())
                              );
                            }),
                      ),
                      Text(
                        MyLocalization.of(context).bottomNavigationDonate,
                        style: GoogleFonts.quattrocentoSans(
                            fontSize: 12, fontWeight: FontWeight.bold),
                      )
                    ],
                  )
                ],
              )
            ),
          ),
        ),
      ],
    );
  }
}
