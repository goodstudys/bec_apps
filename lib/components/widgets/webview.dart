import 'dart:io';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:webview_flutter/webview_flutter.dart';

class ViewURL extends StatefulWidget {
  final String url;
  final String title;

  ViewURL({
    @required this.url,
    @required this.title,
  });

  @override
  ViewURLState createState() => ViewURLState();
}

class ViewURLState extends State<ViewURL> {
  @override
  void initState() {
    super.initState();
    // Enable hybrid composition.
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
  }

  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: theme.primaryColor,
        centerTitle: true,
        title: Text(
          widget.title,
          style: GoogleFonts.quattrocentoSans(
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontSize: 20
          )
        ),
      ),
      body: WebView(
        initialUrl: widget.url,
      )
    );
  }
}