import 'dart:io';
import 'package:buddhist_educenter/components/widgets/webview.dart';
import 'package:buddhist_educenter/locale/my_localization.dart';
import 'package:buddhist_educenter/model/adsModel.dart';
import 'package:buddhist_educenter/env.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class AdsDetail extends StatelessWidget {
  final AdsModel data;
  AdsDetail({
    @required this.data
  });

  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);
    MediaQueryData mediaQD = MediaQuery.of(context);
    return Scaffold(
      backgroundColor: theme.backgroundColor,
      body: Stack(
        children: <Widget>[
          SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  height: MediaQuery.of(context).size.height / 1.3,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    color: theme.backgroundColor,
                    image: DecorationImage(
                      image: NetworkImage(host + 'upload/ads/' + data.image),
                      fit: BoxFit.contain,
                      alignment: Alignment.center,
                    )
                  ),
                ),
                Text(
                  data.title,
                  style: GoogleFonts.quattrocentoSans(
                    fontSize: 22,
                    color: Colors.black87,
                    fontWeight: FontWeight.w600
                  ),
                ),
                Text(
                  data.street,
                  style: GoogleFonts.quattrocentoSans(
                    fontSize: 16.5,
                    color: Colors.black87
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Text(
                  data.descFull,
                  style: GoogleFonts.quattrocentoSans(
                    fontSize: 16.5,
                    color: Colors.black87
                  ),
                ),
                InkWell(
                  onTap: () async {
                    Navigator.push(context, MaterialPageRoute(
                      builder: (BuildContext context) => ViewURL(
                        url: data.link,
                        title: data.title,
                      )
                    ));
                  },
                  child: Container(
                    height: 52,
                    margin: EdgeInsets.only(top: 10.0),
                    decoration: BoxDecoration(
                      color: theme.primaryColor,
                      borderRadius: BorderRadius.circular(10),
                    ),
                    width: mediaQD.size.width / 2.5,
                    child: Align(
                      alignment: Alignment.center,
                      child: Text(
                        MyLocalization.of(context).adsDetailMore,
                        style: TextStyle(color: Colors.white, fontSize: 16.5),
                      )
                    )
                  ),
                ),
              ],
            )
          ),
          Positioned(
            top: 40,
            left: 10.0,
            child: MaterialButton(
              height: 50.0,
              minWidth: 50.0,
              elevation: 0,
              padding: EdgeInsets.zero,
              splashColor: Colors.amber[800],
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(50.0)
              ),
              color: Colors.black12,
              child: Icon(
                Platform.isIOS ? Icons.arrow_back_ios : Icons.arrow_back,
                color: Colors.white,
                size: 22,
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            )
          ),
        ],
      ),
    );
  }
}
