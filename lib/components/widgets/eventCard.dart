import 'package:buddhist_educenter/env.dart';
import 'package:buddhist_educenter/pages/eventdetail.dart';
import 'package:buddhist_educenter/storage/storage.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class EventCard extends StatefulWidget {
  final dynamic data;
  EventCard({@required this.data});
  @override
  _EventCardState createState() => _EventCardState();
}

class _EventCardState extends State<EventCard> {
  DataStore store = DataStore();

  Future<String> senddataEvent(String pointer) async {
    store.setDataString('EventPointer', pointer);
    Navigator.push(context,
        MaterialPageRoute(builder: (BuildContext context) => EventDetail()));
    return 'success!';
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: widget.data.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (BuildContext context, int index) {
          return InkWell(
            onTap: () {
              senddataEvent(widget.data[index]['row_pointer']);
            },
            child: Card(
              elevation: 2.0,
              color: Colors.white,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              child: Stack(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            bottomLeft: Radius.circular(10)),
                        image: DecorationImage(
                          image: NetworkImage(host +
                              'upload/events/' +
                              widget.data[index]['image']),
                          fit: BoxFit.cover,
                          alignment: Alignment.center,
                        )),
                    height: 145,
                    width: MediaQuery.of(context).size.width / 3.8,
                  ),
                  Container(
                      height: 145,
                      width: MediaQuery.of(context).size.width / 1.35,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(''),
                          Container(
                            width: MediaQuery.of(context).size.width / 2.1,
                            child: Padding(
                              padding: const EdgeInsets.all(13.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Padding(
                                    padding:
                                        const EdgeInsets.only(bottom: 10.0),
                                    child: Text(
                                      widget.data[index]['title'],
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                      textAlign: TextAlign.left,
                                      style: GoogleFonts.quattrocentoSans(
                                          fontSize: 12,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black),
                                    ),
                                  ),
                                  Text(
                                    widget.data[index]['description'],
                                    maxLines: 4,
                                    overflow: TextOverflow.ellipsis,
                                    style: GoogleFonts.quattrocentoSans(
                                        fontSize: 9, color: Colors.black),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      )),
                ],
              ),
            ),
          );
        });
  }
}
