import 'dart:convert';
import 'package:buddhist_educenter/components/widgets/appbarAdsCard.dart';
import 'package:buddhist_educenter/model/adsModel.dart';
import 'package:buddhist_educenter/storage/storage.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:buddhist_educenter/env.dart';

class AppBarAds extends StatefulWidget {
  @override
  _AppBarAdsState createState() => _AppBarAdsState();
}

class _AppBarAdsState extends State<AppBarAds> {
  final double appBarHeight = 66.0;
  bool _isLoading = false;
  List<AdsModel> adsList = [];
  DataStore store = DataStore();
  
  void getAds() async {
    setState(() {
      adsList = [];
      _isLoading = true;
    });
    var tokenType = await store.getDataString('token_type');
    var accessToken = await store.getDataString('access_token');
    var res = await http.get(url('api/ads'), headers: {
      'Authorization': tokenType + ' ' + accessToken,
      "Content-type": "application/json",
      "accept": "application/json",
    });
    var content = json.decode(res.body);
    if (res.statusCode == 200 && content['status'] == 'success') {
      setState(() {
        adsList = List<AdsModel>.from(content['events'].map((item) => AdsModel.fromJson(item)));
        _isLoading = false;
      });
    } else {
      setState(() {
        _isLoading = false;
      });
    }
  }

  @override
  void initState() {
    getAds();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);
    final double statusBarHeight = MediaQuery.of(context).padding.top;
    return Container(
      padding: EdgeInsets.only(top: statusBarHeight + 60.0),
      height: statusBarHeight + appBarHeight,
      decoration: BoxDecoration(
        color: theme.primaryColor,
      ),
      child: Center(
        child: _isLoading ? Container(
          child: CircularProgressIndicator(),
        ) : !_isLoading && adsList.length > 0 ? Container(
          height: 120,
          child: ListView.builder(
            padding: EdgeInsets.only(left: 10.0),
            scrollDirection: Axis.horizontal,
            itemCount: adsList.length,
            itemBuilder: (BuildContext context, int index) {
              return AppbarAdsCard(
                data: adsList[index],
              );
            }
          )
        ) : Container()
      ),
    );
  }
}
