/* import 'package:buddhist_educenter/storage/storage.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

import '../../model/bookList.dart';
import '../../pages/readbook.dart';

class AppBarCard extends StatelessWidget {
  final BookModel data;
  final String title;
  final double percent;
  final String percentText;
  final String image;
  final String pointer;

  AppBarCard({
    @required this.data,
    @required this.title,
    @required this.percent,
    @required this.percentText,
    @required this.image,
    @required this.pointer
  });

  DataStore store = DataStore();

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () async {
        store.setDataString('BookPointer', pointer);
        Navigator.push(context,
            MaterialPageRoute(builder: (BuildContext context) => ReadBook()));
      },
      child: Container(
        padding: EdgeInsets.only(bottom: 15.0),
        child: Stack(
          alignment: Alignment.bottomCenter,
          children: <Widget>[
            Card(
              elevation: 2.0,
              color: Colors.white,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              child: Container(
                height: 80,
                width: MediaQuery.of(context).size.width / 1.5,
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    image: DecorationImage(
                      image: AssetImage('assets/img/placeholder.png'),
                      fit: BoxFit.cover,
                      alignment: Alignment.center,
                    )
                  ),
                  height: MediaQuery.of(context).size.width / 4,
                  width: MediaQuery.of(context).size.width / 6,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10.0),
                    child: FadeInImage.assetNetwork(
                      placeholder: 'assets/img/placeholder.png',
                      image: image,
                      fit: BoxFit.cover,
                      alignment: Alignment.center,
                    )
                  )
                ),
                Container(
                  height: 90,
                  width: MediaQuery.of(context).size.width / 2,
                  padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
                  alignment: Alignment.bottomCenter,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        height: MediaQuery.of(context).size.width / 10,
                        child: Text(
                          title,
                          maxLines: 3,
                          softWrap: true,
                          overflow: TextOverflow.ellipsis,
                          textAlign: TextAlign.left,
                          style: GoogleFonts.quattrocentoSans(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                            color: Colors.black
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 2),
                        child: LinearPercentIndicator(
                          width: MediaQuery.of(context).size.width / 2.6,
                          lineHeight: 15.0,
                          progressColor: Color(0xFFF4375A),
                          percent: percent,
                          center: Text(
                            percentText,
                            style: GoogleFonts.quattrocentoSans(
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                              color: Colors.white
                            ),
                          ),
                          animation: true,
                        ),
                      )
                    ],
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
 */