import 'package:buddhist_educenter/env.dart';
import 'package:buddhist_educenter/pages/detailbook.dart';
import 'package:buddhist_educenter/storage/storage.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class CardBookImage extends StatefulWidget {
  final dynamic data;
  CardBookImage({@required this.data});
  @override
  _CardBookImageState createState() => _CardBookImageState();
}

class _CardBookImageState extends State<CardBookImage> {
  DataStore store = DataStore();

  Future<String> senddataBook(String pointer) async {
    store.setDataString('BookPointer', pointer);

    Navigator.push(context,
        MaterialPageRoute(builder: (BuildContext context) => DetailBook()));
    return 'success!';
  }
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      padding: EdgeInsets.only(left: 15.0),
      itemCount: widget.data.length,
      scrollDirection: Axis.horizontal,
      itemBuilder: (BuildContext context, int index) {
        return Container(
          child: InkWell(
            onTap: () {
              senddataBook(widget.data[index]['row_pointer']);
            },
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                  ),
                  elevation: 3.0,
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      image: DecorationImage(
                        image: AssetImage('assets/img/placeholder.png'),
                        fit: BoxFit.cover,
                        alignment: Alignment.center,
                      ),
                    ),
                    height: MediaQuery.of(context).size.width / 2,
                    width: MediaQuery.of(context).size.width / 3,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10.0),
                      child: CachedNetworkImage(
                        imageUrl: gcsurl + widget.data[index]['thumbnail'],
                        placeholder: (context, url) => Container(child: Center(child: CircularProgressIndicator()),),
                        errorWidget: (context, url, error) => Image.asset('assets/img/placeholder.png'),
                        fit: BoxFit.cover,
                        alignment: Alignment.center,
                      )
                    )
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 5.0, bottom: 5.0),
                  width: MediaQuery.of(context).size.width / 3.4,
                  child: Text(
                    widget.data[index]['title'],
                    maxLines: 3,
                    textAlign: TextAlign.center,
                    overflow: TextOverflow.ellipsis,
                    style: GoogleFonts.quattrocentoSans(
                      fontSize: 14,
                      fontWeight: FontWeight.bold,
                      color: Colors.black
                    ),
                  )
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
