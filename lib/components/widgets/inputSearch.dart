import 'package:buddhist_educenter/locale/my_localization.dart';
import 'package:flutter/material.dart';

class InputSearch extends StatelessWidget {
  final TextEditingController controllerInput;
  final Function(String) submitted;
  final double lebar;
  InputSearch({@required this.controllerInput, @required this.submitted,@required this.lebar});

  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);
    return Container(
      margin: EdgeInsets.only(top: 8.0),
      width: MediaQuery.of(context).size.width / lebar,
      height: 42,
      child: Align(
          alignment: Alignment.center,
          child: Theme(
            data: ThemeData(
              hintColor: Colors.transparent,
            ),
            child: Container(
              width: MediaQuery.of(context).size.width / 1.1,
              height: 42,
              child: TextField(
                textInputAction: TextInputAction.search,
                onSubmitted: submitted,
                controller: controllerInput,
                decoration: InputDecoration(
                  hintText: MyLocalization.of(context).textSearch,
                  hintStyle: TextStyle(
                    color: Color(0xff757575),
                    fontSize: 16,
                    fontFamily: 'Regular',
                  ),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(30),
                    borderSide: BorderSide(color: Colors.grey[400]),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: theme.primaryColor),
                    borderRadius: BorderRadius.circular(30),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(30),
                    borderSide: BorderSide(color: Colors.grey[400]),
                  ),
                  contentPadding: EdgeInsets.symmetric(vertical: 0),
                  prefixIcon: Icon(
                    Icons.search,
                    color: Color(0xff757575),
                  ),
                  fillColor: Color(0xffeeeeee),
                  filled: true,
                ),
              ),
            ),
          )),
    );
  }
}
