import 'package:flutter_cache_manager/flutter_cache_manager.dart';

class CustomCacheManager {
  static const key = 'becBooks';
  static const duration = Duration(days: 7);
  final CacheManager instance = CacheManager(
    Config(
      key,
      stalePeriod: duration,
      maxNrOfCacheObjects: 200,
      repo: JsonCacheInfoRepository(databaseName: key),
      // fileSystem: IOFileSystem(key),
      fileService: HttpFileService(),
    ),
  );
}