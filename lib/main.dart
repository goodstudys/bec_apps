import 'package:buddhist_educenter/bloc/global/BlocProvider.dart';
import 'package:buddhist_educenter/bloc/global/GlobalBloc.dart';
import 'package:buddhist_educenter/locale/my_localization_delegate.dart';
import 'package:buddhist_educenter/locale/provider_localization.dart';
import 'package:buddhist_educenter/pages/splashScreen.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';

// import 'package:admob_flutter/admob_flutter.dart';
void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: Colors.transparent, // transparent status bar
  ));
  LicenseRegistry.addLicense(() async* {
    final license = await rootBundle.loadString('google_fonts/OFL.txt');
    yield LicenseEntryWithLineBreaks(['google_fonts'], license);
  });
  runApp(MyApp());
  // Admob.initialize('ca-app-pub-3590413100109764~7157663115');
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  // MyLocalizationDelegate _myLocalizationDelegate =
  // MyLocalizationDelegate(Locale('en', 'US'));
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (context) => ProviderLocalization(),
        // builder: (context) => ProviderLocalization(),
        child: Consumer<ProviderLocalization>(
            builder: (context, localization, child) {
          return BlocProvider<GlobalBloc>(
            bloc: GlobalBloc(),
            child: MaterialApp(
              title: 'BEC Apps',
              theme: ThemeData(
                primarySwatch: Colors.blue,
                primaryColor: Color(0xFF15469D),
                accentColor: Color(0xFFEB1C24),
                visualDensity: VisualDensity.adaptivePlatformDensity,
                textTheme: GoogleFonts.quattrocentoSansTextTheme(
                  Theme.of(context).textTheme,
                ),
                brightness: Brightness.light,
                scaffoldBackgroundColor: Color(0xFFF0F0F0),
                backgroundColor: Color(0xFFF0F0F0),
              ),
              debugShowCheckedModeBanner: false,
              localizationsDelegates: [
                GlobalMaterialLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
                MyLocalizationDelegate(localization.language),
              ],
              supportedLocales: [
                Locale('en', 'US'),
                Locale('id', 'ID'),
                Locale('cn', 'CN'),
              ],
              home: SplashScreen(),
            ),
          );
        }));
  }
}
