import 'dart:convert';
import 'package:buddhist_educenter/storage/storage.dart';
import 'package:http/http.dart' as http;
import 'package:buddhist_educenter/env.dart';
// import 'package:buddhist_educenter/model/eventList.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

class EventDetail extends StatefulWidget {
  // EventDetail({this.eventData});
  @override
  _EventDetailState createState() => _EventDetailState();
}

class _EventDetailState extends State<EventDetail> {
  DataStore store = DataStore();
  DateFormat dateFormat = DateFormat("yyyy-MM-dd HH:mm:ss");
  
  final DateFormat formatter = DateFormat('yyyy-MM-dd');
  final DateFormat waktu = DateFormat('HH:mm');
  DateTime tanggalMulai, tanggalSelesai;
  bool _isLoading = false;
  var data;
  Future<String> detailEvent() async {
    setState(() {
      _isLoading = true;
    });
    var tokenType = await store.getDataString('token_type');
    var accessToken = await store.getDataString('access_token');
    var pointer = await store.getDataString('EventPointer');
    var res = await http.get(url('api/event/' + pointer),
        headers: {'Authorization': tokenType + ' ' + accessToken});

    setState(() {
      var content = json.decode(res.body);
      debugPrint(content['event']['title'].toString());
      tanggalMulai = dateFormat.parse(content['event']['date_start']);
      tanggalSelesai = dateFormat.parse(content['event']['date_end']);
      data = content['event'];
    });
    setState(() {
      _isLoading = false;
    });
    return 'success!';
  }

  @override
  void initState() {
    super.initState();
    detailEvent();
  }

  @override
  Widget build(BuildContext context) {
    return _isLoading == true
        ? Container(
            child: Center(
              child: CircularProgressIndicator(),
            ),
          )
        : Scaffold(
            body: SingleChildScrollView(
              child: SafeArea(
                  child: Container(
                width: MediaQuery.of(context).size.width,
                child: Align(
                  alignment: Alignment.topCenter,
                  child: Stack(
                    children: <Widget>[
                      Positioned(
                        top: 0.0,
                        child: Container(
                          child: Align(
                            alignment: Alignment.center,
                            child: Container(
                              height: MediaQuery.of(context).size.height / 2.1,
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                  color: Colors.black,
                                  image: DecorationImage(
                                    image: NetworkImage(host +
                                        'upload/events/' +
                                        data['image']),
                                    fit: BoxFit.cover,
                                    alignment: Alignment.center,
                                    colorFilter: new ColorFilter.mode(
                                        Colors.black.withOpacity(0.5),
                                        BlendMode.dstATop),
                                  )),
                            ),
                          ),
                        ),
                      ),
                      Positioned(
                        top: 0.0,
                        right: 0.0,
                        left: 0.0,
                        child: Container(
                          child: Align(
                            alignment: Alignment.center,
                            child: Container(
                              height: 60,
                              width: MediaQuery.of(context).size.width / 1.01,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  IconButton(
                                      icon: Icon(
                                        Icons.arrow_back,
                                        color: Colors.white,
                                        size: 26,
                                      ),
                                      onPressed: () {
                                        Navigator.pop(context);
                                      }),
                                  Text('')
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        child: Column(
                          children: <Widget>[
                            Container(
                                height:
                                    MediaQuery.of(context).size.height / 2.4,
                                width: MediaQuery.of(context).size.width / 1.1,
                                child: Align(
                                    alignment: Alignment.bottomLeft,
                                    child: Text(
                                      data['title'],
                                      style: GoogleFonts.quattrocentoSans(
                                          fontSize: 26,
                                          color: Color(0xFFFCFCFC)),
                                    ))),
                            Container(
                                margin: EdgeInsets.only(top: 13.0),
                                width: MediaQuery.of(context).size.width / 1.1,
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      Container(
                                        width:
                                            MediaQuery.of(context).size.width /
                                                2.6,
                                        height: 200,
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(10)),
                                            image: DecorationImage(
                                              image: NetworkImage(host +
                                                  'upload/events/' +
                                                  data['image']),
                                              fit: BoxFit.cover,
                                              alignment: Alignment.center,
                                            )),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(top: 33.0),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              children: <Widget>[
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.center,
                                                  children: <Widget>[
                                                    Container(
                                                        width: 40,
                                                        child: Icon(
                                                            Icons.date_range)),
                                                    Text(
                                                      formatter.format(tanggalMulai),
                                                      style: GoogleFonts
                                                          .montserrat(
                                                              fontSize: 14),
                                                    )
                                                  ],
                                                ),
                                                Row(
                                                  children: <Widget>[
                                                    Container(
                                                      width: 40,
                                                    ),
                                                    Text(
                                                      waktu.format(tanggalMulai) +
                                                          ' - ' +
                                                          waktu.format(tanggalSelesai),
                                                      style: GoogleFonts
                                                          .montserrat(
                                                              color: Colors
                                                                  .black38,
                                                              fontSize: 12),
                                                    )
                                                  ],
                                                ),
                                              ],
                                            ),
                                            Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              children: <Widget>[
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.center,
                                                  children: <Widget>[
                                                    Container(
                                                        width: 40,
                                                        child: Icon(
                                                            Icons.pin_drop)),
                                                    Text(
                                                      data['location'],
                                                      style: GoogleFonts
                                                          .montserrat(
                                                              fontSize: 14),
                                                    )
                                                  ],
                                                ),
                                                Row(
                                                  children: <Widget>[
                                                    Container(
                                                      width: 40,
                                                    ),
                                                    Container(
                                                        width:
                                                            MediaQuery.of(
                                                                        context)
                                                                    .size
                                                                    .width /
                                                                2.5,
                                                        child: Text(
                                                            data['street'],
                                                            style: GoogleFonts
                                                                .montserrat(
                                                                    color: Colors
                                                                        .black38,
                                                                    fontSize:
                                                                        12)))
                                                  ],
                                                ),
                                              ],
                                            )
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                )),
                            Container(
                              width: MediaQuery.of(context).size.width / 1.1,
                              padding: EdgeInsets.only(top: 23.0),
                              child: Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  'Event',
                                  style: GoogleFonts.quattrocentoSans(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16),
                                ),
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width / 1.1,
                              padding: EdgeInsets.only(top: 23.0, bottom: 23.0),
                              child: Align(
                                alignment: Alignment.centerLeft,
                                child: Text(data['description']),
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              )),
            ),
          );
  }
}
