import 'dart:convert';
import 'package:buddhist_educenter/auth/login.dart';
import 'package:buddhist_educenter/bloc/global/BlocProvider.dart';
import 'package:buddhist_educenter/bloc/global/GlobalBloc.dart';
import 'package:buddhist_educenter/components/widgets/HomeSlider.dart';
import 'package:buddhist_educenter/components/widgets/appbar.dart';
import 'package:buddhist_educenter/components/widgets/appbarAds.dart';
import 'package:buddhist_educenter/components/widgets/bottomNavigation.dart';
import 'package:buddhist_educenter/components/widgets/cardBookImage.dart';
import 'package:buddhist_educenter/env.dart';
import 'package:buddhist_educenter/locale/my_localization.dart';
import 'package:buddhist_educenter/model/eventModel.dart';
import 'package:buddhist_educenter/pages/detailbook.dart';
import 'package:buddhist_educenter/pages/eventdetailImage.dart';
import 'package:buddhist_educenter/storage/storage.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:http/http.dart' as http;
import 'package:buddhist_educenter/pages/tabsHome/all.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with SingleTickerProviderStateMixin {
  DataStore store = DataStore();
  TabController _tabController;
  ScrollController _hideButtonController;
  bool _isVisible = true;
  bool _isLoading = false;
  double top = 210.0;
  List<dynamic> books = [];
  List<dynamic> popularBooks = [];
  List<dynamic> bookrandom = [];
  List<dynamic> booknew = [];
  List<EventModel> events = [];
  Future<Null> removeSharedPrefs() async {
    store.clearData();
    store.setDataString('name', 'Tidak ditemukan');
  }

  Future<String> getBook() async {
    setState(() {
      _isLoading = true;
    });
    var tokenType = await store.getDataString('token_type');
    var accessToken = await store.getDataString('access_token');
    var res = await http.get(url('api/home'), headers: {
      'Authorization': tokenType + ' ' + accessToken,
      "Content-type": "application/json",
      "accept": "application/json",
    });
    var content = json.decode(res.body);
    if (res.statusCode == 401) {
      removeSharedPrefs();
      Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
        (Route<dynamic> route) => false
      );
    } else if (res.statusCode == 200 && content['status'] == 'success') {
      setState(() {
        books = content['books'];
        bookrandom = content['random'];
        booknew = content['booknew'];
        events = List<EventModel>.from(content['events'].map((item) => EventModel.fromJson(item)));
        popularBooks = content['booksPopular'];
        _isLoading = false;
      });
    } else {
      setState(() {
        _isLoading = false;
      });
    }
    return 'success!';
  }

  void getReadedBooks() async {
    int total = BlocProvider.of<GlobalBloc>(context).bookBloc.bookData.listContinue.length;
    if (total == 0) {
      await BlocProvider.of<GlobalBloc>(context).bookBloc.loadBooks();
    }
  }

  @override
  void initState() {
    super.initState();
    getBook();
    getReadedBooks();
    _tabController = TabController(vsync: this, length: 3);
    _isVisible = true;
    _hideButtonController = new ScrollController();
    _hideButtonController.addListener(() {
      // debugPrint("listener");
      if (_hideButtonController.position.userScrollDirection == ScrollDirection.reverse) {
        setState(() {
          _isVisible = false;
          // debugPrint("**** $_isVisible up");
        });
      }
      if (_hideButtonController.position.userScrollDirection == ScrollDirection.forward) {
        setState(() {
          _isVisible = true;
          // debugPrint("**** $_isVisible down");
        });
      }
    });
  }

  

  Future<String> senddata(String pointerBook, String pointerEvent, String gambar) async {
    if (pointerBook == 'kosong') {
      store.setDataString('EventPointer', pointerEvent);

      Navigator.push(context,
          MaterialPageRoute(builder: (BuildContext context) => DetailImage(image: gambar,)));
    } else {
      store.setDataString('BookPointer', pointerBook);

      Navigator.push(context,
          MaterialPageRoute(builder: (BuildContext context) => DetailBook()));
    }

    return 'success!';
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    MediaQueryData mediaQD = MediaQuery.of(context);
    ThemeData theme = Theme.of(context);
    return WillPopScope(
        onWillPop: () async => showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(10.0)),
                  content: new Text(
                    MyLocalization.of(context).closeAppQuestion,
                    textAlign: TextAlign.left,
                  ),
                  actions: <Widget>[
                    FlatButton(
                      child: Text(
                        MyLocalization.of(context).no,
                        style: TextStyle(color: Colors.grey),
                      ),
                      onPressed: () => Navigator.of(context).pop(false),
                    ),
                    FlatButton(
                      child: new Text(
                        MyLocalization.of(context).yes,
                        style: TextStyle(color: theme.primaryColor),
                      ),
                      onPressed: () => Navigator.of(context).pop(true),
                    ),
                  ],
                );
              },
            ),
        child: _isLoading == true
            ? Scaffold(
                body: Stack(
                  children: [
                    CustomScrollView(
                      controller: _hideButtonController,
                      slivers: <Widget>[
                        SliverAppBar(
                          pinned: true,
                          floating: true,
                          snap: true,
                          elevation: 6,
                          backgroundColor: theme.primaryColor,
                          title: MyAppBar(
                            closed: top.ceilToDouble() > 85.0 ? false : true,
                          ),
                          iconTheme: IconThemeData(color: Colors.white),
                          actionsIconTheme: IconThemeData(color: Colors.white),
                          expandedHeight: 170.0,
                          flexibleSpace: LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
                            top = constraints.biggest.height;
                            return FlexibleSpaceBar(
                              background: Container(
                                child: Center(
                                  child: CircularProgressIndicator(),
                                )
                              )
                            );
                          }),
                        ),
                        SliverList(
                          delegate: SliverChildListDelegate([
                            Container(
                              width: mediaQD.size.width / 1.2,
                              height: mediaQD.size.width / 2,
                              alignment: Alignment.center,
                              child: CircularProgressIndicator(),
                            )
                          ])
                        )
                      ]
                    )
                  ]
                )
              )
            : StreamBuilder(
                stream:
                    BlocProvider.of<GlobalBloc>(context).bookBloc.bookStream,
                builder: (context, snapshot) {
                  // BookData dataBook =
                  //     BlocProvider.of<GlobalBloc>(context).bookBloc.bookData;
                  return Scaffold(
                      body: Stack(
                    children: [
                      CustomScrollView(
                        controller: _hideButtonController,
                        slivers: <Widget>[
                          SliverAppBar(
                            pinned: true,
                            floating: true,
                            snap: true,
                            elevation: 6,
                            backgroundColor: theme.primaryColor,
                            title: MyAppBar(
                              closed: top.ceilToDouble() > 85.0 ? false : true,
                            ),
                            iconTheme: IconThemeData(color: Colors.white),
                            actionsIconTheme:
                                IconThemeData(color: Colors.white),
                            expandedHeight: 170.0,
                            flexibleSpace: LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
                              top = constraints.biggest.height;
                              return FlexibleSpaceBar(
                                background: AppBarAds(),
                              );
                            }),
                          ),
                          SliverList(
                            delegate: SliverChildListDelegate([
                              Container(
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Container(
                                    margin: EdgeInsets.only(top: 20.0),
                                    width:
                                        mediaQD.size.width / 1.1,
                                    child: Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text(
                                        MyLocalization.of(context).homeEvents,
                                        style: GoogleFonts.quattrocentoSans(
                                          fontSize: 18,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              events.length > 1 ? Container(
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Container(
                                    width: mediaQD.size.width,
                                    child: CarouselSlider.builder(
                                      options: CarouselOptions(
                                        autoPlay: true,
                                        height: mediaQD.size.height / 3.8,
                                        viewportFraction: 0.95,
                                        autoPlayAnimationDuration: Duration(milliseconds: 800),
                                        autoPlayCurve: Curves.fastOutSlowIn,
                                      ),
                                      itemCount: events.length,
                                      itemBuilder: (BuildContext context, int index) => HomeSlider(
                                        events: events[index],
                                        action: () {
                                          senddata('kosong', events[index].rowPointer, host + 'upload/events/' + events[index].image);
                                        }
                                      ),
                                    ),
                                  ),
                                ),
                              ) : events.length == 1 ? Container(
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Container(
                                    width: mediaQD.size.width / 1.1,
                                    height: mediaQD.size.height / 3.8,
                                    child: HomeSlider(
                                      events: events[0],
                                      action: () {
                                        senddata('kosong', events[0].rowPointer, host + 'upload/events/' + events[0].image);
                                      }
                                    ),
                                  ),
                                ),
                              ) : Container(),
                              Container(
                                  padding: EdgeInsets.only(top: 17.0),
                                  child: Align(
                                    alignment: Alignment.center,
                                    child: Container(
                                      height: 35,
                                      width: mediaQD.size.width /
                                          1.1,
                                      child: TabBar(
                                          controller: _tabController,
                                          isScrollable: true,
                                          labelStyle: GoogleFonts.quattrocentoSans(
                                            fontSize: 18,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.black
                                          ),
                                          unselectedLabelStyle:
                                              GoogleFonts.quattrocentoSans(
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.w400,
                                                  color: Color(0xFF898989)),
                                          labelPadding: EdgeInsets.only(
                                              left: 0.0, right: 13.0),
                                          indicatorPadding: EdgeInsets.only(
                                              left: 0.0, right: 13.0),
                                          labelColor: Colors.black,
                                          unselectedLabelColor:
                                              Color(0xFF898989),
                                          indicatorSize:
                                              TabBarIndicatorSize.label,
                                          tabs: [
                                            Tab(
                                              child: Align(
                                                alignment: Alignment.center,
                                                child: Text(MyLocalization.of(context).homeAll,),
                                              ),
                                            ),
                                            Tab(
                                              child: Align(
                                                alignment: Alignment.center,
                                                child: Text(MyLocalization.of(context).homeRecomended,),
                                              ),
                                            ),
                                            Tab(
                                              child: Align(
                                                alignment: Alignment.center,
                                                child: Text(MyLocalization.of(context).homePopularBooks,),
                                              ),
                                            ),
                                          ]),
                                    ),
                                  )),
                              Container(
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Container(
                                    height: mediaQD.size.width / 1.7,
                                    child: new TabBarView(
                                      physics: NeverScrollableScrollPhysics(),
                                      controller: _tabController,
                                      children: <Widget>[
                                        TabsAll(
                                          data: booknew,
                                        ),
                                        TabsAll(
                                          data: books,
                                        ),
                                        TabsAll(
                                          data: popularBooks,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.only(top: 20.0, left: 16.0, right: 16.0, bottom: 5.0),
                                width: mediaQD.size.width / 1.1,
                                child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    MyLocalization.of(context).homeMayAlsoLike,
                                    style: GoogleFonts.quattrocentoSans(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(bottom: 30.0),
                                height: mediaQD.size.width / 1.4,
                                width: mediaQD.size.width,
                                child: CardBookImage(
                                  data: bookrandom,
                                ),
                              ),
                            ]),
                          ),
                        ],
                      ),
                      AnimatedOpacity(
                        opacity: _isVisible ? 1.0 : 0.0,
                        duration: Duration(milliseconds: 200),
                        child: AnimatedContainer(
                          padding: EdgeInsets.only(bottom: _isVisible ? 10.0 : 0.0,),
                          duration: Duration(milliseconds: 200),
                          child: BottomNav(),
                        )
                      ),
                    ],
                  )
                );
              }
            )
          );
  }
}
