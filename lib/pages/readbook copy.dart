/* import 'dart:convert';
import 'package:buddhist_educenter/components/widgets/pdfAnjay.dart';
import 'package:buddhist_educenter/model/bookModelApi.dart';
import 'package:http/http.dart' as http;
import 'package:buddhist_educenter/env.dart';
import 'package:flutter/material.dart';
import 'package:flutter_plugin_pdf_viewer/flutter_plugin_pdf_viewer.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ReadBook extends StatefulWidget {
  @override
  _ReadBookState createState() => _ReadBookState();
}

class _ReadBookState extends State<ReadBook> {
  bool _isLoading = true;
  PDFDocument document;
  int halamanBuku;
  BookModelApi detailBook;
  var data;
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  Future<String> getBook() async {
    setState(() {
      _isLoading = true;
    });
    SharedPreferences prefs = await SharedPreferences.getInstance();

    var tokenType = store.getDataString('token_type');
    var accessToken = store.getDataString('access_token');
    var pointer = store.getDataString('BookPointer');
    var res = await http.get(url('api/book/' + pointer),
        headers: {'Authorization': tokenType + ' ' + accessToken});

    setState(() {
      var content = json.decode(res.body);
      debugPrint(content['book']['title'].toString());

      detailBook = BookModelApi.fromJson(content['book']);
      data = content['book'];
      loadDocument(gcsurl + content['book']['book'],
          content['book']['row_pointer']);
    });
    document = await PDFDocument.fromURL(
        gcsurl+ detailBook.book);
    halamanBuku = prefs.getInt(detailBook.rowPointer) ?? 1;

    setState(() {
      if (halamanBuku == 1) {
        addViewCount();
      } else {
        _isLoading = false;
      }
    });
    return 'success!';
  }

  Future<String> addViewCount() async {
    setState(() {
      _isLoading = true;
    });
    SharedPreferences prefs = await SharedPreferences.getInstance();

    var tokenType = store.getDataString('token_type');
    var accessToken = store.getDataString('access_token');
    var pointer = store.getDataString('BookPointer');
    var res = await http.get(url('api/addViewCount/' + pointer),
        headers: {'Authorization': tokenType + ' ' + accessToken});
    debugPrint(res.toString());

    // setState(() {
    //   var content = json.decode(res.body);
    //   debugPrint(content['book']['title'].toString());

    //   detailBook = BookModelApi.fromJson(content['book']);
    //   data = content['book'];
    //   loadDocument(host + 'upload/book/files/' + content['book']['book'],
    //       content['book']['row_pointer']);
    // });
    // document = await PDFDocument.fromURL(
    //     host + 'upload/book/files/' + detailBook.book);
    // halamanBuku = prefs.getInt(detailBook.rowPointer) ?? 1;

    setState(() {
      _isLoading = false;
    });
    return 'success!';
  }

  @override
  void initState() {
    super.initState();
    getBook();
  }

  loadDocument(bookurl, pointerbook) async {
    final SharedPreferences prefs = await _prefs;
    document = await PDFDocument.fromURL(bookurl);
    halamanBuku = prefs.getInt(pointerbook) ?? 1;
    setState(() => _isLoading = false);
  }

  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);
    return Scaffold(
      backgroundColor: theme.backgroundColor,
      appBar: AppBar(
        title: Text(_isLoading ? '...' : detailBook.title),
        backgroundColor: theme.primaryColor,
      ),
      body: Center(
          child: _isLoading
              ? Center(child: CircularProgressIndicator())
              : PDFAnjay(
                  document: document,
                  halaman: halamanBuku,
                  idBuku: detailBook.rowPointer,
                  bookModel: detailBook,
                )),
    );
  }
}
 */