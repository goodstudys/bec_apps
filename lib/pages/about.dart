import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class AboutPage extends StatefulWidget {
  @override
  _AboutPageState createState() => _AboutPageState();
}

class _AboutPageState extends State<AboutPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text('About'),
        centerTitle: true,
      ),
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.only(left: 20.0, right: 20.0, top: 10.0, bottom: 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                padding: EdgeInsets.symmetric(horizontal: 30.0),
                child: Image.asset('assets/img/logo_bec.jpg'),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: Text(
                  'YAYASAN DHARMA RANGSI',
                  textAlign: TextAlign.center,
                  style: GoogleFonts.quattrocentoSans(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    color: Colors.black
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 5.0),
                child: Text(
                  'Jl. HR.Muhammad 179\nKomplek Pertokoan Surya Inti Permata II Blok D 8-9\nSurabaya - Indonesia\nTel: (+6231) 734 5135, Fax: (+6231) 734 5143\nEmail: becsurabaya@yahoo.com, becsurabaya@gmail.com',
                  textAlign: TextAlign.center,
                  style: GoogleFonts.quattrocentoSans(
                    fontSize: 14,
                    color: Colors.black54,
                    height: 1.5
                  ),
                ),
              ),
              Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                  side: BorderSide(color: Colors.grey[600])
                ),
                child: Row(
                  children: [

                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
