import 'dart:async';
import 'package:buddhist_educenter/bloc/global/BlocProvider.dart';
import 'package:buddhist_educenter/bloc/global/GlobalBloc.dart';
import 'package:buddhist_educenter/locale/my_localization.dart';
import 'package:buddhist_educenter/locale/provider_localization.dart';
import 'package:buddhist_educenter/pages/home.dart';
import 'package:buddhist_educenter/pages/language.dart';
import 'package:buddhist_educenter/storage/storage.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  Timer timer;
  String _name;
  DataStore store = new DataStore();

  void navigationPage() {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => LanguagePage()),
    );
  }

  void dashboard() {
    Navigator.pushReplacement(context,
        MaterialPageRoute(builder: (BuildContext context) => HomePage()));
  }

  Future<Null> getSharedPrefs() async {
    /* dynamic ab = store.getDataString('listjsonreading');
    if (ab != null) {
      dynamic cd = jsonDecode(ab);
      List<ReadingBookModel> listreading = List<ReadingBookModel>.from(
          cd.map((item) => ReadingBookModel.fromJson(item)));
      BlocProvider.of<GlobalBloc>(context).bookBloc.sendListBook(listreading);
    } */
    _name = await store.getDataString("name");
    String localeid1 = await store.getDataString('localeid1') ?? 'kosong';
    String localeid2 = await store.getDataString('localeid2') ?? 'kosong';
    debugPrint(_name);
    if (localeid1 != 'kosong' || localeid2 != 'kosong') {
      MyLocalization.load(Locale(localeid1, localeid2));
      store.setDataString('localeid1', localeid1);
      store.setDataString('localeid2', localeid2);
      Provider.of<ProviderLocalization>(context, listen: false);
      // Provider.of<ProviderLocalization>(context)
      //     .setLanguage(Locale(localeid1, localeid2));
    }
    await BlocProvider.of<GlobalBloc>(context).bookBloc.loadBooks();
    if (_name == 'Tidak ditemukan') {
      timer = Timer.periodic(
          Duration(seconds: 2),
          (Timer t) => setState(() {
                navigationPage();
              }));
    } else {
      timer = Timer.periodic(
          Duration(seconds: 2),
          (Timer t) => setState(() {
                dashboard();
              }));
    }
  }

  @override
  void initState() {
    super.initState();
    getSharedPrefs();
  }

  @override
  void dispose() {
    super.dispose();
    timer.cancel();
  }
  // @override
  // void initState() {
  //   super.initState();
  //   Timer(
  //       Duration(seconds: 10),
  //       () => Navigator.pushReplacement(context,
  //           MaterialPageRoute(builder: (BuildContext context) => LoginPage())));
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        brightness: Brightness.light,
        elevation: 0,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(horizontal: 30.0),
            child: Image.asset('assets/img/logo_bec.jpg'),
          ),
          Padding(
            padding: EdgeInsets.only(top: 20.0),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 23, right: 23, bottom: 23),
            child: CircularProgressIndicator(),
          ),
        ],
      ),
    );
  }
}
