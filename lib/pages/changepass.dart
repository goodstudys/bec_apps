import 'dart:convert';
import 'package:buddhist_educenter/storage/storage.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:sweetalert/sweetalert.dart';
import '../env.dart';

class ChangePass extends StatefulWidget {
  @override
  _ChangePassState createState() => _ChangePassState();
}

class _ChangePassState extends State<ChangePass> {
  DataStore store = DataStore();

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final TextEditingController _passLama = TextEditingController();
  final TextEditingController _passBaru = TextEditingController();

  final FocusNode myFocusNodeConfirmPassword = FocusNode();
  final FocusNode myFocusNodePassword = FocusNode();

  bool _obscureTextLogin = true;
  bool _obscureTextLogin2 = true;
  void _toggleLogin2() {
    setState(() {
      _obscureTextLogin = !_obscureTextLogin;
    });
  }

  void _toggleLogin() {
    setState(() {
      _obscureTextLogin = !_obscureTextLogin;
    });
  }

  List data;
  bool isLoading = false;
  String getType;

  submit() async {
    var tokenType = await store.getDataString('token_type');
    var accessToken = await store.getDataString('access_token');
    String id = await store.getDataString('id');
    var res = await http.post(url('api/changePassword/$id'), headers: {
      'Authorization': '$tokenType $accessToken',
      "Content-Type": "application/x-www-form-urlencoded"
    }, body: {
      'cur_pass': _passLama.text,
      'password': _passBaru.text,
    });

    setState(() {
      var data = json.decode(res.body);
      debugPrint(data.toString());
      if (data['status'] == 'success') {
        SweetAlert.show(context, onPress: (isConfirm) {
          if (isConfirm) {
            Navigator.pop(context);
            Navigator.pop(context);
          } else {
            Navigator.pop(context);
            Navigator.pop(context);
          }
          return false;
        }, title: data['message'], style: SweetAlertStyle.success);
        // showDialog(
        //     context: context,
        //     builder: (context) {
        //       Future.delayed(Duration(seconds: 1), () {
        //         Navigator.of(context).pop(true);
        //         Navigator.of(context).pop(true);
        //       });
        //       return DialogSuccess(
        //         type: true,
        //         txtHead: 'success !!!',
        //         txtSub: "Password Baru Tersimpan",
        //       );
        //     });
      } else if (data['status'] == 'error') {
        SweetAlert.show(context, onPress: (isConfirm) {
          if (isConfirm) {
            Navigator.pop(context);
          } else {
            Navigator.pop(context);
          }
          return false;
        }, title: data['message'], style: SweetAlertStyle.error,);
        // showDialog(
        //     context: context,
        //     builder: (context) {
        //       Future.delayed(Duration(seconds: 1), () {
        //         Navigator.of(context).pop(true);
        //       });
        //       return DialogSuccess(
        //         type: false,
        //         txtHead: data['status'],
        //         txtSub: data['message'],
        //       );
        //     });
      }
    });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Change Password'),
      ),
      body: Stack(
        children: <Widget>[
          Container(
            child: Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(
                        top: 0.0,
                        bottom: 0.0,
                        left: 10.0,
                        right: 10.0),
                    child: TextFormField(
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      focusNode: myFocusNodePassword,
                      controller: _passLama,
                      obscureText: _obscureTextLogin,
                      validator: (val) {
                        if (val.isEmpty) return 'Empty';
                        return null;
                      },
                      style: GoogleFonts.quattrocentoSans(
                          fontSize: 12.0,
                          color: Colors.black),
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        // icon: Icon(
                        //   FontAwesomeIcons.lock,
                        //   size: 22.0,
                        //   color: Colors.black,
                        // ),
                        hintText: "Masukkan Password Lama",
                        hintStyle: GoogleFonts.quattrocentoSans(fontSize: 12.0),
                        suffixIcon: GestureDetector(
                          onTap: _toggleLogin,
                          child: Icon(
                            _obscureTextLogin
                                ? FontAwesomeIcons.eye
                                : FontAwesomeIcons.eyeSlash,
                            size: 12.0,
                            color: Colors.black,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Divider(
                    thickness: 1.0,
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: 0.0,
                        bottom: 0.0,
                        left: 10.0,
                        right: 10.0),
                    child: TextFormField(
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      focusNode: myFocusNodeConfirmPassword,
                      controller: _passBaru,
                      obscureText: _obscureTextLogin2,
                      validator: (val) {
                        if (val.isEmpty) return 'Empty';
                        return null;
                      },
                      style: GoogleFonts.quattrocentoSans(
                          fontSize: 12.0,
                          color: Colors.black),
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        // icon: Icon(
                        //   FontAwesomeIcons.lock,
                        //   size: 22.0,
                        //   color: Colors.black,
                        // ),
                        hintText: "Masukkan Password Baru",
                        hintStyle: GoogleFonts.quattrocentoSans(fontSize: 12.0),
                        suffixIcon: GestureDetector(
                          onTap: _toggleLogin2,
                          child: Icon(
                            _obscureTextLogin2
                                ? FontAwesomeIcons.eye
                                : FontAwesomeIcons.eyeSlash,
                            size: 12.0,
                            color: Colors.black,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Divider(
                    thickness: 1.0,
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            bottom: 13.0,
            left: 13.0,
            right: 13.0,
            child: InkWell(
              onTap: () {
                final formState = _formKey.currentState;
                if (formState.validate()) {
                  submit();
                }
              },
              child: Container(
                height: 55.0,
                width: 300.0,
                decoration: BoxDecoration(
                    color: theme.primaryColor,
                    borderRadius: BorderRadius.all(Radius.circular(40.0))),
                child: Center(
                  child: Text(
                    "Submit",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w700,
                        fontSize: 16.5,
                        letterSpacing: 1.0),
                  ),
                ),
              ),
            ),
          ),
        ],
      )
    );
  }
}
