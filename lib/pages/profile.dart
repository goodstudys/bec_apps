import 'package:buddhist_educenter/bloc/global/BlocProvider.dart';
import 'package:buddhist_educenter/bloc/global/GlobalBloc.dart';
import 'package:buddhist_educenter/components/widgets/webview.dart';
import 'package:buddhist_educenter/locale/my_localization.dart';
import 'package:buddhist_educenter/model/readbookModel.dart';
import 'package:buddhist_educenter/pages/changepass.dart';
import 'package:buddhist_educenter/pages/language.dart';
import 'package:mime/mime.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:buddhist_educenter/storage/storage.dart';
import 'package:image_picker/image_picker.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'package:buddhist_educenter/env.dart';
import 'dart:convert';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  DataStore store = DataStore();
  final ImagePicker _picker = ImagePicker();
  String data;
  String name, email, phone, photo, userPointer, accessToken, tokenType;
  bool isLoading = false;
  bool _isUploading = false;
  PickedFile _image;
  var profile;
  Map<String, String> baseHeaders = {
    "Content-type": "application/json",
    "accept": "application/json",
  };

  Future<dynamic> uploadImage(PickedFile image) async {
    setState(() {
      _isUploading = true;
    });
    baseHeaders.addAll({
      "Authorization": tokenType + ' ' + accessToken,
    });
    final mimeTypeData =
        lookupMimeType(image.path, headerBytes: [0xFF, 0xD8]).split('/');
    final request = http.MultipartRequest(
        'POST', Uri.parse(url('api/updatePhoto/' + userPointer)));
    request.headers.addAll(baseHeaders);
    // Attach the file in the request
    final file = await http.MultipartFile.fromPath('image', image.path,
        contentType: MediaType(mimeTypeData[0], mimeTypeData[1]));

    request.fields['ext'] = mimeTypeData[1];
    request.fields['data'] = '{"users": {"crmcode": "aaa"}}';
    request.files.add(file);
    try {
      final streamedResponse = await request.send();
      final response = await http.Response.fromStream(streamedResponse);
      var content = json.decode(response.body);
      final int statusCode = response.statusCode;
      if (statusCode == 204) {
        throw new Exception("No record found.");
      } else if (statusCode == 400) {
        throw new Exception("Invalid request parameter");
      } else if (statusCode == 401) {
        throw new Exception(
            "Unauthorized Access, please check your credentials");
      } else if (statusCode == 500) {
        throw new Exception("Server error");
      } else if (json == null) {
        throw new Exception("Content not found");
      }
      debugPrint(content['status']);
      setState(() {
        if (content['status'] == 'success') {
          store.setDataString('photo', content['imageName']);
          photo = content['imageName'];
          _isUploading = false;
        }
      });

      return debugPrint(content);
    } catch (e) {
      debugPrint(e);
      return null;
    }
  }

  Future<Null> logOut() async {
    List<ReadingBookModel> readingBooks = BlocProvider.of<GlobalBloc>(context).bookBloc.bookData.listContinue;
    store.clearData();
    store.setDataString('name', 'Tidak ditemukan');
    // keep reading list according to user
    String email = await store.getDataString('email') ?? '';
    String phone = await store.getDataString('phone') ?? '';
    String id = 'bookmark-' + email + '-' + phone;
    String listJson = jsonEncode(readingBooks);
    store.setDataString(id, listJson);
  }

  Future getImage(ImageSource media) async {
    try {
      final image = await _picker.getImage(
        source: media,
        maxWidth: 800,
      );
      setState(() {
        _image = image;
      });
      if (_image != null) {
        uploadImage(_image);
        // Closes the bottom sheet
        // Navigator.pop(context);
      }
    } catch (error) {
      // Navigator.pop(context);
      debugPrint('Failed!  ' + error.toString());
    }
  }

  void myAlert() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
            title: Text(MyLocalization.of(context).textChooseMedia),
            content: Container(
              height: MediaQuery.of(context).size.height / 6,
              child: Column(
                children: <Widget>[
                  FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                      getImage(ImageSource.gallery);
                    },
                    child: Row(
                      children: <Widget>[
                        Icon(Icons.image),
                        Text(MyLocalization.of(context).textFromGallery),
                      ],
                    ),
                  ),
                  FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                      getImage(ImageSource.camera);
                    },
                    child: Row(
                      children: <Widget>[
                        Icon(Icons.camera),
                        Text(MyLocalization.of(context).textFromCamera),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

  Future<String> getData() async {
    String getname = await store.getDataString('name') ?? '';
    String getemail = await store.getDataString('email') ?? '';
    String getphone = await store.getDataString('phone') ?? '';
    String getphoto = await store.getDataString('photo') ?? '';
    String getuserPointer = await store.getDataString('userPointer') ?? '';
    String gettokenType = await store.getDataString('token_type') ?? '';
    String getaccessToken = await store.getDataString('access_token') ?? '';
    setState(() {
      isLoading = true;
      name = getname;
      email = getemail;
      phone = getphone;
      photo = getphoto;
      userPointer = getuserPointer;
      tokenType = gettokenType;
      accessToken = getaccessToken;
    });
    setState(() {
      debugPrint(
          'nama : $name , email : $email , phone : $phone , photo : $photo , userPointer : $userPointer , ');
    });
    return 'success!';
  }

  @override
  void initState() {
    super.initState();
    getData().then((s) => isLoading = true);
  }

  @override
  Widget build(BuildContext context) {
    MediaQueryData mediaQD = MediaQuery.of(context);
    ThemeData theme = Theme.of(context);
    return Scaffold(
        appBar: AppBar(
          backgroundColor: theme.primaryColor,
          centerTitle: true,
          title: Text(MyLocalization.of(context).bottomNavigationAccount,
              style: GoogleFonts.quattrocentoSans(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 20)),
        ),
        body: isLoading
            ? Column(
                children: <Widget>[
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        color: theme.primaryColor,
                        width: mediaQD.size.width,
                        padding: EdgeInsets.only(top: 30, bottom: 20.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Stack(
                              children: [
                                _isUploading
                                    ? Container(
                                        padding: EdgeInsets.only(
                                            top: 6.0, right: 6.0),
                                        child: ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(45.0),
                                            child: Container(
                                                width: 75,
                                                height: 75,
                                                padding: EdgeInsets.all(20.0),
                                                color: Colors.white,
                                                child:
                                                    CircularProgressIndicator())),
                                      )
                                    : Container(
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(75),
                                          image: DecorationImage(
                                            image: photo == 'null' ? AssetImage('assets/img/placeholder.png')
                                              : NetworkImage(host + 'upload/user/' + photo),
                                            fit: BoxFit.cover,
                                            alignment: Alignment.center,
                                          ),
                                        ),
                                        height: 100,
                                        width: 100,
                                      ),
                                Positioned(
                                  top: 0.0,
                                  right: 0.0,
                                  child: InkWell(
                                    onTap: () {
                                      myAlert();
                                    },
                                    child: Container(
                                      padding: EdgeInsets.all(6.0),
                                      decoration: BoxDecoration(
                                        color: theme.accentColor,
                                        shape: BoxShape.circle,
                                      ),
                                      constraints: BoxConstraints(
                                        minWidth: 20.0,
                                        minHeight: 20.0,
                                      ),
                                      child: Center(
                                        child: Icon(
                                          Icons.camera_alt,
                                          color: Colors.white,
                                          size: 20,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Container(
                              width: mediaQD.size.width / 1.1,
                              padding: EdgeInsets.only(
                                  top: 8.0, left: 10.0, right: 10.0),
                              child: Align(
                                alignment: Alignment.center,
                                child: Text(name,
                                    softWrap: true,
                                    style: GoogleFonts.quattrocentoSans(
                                        fontSize: 24,
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold)),
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(top: 13.0),
                              width: mediaQD.size.width / 1.1,
                              child: Align(
                                  alignment: Alignment.center,
                                  child: Text(
                                    phone,
                                    style: GoogleFonts.quattrocentoSans(
                                        fontSize: 14, color: Colors.grey[100]),
                                  )),
                            ),
                            Container(
                              width: mediaQD.size.width / 1.1,
                              child: Align(
                                  alignment: Alignment.center,
                                  child: Text(
                                    email,
                                    style: GoogleFonts.quattrocentoSans(
                                        fontSize: 14, color: Colors.grey[100]),
                                  )),
                            ),
                            Padding(padding: EdgeInsets.only(top: 13.0)),
                          ],
                        ),
                      ),
                      // Container(
                      //   child: Align(
                      //     alignment: Alignment.center,
                      //     child: Container(
                      //       width: mediaQD.size.width / 1.1,
                      //       height: 50,
                      //       child: Align(
                      //           alignment: Alignment.centerLeft,
                      //           child: Text(
                      //             'Your Subscribe',
                      //             style: GoogleFonts.quattrocentoSans(
                      //                 fontWeight: FontWeight.bold,
                      //                 fontSize: 16,
                      //                 color: Colors.black),
                      //           )),
                      //     ),
                      //   ),
                      // ),
                      // Container(
                      //   child: Align(
                      //     alignment: Alignment.center,
                      //     child: Container(
                      //       width: mediaQD.size.width / 1.1,
                      //       child: Card(
                      //         elevation: 2.0,
                      //         color: Colors.white,
                      //         shape: RoundedRectangleBorder(
                      //           borderRadius: BorderRadius.circular(10.0),
                      //         ),
                      //         child: Stack(
                      //           children: <Widget>[
                      //             Container(
                      //               decoration: BoxDecoration(
                      //                 color: Color(0xff013db7),
                      //                 borderRadius: BorderRadius.only(
                      //                     topLeft: Radius.circular(10),
                      //                     bottomLeft: Radius.circular(10)),
                      //               ),
                      //               height: 120,
                      //               width: mediaQD.size.width /
                      //                   3.8,
                      //               child: Align(
                      //                 alignment: Alignment.center,
                      //                 child: Column(
                      //                   crossAxisAlignment:
                      //                       CrossAxisAlignment.center,
                      //                   mainAxisAlignment:
                      //                       MainAxisAlignment.center,
                      //                   children: [
                      //                     Text(
                      //                       '1',
                      //                       style: GoogleFonts.quattrocentoSans(
                      //                           fontWeight: FontWeight.bold,
                      //                           fontSize: 40,
                      //                           color: Colors.white),
                      //                     ),
                      //                     Text(
                      //                       'Year',
                      //                       style: GoogleFonts.quattrocentoSans(
                      //                           fontWeight: FontWeight.bold,
                      //                           fontSize: 24,
                      //                           color: Colors.white),
                      //                     ),
                      //                   ],
                      //                 ),
                      //               ),
                      //             ),
                      //             Container(
                      //                 height: 120,
                      //                 width:
                      //                     mediaQD.size.width /
                      //                         1.125,
                      //                 child: Row(
                      //                   mainAxisAlignment:
                      //                       MainAxisAlignment.start,
                      //                   crossAxisAlignment:
                      //                       CrossAxisAlignment.start,
                      //                   children: <Widget>[
                      //                     Container(
                      //                         width: mediaQD
                      //                                 .size
                      //                                 .width /
                      //                             3.8,
                      //                         child: Text('')),
                      //                     Container(
                      //                       width: mediaQD
                      //                               .size
                      //                               .width /
                      //                           1.8,
                      //                       child: Padding(
                      //                         padding:
                      //                             const EdgeInsets.all(13.0),
                      //                         child: Stack(
                      //                           children: <Widget>[
                      //                             Column(
                      //                               crossAxisAlignment:
                      //                                   CrossAxisAlignment
                      //                                       .start,
                      //                               mainAxisAlignment:
                      //                                   MainAxisAlignment
                      //                                       .center,
                      //                               children: <Widget>[
                      //                                 Padding(
                      //                                   padding:
                      //                                       const EdgeInsets
                      //                                               .only(
                      //                                           bottom: 10.0),
                      //                                   child: Text(
                      //                                     '',
                      //                                     maxLines: 1,
                      //                                     overflow:
                      //                                         TextOverflow
                      //                                             .ellipsis,
                      //                                     textAlign:
                      //                                         TextAlign.left,
                      //                                     style: GoogleFonts
                      //                                         .montserrat(
                      //                                             fontSize:
                      //                                                 20,
                      //                                             fontWeight:
                      //                                                 FontWeight
                      //                                                     .w500,
                      //                                             color: Colors
                      //                                                 .black),
                      //                                   ),
                      //                                 ),
                      //                                 Text(
                      //                                   'Enjoy One Year all books reading',
                      //                                   maxLines: 4,
                      //                                   overflow: TextOverflow
                      //                                       .ellipsis,
                      //                                   style: GoogleFonts
                      //                                       .montserrat(
                      //                                           fontSize: 12,
                      //                                           color: Colors
                      //                                               .black),
                      //                                 ),
                      //                               ],
                      //                             ),
                      //                           ],
                      //                         ),
                      //                       ),
                      //                     ),
                      //                   ],
                      //                 )),
                      //           ],
                      //         ),
                      //       ),
                      //     ),
                      //   ),
                      // ),
                      // Container(
                      //   child: Align(
                      //     alignment: Alignment.center,
                      //     child: Container(
                      //       width: mediaQD.size.width / 1.1,
                      //       child: Card(
                      //         elevation: 2.0,
                      //         color: Colors.white,
                      //         shape: RoundedRectangleBorder(
                      //           borderRadius: BorderRadius.circular(10.0),
                      //         ),
                      //         child: Container(
                      //             // height: 50,
                      //             // width: mediaQD.size.width / 1.,
                      //             // color: Colors.red,
                      //             padding: EdgeInsets.all(13.0),
                      //             child: Row(
                      //               mainAxisAlignment:
                      //                   MainAxisAlignment.spaceBetween,
                      //               crossAxisAlignment:
                      //                   CrossAxisAlignment.center,
                      //               children: <Widget>[
                      //                 Row(
                      //                   children: [
                      //                     Text('Plane Active : ',
                      //                         style: GoogleFonts.quattrocentoSans(
                      //                             fontSize: 10,
                      //                             color: Colors.black)),
                      //                     Text('1 Jan,2021',
                      //                         style: GoogleFonts.quattrocentoSans(
                      //                             fontSize: 10,
                      //                             fontWeight: FontWeight.bold,
                      //                             color: Colors.black))
                      //                   ],
                      //                 ),
                      //                 Row(
                      //                   children: [
                      //                     Text('Expiry Date',
                      //                         style: GoogleFonts.quattrocentoSans(
                      //                             fontSize: 10,
                      //                             color: Colors.black)),
                      //                     Text('30 Dec, 2021',
                      //                         style: GoogleFonts.quattrocentoSans(
                      //                             fontSize: 10,
                      //                             fontWeight: FontWeight.bold,
                      //                             color: Colors.black))
                      //                   ],
                      //                 ),
                      //               ],
                      //             )),
                      //       ),
                      //     ),
                      //   ),
                      // ),
                    ],
                  ),
                  SizedBox(
                    height: 2.0,
                  ),
                  ButtonTheme(
                      minWidth: mediaQD.size.width,
                      height: 50.0,
                      child: RaisedButton(
                        color: Colors.grey,
                        onPressed: () async {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      ChangePass()));
                        },
                        child: Text(
                          MyLocalization.of(context).profileChangePassword,
                          style: GoogleFonts.quattrocentoSans(
                              color: Colors.white, fontSize: 16),
                        ),
                      )),
                  SizedBox(
                    height: 2.0,
                  ),
                  ButtonTheme(
                      minWidth: mediaQD.size.width,
                      height: 50.0,
                      child: RaisedButton(
                        color: Colors.grey,
                        onPressed: () async {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      LanguagePage()));
                        },
                        child: Text(
                          MyLocalization.of(context).profileSelectLanguage,
                          style: GoogleFonts.quattrocentoSans(
                              color: Colors.white, fontSize: 16),
                        ),
                      )),
                  SizedBox(
                    height: 2.0,
                  ),
                  ButtonTheme(
                      minWidth: mediaQD.size.width,
                      height: 50.0,
                      child: RaisedButton(
                        color: Colors.grey,
                        onPressed: () async {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (BuildContext context) => ViewURL(
                                        url:
                                            'https://docs.becindonesia.org/termsconditions.html',
                                        title: 'Terms & Conditions',
                                      )));
                        },
                        child: Text(
                          MyLocalization.of(context).textTerms,
                          style: GoogleFonts.quattrocentoSans(
                              color: Colors.white, fontSize: 16),
                        ),
                      )),
                  SizedBox(
                    height: 2.0,
                  ),
                  ButtonTheme(
                      minWidth: mediaQD.size.width,
                      height: 50.0,
                      child: RaisedButton(
                        color: Colors.grey,
                        onPressed: () async {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (BuildContext context) => ViewURL(
                                        url:
                                            'https://docs.becindonesia.org/privacypolicy.html',
                                        title: 'Privacy Policy',
                                      )));
                        },
                        child: Text(
                          MyLocalization.of(context).profilePrivacy,
                          style: GoogleFonts.quattrocentoSans(
                              color: Colors.white, fontSize: 16),
                        ),
                      )),
                  SizedBox(
                    height: 2.0,
                  ),
                  ButtonTheme(
                    minWidth: mediaQD.size.width,
                    height: 50.0,
                    child: RaisedButton(
                      padding: EdgeInsets.symmetric(
                          horizontal: 20.0, vertical: 10.0),
                      color: Colors.grey,
                      onPressed: () async {
                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return AlertDialog(
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      new BorderRadius.circular(10.0)),
                              content: new Text(
                                'Are you sure you want to logout ?',
                                textAlign: TextAlign.left,
                              ),
                              actions: <Widget>[
                                FlatButton(
                                  child: Text(
                                    MyLocalization.of(context).no,
                                    style: TextStyle(color: Colors.grey),
                                  ),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                ),
                                FlatButton(
                                  child: new Text(
                                    MyLocalization.of(context).yes,
                                    style: TextStyle(color: theme.primaryColor),
                                  ),
                                  onPressed: () {
                                    logOut().then((value) => Navigator.of(context).pushAndRemoveUntil(
                                      MaterialPageRoute(builder: (BuildContext context) => LanguagePage()),
                                        (Route<dynamic> route) => false
                                      )
                                    );
                                  },
                                ),
                              ],
                            );
                          },
                        );
                      },
                      child: Text(
                        MyLocalization.of(context).profileSignout,
                        style: GoogleFonts.quattrocentoSans(
                            color: Colors.white, fontSize: 16),
                      ),
                    ),
                  )
                ],
              )
            : Container(
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              ));
  }
}
