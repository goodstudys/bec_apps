import 'package:buddhist_educenter/bloc/book/book.dart';
import 'package:buddhist_educenter/bloc/global/BlocProvider.dart';
import 'package:buddhist_educenter/bloc/global/GlobalBloc.dart';
import 'package:buddhist_educenter/components/widgets/inputSearch.dart';
import 'package:buddhist_educenter/locale/my_localization.dart';
import 'package:buddhist_educenter/model/bookModelApi.dart';
import 'package:buddhist_educenter/model/readbookModel.dart';
import 'package:buddhist_educenter/pages/readbook.dart';
import 'package:buddhist_educenter/storage/storage.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

class ContinueReading extends StatefulWidget {
  @override
  _ContinueReadingState createState() => _ContinueReadingState();
}

class _ContinueReadingState extends State<ContinueReading> {
  DataStore store = DataStore();
  TextEditingController editingController = TextEditingController();
  bool _isLoading = true;
  List<ReadingBookModel> duplicateItems = [];
  List<ReadingBookModel> items = [];

  void getreadingbook() async {
    setState(() {
      _isLoading = true;
    });
    items.clear();
    duplicateItems.clear();
    int total = BlocProvider.of<GlobalBloc>(context).bookBloc.bookData.listContinue.length;
    if (total == 0) {
      await BlocProvider.of<GlobalBloc>(context).bookBloc.loadBooks();
    }
    items.addAll(BlocProvider.of<GlobalBloc>(context).bookBloc.bookData.listContinue);
    duplicateItems.addAll(BlocProvider.of<GlobalBloc>(context).bookBloc.bookData.listContinue);
    setState(() {
      _isLoading = false;
    });
  }

  /* Future<String> senddataBook(String pointer) async {
    store.setDataString('BookPointer', pointer);

    Navigator.push(context,
        MaterialPageRoute(builder: (BuildContext context) => DetailBook()));
    return 'success!';
  } */

  @override
  void initState() {
    getreadingbook();
    super.initState();
  }

  void filterSearchResults(String query) {
    List<ReadingBookModel> dummySearchList = List<ReadingBookModel>();
    dummySearchList.addAll(duplicateItems);
    if (query.isNotEmpty) {
      List<ReadingBookModel> dummyListData = List<ReadingBookModel>();
      dummySearchList.forEach((item) {
        if (item.title.toLowerCase().contains(query.toLowerCase())) {
          dummyListData.add(item);
        }
      });
      setState(() {
        items.clear();
        items.addAll(dummyListData);
      });
      return;
    } else {
      setState(() {
        items.clear();
        items.addAll(duplicateItems);
      });
    }
  }

  removeRead(ReadingBookModel item) async {
    setState(() {
      _isLoading = true;
    });
    BlocProvider.of<GlobalBloc>(context).bookBloc.removeBookItem(item);
    items.clear();
    getreadingbook();
    setState(() {
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);
    MediaQueryData mediaQD = MediaQuery.of(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: theme.primaryColor,
        centerTitle: true,
        title: Text(
          MyLocalization.of(context).appbarContinue,
          style: GoogleFonts.quattrocentoSans(
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontSize: 20
          )
        ),
      ),
      body: SafeArea(
        child: !_isLoading ? ListView(
          children: <Widget>[
            Column(
              children: <Widget>[
                InputSearch(
                  lebar: 1.1,
                  controllerInput: editingController,
                  submitted: (value) {
                    filterSearchResults(value);
                  }
                ),
              ],
            ),
            StreamBuilder(
              stream: BlocProvider.of<GlobalBloc>(context).bookBloc.bookStream,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  BookData dataBook = snapshot.data;
                  // BookData dataBook = BlocProvider.of<GlobalBloc>(context).bookBloc.bookData;
                  return dataBook.listContinue.length > 0 ? Container(
                    width: mediaQD.size.width,
                    child: ListView.builder(
                      padding: EdgeInsets.only(top: 20.0, left: 15.0, right: 15.0),
                      shrinkWrap: true,
                      itemCount: items.length,
                      itemBuilder: (BuildContext context, int index) {
                        return InkWell(
                          onTap: () {
                            Navigator.push(context, MaterialPageRoute(
                              builder: (BuildContext context) => ReadBook(
                                bookData: BookModelApi(
                                  id: items[index].id,
                                  title: items[index].title,
                                  thumbnail: items[index].img,
                                  description: items[index].description,
                                  author: items[index].author,
                                  book: items[index].booklink,
                                  rowPointer: items[index].rowPointer
                                ),
                                bookPage: items[index].pageRead,
                              ))
                            ).then((value) => getreadingbook());
                            // senddataBook(dataBook.listContinue[index].rowPointer);
                          },
                          child: Card(
                            elevation: 2.0,
                            color: Colors.white,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            child: Stack(
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(10),
                                          bottomLeft: Radius.circular(10)
                                        ),
                                        image: DecorationImage(
                                          image: AssetImage('assets/img/placeholder.png'),
                                          fit: BoxFit.cover,
                                          alignment: Alignment.center,
                                        )
                                      ),
                                      height: 145,
                                      width: mediaQD.size.width / 3.8,
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(10),
                                          bottomLeft: Radius.circular(10)
                                        ),
                                        child: CachedNetworkImage(
                                          imageUrl: items[index].img,
                                          placeholder: (context, url) => Container(child: Center(child: CircularProgressIndicator()),),
                                          errorWidget: (context, url, error) => Image.asset('assets/img/placeholder.png'),
                                          fit: BoxFit.cover,
                                          alignment: Alignment.center,
                                        )
                                      )
                                    ),
                                    Container(
                                      height: 145,
                                      padding: EdgeInsets.all(12.0),
                                      width: mediaQD.size.width / 1.6,
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Container(
                                            width: mediaQD.size.width / 2.4,
                                            padding: EdgeInsets.only(bottom: 2.0),
                                            child: Text(
                                              items[index].title,
                                              maxLines: 3,
                                              softWrap: true,
                                              overflow: TextOverflow.ellipsis,
                                              textAlign: TextAlign.left,
                                              style: GoogleFonts.quattrocentoSans(
                                                fontSize: 15,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.black
                                              ),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(bottom: 10.0),
                                            child: Text(
                                              "Author:\n${items[index].author}",
                                              maxLines: 3,
                                              softWrap: true,
                                              overflow: TextOverflow.ellipsis,
                                              textAlign: TextAlign.left,
                                              style: GoogleFonts.quattrocentoSans(
                                                fontSize: 14,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.black54
                                              ),
                                            ),
                                          ),
                                          LinearPercentIndicator(
                                            width: mediaQD.size.width / 1.8,
                                            lineHeight: 15.0,
                                            progressColor: Color(0xFFF4375A),
                                            percent: items[index].pageRead / items[index].totalPage,
                                            center: Text(
                                              (items[index].pageRead / items[index].totalPage * 100).toInt().toString() + '%',
                                              style: TextStyle(
                                                fontSize: 12,
                                                color: Colors.white
                                              ),
                                            ),
                                            animation: true,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                                Positioned(
                                  top: 3.0,
                                  right: 3.0,
                                  child: CircleAvatar(
                                    backgroundColor: Colors.black12,
                                    radius: 15,
                                    child: IconButton(
                                      padding: EdgeInsets.zero,
                                      icon: Icon(
                                        Icons.close,
                                        size: 20,
                                      ),
                                      color: Colors.white,
                                      onPressed: () {
                                        removeRead(items[index]);
                                      },
                                    ),
                                  )
                                ),
                              ],
                            ),
                          ),
                        );
                      }
                    ),
                  ) : Container(
                    height: mediaQD.size.height / 1.5,
                    child: Center(
                      child: Text(
                        MyLocalization.of(context).homeStartReadingbook,
                        style: GoogleFonts.quattrocentoSans(
                          color: Colors.black54,
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold
                        )
                      ),
                    ),
                  );
                } else {
                  return Container(
                    height: mediaQD.size.height / 1.5,
                    child: Center(
                      child: Text(
                        MyLocalization.of(context).homeStartReadingbook,
                        style: GoogleFonts.quattrocentoSans(
                          color: Colors.black54,
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold
                        )
                      ),
                    ),
                  );
                }
              }
            )
          ],
        ) : Container(
          child: Center(
            child: CircularProgressIndicator()
          ),
        ),
      ),
    );
  }
}
