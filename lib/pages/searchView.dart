import 'package:buddhist_educenter/bloc/book/book.dart';
import 'package:buddhist_educenter/bloc/global/BlocProvider.dart';
import 'package:buddhist_educenter/bloc/global/GlobalBloc.dart';
import 'package:buddhist_educenter/components/widgets/cardBook.dart';
import 'package:buddhist_educenter/components/widgets/inputSearch.dart';
import 'package:buddhist_educenter/model/bookModelApi.dart';
import 'package:buddhist_educenter/service/apiget.dart';
import 'package:flutter/material.dart';

class SearchView extends StatefulWidget {
  final String textSearch;
  SearchView({this.textSearch});
  @override
  _SearchViewState createState() => _SearchViewState();
}

class _SearchViewState extends State<SearchView> {
  TextEditingController editingController = TextEditingController();
  ServiceApi getApi = ServiceApi();
  bool _isLoading = true;
  void search(String value) async {
    setState(() {
      _isLoading = true;
    });
    try {
      dynamic res = await getApi.getApi('api/book/search/' + value);
      setState(() {
        if (res['data'] == 0) {
          BlocProvider.of<GlobalBloc>(context).bookBloc.searchItem(0);
          setState(() {
            _isLoading = false;
          });
          /* Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (BuildContext context) => SearchView(
                        textSearch: value,
                      ))); */
        } else {
          List<BookModelApi> dataBukuApi = List<BookModelApi>.from(res['data'].map((item) => BookModelApi.fromJson(item)));
          BlocProvider.of<GlobalBloc>(context).bookBloc.searchItem(dataBukuApi);
          setState(() {
            _isLoading = false;
          });
          /* Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (BuildContext context) => SearchView(
                        textSearch: value,
                      ))); */
        }
      });
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    editingController.text = widget.textSearch;
    search(widget.textSearch);
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: BlocProvider.of<GlobalBloc>(context).bookBloc.bookStream,
      builder: (context, snapshot) {
        BookData dataBook = BlocProvider.of<GlobalBloc>(context).bookBloc.bookData;
        return SafeArea(
          child: Scaffold(
            body: !_isLoading ? Column(
              children: <Widget>[
                Container(
                  child: Align(
                    alignment: Alignment.center,
                    child: Container(
                      height: 60,
                      width: MediaQuery.of(context).size.width / 1.1,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          IconButton(
                            icon: Icon(
                              Icons.arrow_back,
                              size: 30,
                            ),
                            onPressed: () {
                              Navigator.pop(context);
                            }
                          ),
                          InputSearch(
                            lebar: 1.3,
                            controllerInput: editingController,
                            submitted: (value) {
                              search(value);
                            }
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    width: MediaQuery.of(context).size.width / 1.1,
                    child: Align(
                      alignment: Alignment.topCenter,
                      child: ListView.builder(
                        padding: EdgeInsets.only(top: 10.0),
                        shrinkWrap: true,
                        itemCount: dataBook.listSearch.length,
                        itemBuilder: (BuildContext context, int index) {
                          return CardBook(
                            listBookModel: dataBook.listSearch[index]
                          );
                        }
                      )
                    )
                  )
                )
              ],
            ) : Container(
              child: Center(child: CircularProgressIndicator()),
            ),
          ),
        );
      }
    );
  }
}
