/* import 'dart:async';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'dart:convert';
import 'package:buddhist_educenter/components/widgets/pdfAnjay.dart';
import 'package:buddhist_educenter/model/bookModelApi.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_pdfview/flutter_pdfview.dart';
import 'package:http/http.dart' as http;
import 'package:buddhist_educenter/env.dart';
import 'package:flutter/material.dart';
import 'package:flutter_plugin_pdf_viewer/flutter_plugin_pdf_viewer.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ReadBook extends StatefulWidget {
  @override
  _ReadBookState createState() => _ReadBookState();
}

class _ReadBookState extends State<ReadBook> {
  bool _isLoading = true;
  PDFDocument document;
  int halamanBuku;
  BookModelApi detailBook;
  var data;
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  Future<String> getBook() async {
    setState(() {
      _isLoading = true;
    });
    SharedPreferences prefs = await SharedPreferences.getInstance();

    var tokenType = store.getDataString('token_type');
    var accessToken = store.getDataString('access_token');
    var pointer = store.getDataString('BookPointer');
    var res = await http.get(url('api/book/' + pointer),
        headers: {'Authorization': tokenType + ' ' + accessToken});

    setState(() {
      var content = json.decode(res.body);
      debugPrint(content['book']['title'].toString());

      detailBook = BookModelApi.fromJson(content['book']);
      data = content['book'];
      /* loadDocument(gcsurl + content['book']['book'],
          content['book']['row_pointer']); */
    });
    document = await PDFDocument.fromURL(
        gcsurl+ detailBook.book);
    halamanBuku = prefs.getInt(detailBook.rowPointer) ?? 1;

    setState(() {
      if (halamanBuku == 1) {
        addViewCount();
      } else {
        _isLoading = false;
      }
    });
    return gcsurl + detailBook.book;
  }

  Future<String> addViewCount() async {
    setState(() {
      _isLoading = true;
    });
    SharedPreferences prefs = await SharedPreferences.getInstance();

    var tokenType = store.getDataString('token_type');
    var accessToken = store.getDataString('access_token');
    var pointer = store.getDataString('BookPointer');
    var res = await http.get(url('api/addViewCount/' + pointer), headers: {'Authorization': tokenType + ' ' + accessToken});
    debugPrint(res.toString());

    setState(() {
      _isLoading = false;
    });
    return 'success!';
  }

  loadDocument(bookurl, pointerbook) async {
    final SharedPreferences prefs = await _prefs;
    document = await PDFDocument.fromURL(bookurl);
    halamanBuku = prefs.getInt(pointerbook) ?? 1;
    setState(() => _isLoading = false);
  }

  String pathPDF = "";
  String landscapePathPdf = "";
  String corruptedPathPDF = "";

  final Completer<PDFViewController> _controller = Completer<PDFViewController>();
  int pages = 0;
  int currentPage = 0;
  bool isReady = false;
  String errorMessage = '';

  Future<File> createFileOfPdfUrl(String fileURL) async {
    Completer<File> completer = Completer();
    print("Start download file from internet!");
    try {
      final url = fileURL;
      final filename = url.substring(url.lastIndexOf("/") + 1);
      var request = await HttpClient().getUrl(Uri.parse(url));
      var response = await request.close();
      var bytes = await consolidateHttpClientResponseBytes(response);
      var dir = await getApplicationDocumentsDirectory();
      print("Download files");
      print("${dir.path}/$filename");
      File file = File("${dir.path}/$filename");
      await file.writeAsBytes(bytes, flush: true);
      completer.complete(file);
    } catch (e) {
      throw Exception('Error parsing asset file!');
    }

    return completer.future;
  }

  @override
  void initState() {
    super.initState();
    getBook().then((value) => createFileOfPdfUrl(value).then((f) {
      setState(() {
        pathPDF = f.path;
      });
    }));
  }

  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);
    return Scaffold(
      backgroundColor: theme.backgroundColor,
      appBar: AppBar(
        title: Text(_isLoading ? '...' : detailBook.title),
        backgroundColor: theme.primaryColor,
      ),
      body: Center(
        child: Stack(
          children: [
        /* _isLoading ? Center(
          child: CircularProgressIndicator()
        ) : PDFAnjay(
          document: document,
          halaman: halamanBuku,
          idBuku: detailBook.rowPointer,
          bookModel: detailBook,
        )  */
          pathPDF != null && pathPDF.isNotEmpty ? PDFView(
            filePath: pathPDF,
            enableSwipe: true,
            swipeHorizontal: true,
            autoSpacing: false,
            pageFling: true,
            pageSnap: true,
            defaultPage: currentPage,
            fitPolicy: FitPolicy.BOTH,
            preventLinkNavigation:
                false, // if set to true the link is handled in flutter
            onRender: (_pages) {
              setState(() {
                pages = _pages;
                isReady = true;
              });
            },
            onError: (error) {
              setState(() {
                errorMessage = error.toString();
              });
              print(error.toString());
            },
            onPageError: (page, error) {
              setState(() {
                errorMessage = '$page: ${error.toString()}';
              });
              print('$page: ${error.toString()}');
            },
            onViewCreated: (PDFViewController pdfViewController) {
              _controller.complete(pdfViewController);
            },
            onLinkHandler: (String uri) {
              print('goto uri: $uri');
            },
            onPageChanged: (int page, int total) {
              print('page change: $page/$total');
              setState(() {
                currentPage = page;
              });
            },
          ) : Container(),
          errorMessage.isEmpty ? !isReady ? Center(
            child: CircularProgressIndicator(),
          ) : Container() : Center(
            child: Text(errorMessage),
          )
          ],
        )
      ),
    );
  }
}
 */