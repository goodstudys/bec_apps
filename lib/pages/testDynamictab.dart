/* import 'dart:convert';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:buddhist_educenter/env.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TabDream11 extends StatefulWidget {
  @override
  _TabDream11State createState() => _TabDream11State();
}

class _TabDream11State extends State<TabDream11> with SingleTickerProviderStateMixin {
  List<String> categories = ["All", "Today"];
  TabController tabController;
  TextStyle tabStyle = TextStyle(fontSize: 16);
  bool _isLoading = false;
  List<dynamic> kategoriBuku;
  Future<String> getBook() async {
    setState(() {
      _isLoading = true;
    });
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var tokenType = store.getDataString('token_type');
    var accessToken = store.getDataString('access_token');
    var res = await http.get(url('api/category'),
        headers: {'Authorization': tokenType + ' ' + accessToken});
    if (mounted)
      setState(() {
        Map<String, dynamic> content = json.decode(res.body);
        kategoriBuku = content['data'];
        debugPrint(kategoriBuku.toString());
      });
    setState(() {
          tabController =
        TabController(length: kategoriBuku.length, vsync: this, initialIndex: 0);
      _isLoading = false;
    });
    return 'success!';
  }
  @override
  void initState() {
    super.initState();
    getBook();

  }

  tabCreate() =>
      CustomScrollView(
        slivers: <Widget>[
          SliverFillRemaining(
            child: Scaffold(
              backgroundColor: Colors.white70,
              appBar: TabBar(
                        indicator: BoxDecoration(
                            gradient: LinearGradient(colors: [
                              Colors.redAccent,
                              Colors.orangeAccent
                            ]),
                            borderRadius: BorderRadius.circular(50),
                            color: Colors.redAccent),
                        controller: tabController,
                        isScrollable: true,
                        labelStyle: GoogleFonts.quattrocentoSans(
                            fontSize: 14,
                            fontWeight: FontWeight.bold,
                            color: Colors.black),
                        unselectedLabelStyle: GoogleFonts.quattrocentoSans(
                            fontSize: 13,
                            fontWeight: FontWeight.w400,
                            color: Color(0xFF898989)),
                        labelPadding: EdgeInsets.only(left: 13.0, right: 13.0),
                        indicatorPadding:
                            EdgeInsets.only(left: 13.0, right: 13.0),
                        labelColor: Colors.black,
                        unselectedLabelColor: Color(0xFF898989),
                        indicatorSize: TabBarIndicatorSize.tab,
                tabs: List<Widget>.generate(kategoriBuku.length, (int index) {
                  return Tab(
                      child: Text(kategoriBuku[index]['name'],
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 15.0)));
                }),
              ),

              body: TabBarView(
                  controller: tabController,
                  children: List<Widget>.generate(
                      kategoriBuku.length, (int index) {
                    return new Text(kategoriBuku[index]['name']);
                  })),
            ),
          ),
        ],
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Flutter Tab Dynamic'),
        ),
        body: _isLoading == true?Container(child:Center(child: CircularProgressIndicator(),)): tabCreate());
  }

  tabName(String name) =>
      Tab(
        child: Text(
          name,
          style: tabStyle,
        ),
      );
} */