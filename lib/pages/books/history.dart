import 'dart:io';
import 'package:buddhist_educenter/storage/storage.dart';
import 'package:buddhist_educenter/components/widgets/cardBookApi.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:sweetalert/sweetalert.dart';
import 'package:buddhist_educenter/env.dart';
import '../detailbook.dart';

class HistoryTab extends StatefulWidget {
  final String idBuku;
  HistoryTab({ @required this.idBuku});
  @override
  _HistoryTabState createState() => _HistoryTabState();
}

class _HistoryTabState extends State<HistoryTab> {
  DataStore store = DataStore();
  bool _isLoading = true;
  var listBook;
  
  Future<String> senddataBook(String pointer) async {
    store.setDataString('BookPointer', pointer);
    Navigator.push(context,
        MaterialPageRoute(builder: (BuildContext context) => DetailBook()));
    return 'success!';
  }

  Future<dynamic> getBooks(String idBuku) async {
    setState(() {
      _isLoading = true;
    });
    try {
      var tokenType = await store.getDataString('token_type');
      var accessToken = await store.getDataString('access_token');
      final getToken = await http.get(url('api/category/book/'+idBuku), headers: {"Authorization": tokenType + ' ' + accessToken});
      var getTokenDecode = json.decode(getToken.body);
      if (getTokenDecode['code'] == 200) {
        listBook = getTokenDecode['data'];
        setState(() {
          _isLoading = false;
        });
      } else if (getToken.statusCode == 401) {
        SweetAlert.show(context,
          title: "GAGAL",
          style: SweetAlertStyle.confirm,
          showCancelButton: false
        );
        setState(() {
          _isLoading = false;
        });
      } else {
        // showInSnackBar('Request failed with status: ${getToken.statusCode}');
        setState(() {
          _isLoading = false;
        });
      }
      // debugPrint(datajson.toString());
    } on SocketException catch (_) {
      // showInSnackBar('Connection Timed Out');
      setState(() {
        _isLoading = false;
      });
    } catch (e) {
      debugPrint(e);
      setState(() {
        _isLoading = false;
      });
      // showInSnackBar(e);
    }
    // setState(() {
    //   _isLoading = false;
    // });
    return 'success';
  }

  @override
  void initState() {
    getBooks(widget.idBuku);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return _isLoading ? Container(
      child: Center(
        child: CircularProgressIndicator(),
      )
    ) : !_isLoading && listBook == 0 ? Container(
      child: Center(
        child: Image(
          image: AssetImage('assets/img/empty.png'),
          width: MediaQuery.of(context).size.width / 1.5,
        ),
      )
    ) : ListView.builder(
      padding: EdgeInsets.only(top: 10.0),
      shrinkWrap: true,
      itemCount: listBook == null ? 0 :listBook.length,
      itemBuilder: (BuildContext context, int index) {
        return CardBooknew(
          listBookModel: listBook[index],
          description: listBook[index]['description'],
          img: listBook[index]['thumbnail'],
          title: listBook[index]['title'],
          author: listBook[index]['author'],
          tap: () {
            senddataBook(listBook[index]['row_pointer']);
          },
        );
      }
    );
  }
}
