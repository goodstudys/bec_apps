import 'package:buddhist_educenter/components/widgets/inputSearch.dart';
import 'package:buddhist_educenter/env.dart';
import 'package:buddhist_educenter/locale/my_localization.dart';
import 'package:buddhist_educenter/pages/books/history.dart';
import 'package:buddhist_educenter/pages/searchView.dart';
import 'package:buddhist_educenter/service/apiget.dart';
import 'package:buddhist_educenter/storage/storage.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class BooksPage extends StatefulWidget {
  @override
  _BooksPageState createState() => _BooksPageState();
}

class _BooksPageState extends State<BooksPage> with SingleTickerProviderStateMixin {
  TabController _tabController;
  ServiceApi getApi = ServiceApi();
  List<dynamic> kategoriBuku;
  bool _isLoading = false;
  TextEditingController editingController = TextEditingController();
  DataStore store = DataStore();

  void search(String value) {
    Navigator.push(context, MaterialPageRoute(
      builder: (BuildContext context) => SearchView(
        textSearch: value,
      ))
    ).then((value) => editingController.clear());
  }


  Future<String> getBook() async {
    var tokenType = await store.getDataString('token_type');
    var accessToken = await store.getDataString('access_token');
    var res = await http.get(url('api/category'),
        headers: {'Authorization': tokenType + ' ' + accessToken});
    if (mounted)
      setState(() {
        Map<String, dynamic> content = json.decode(res.body);
        kategoriBuku = content['data'];
        debugPrint(kategoriBuku.toString());
        _tabController = TabController(
            length: kategoriBuku.length, vsync: this, initialIndex: 0);
      });
    return 'success!';
  }

  @override
  void initState() {
    super.initState();
    if (mounted)
      getBook().then((s) => setState(() {
            _isLoading = true;
          }));
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: theme.primaryColor,
        centerTitle: true,
        title: Text(
          MyLocalization.of(context).bottomNavigationBooks,
          style: GoogleFonts.quattrocentoSans(
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontSize: 20
          )
        ),
      ),
      body: SafeArea(
        child: _isLoading
            //  == false
            ? Column(
                children: <Widget>[
                  InputSearch(
                      lebar: 1.1,
                      controllerInput: editingController,
                      submitted: (value) {
                        search(value);
                      }),
                  Container(
                      padding: EdgeInsets.only(top: 17.0, bottom: 10.0),
                      child: Align(
                        alignment: Alignment.center,
                        child: Container(
                          height: 35,
                          width: MediaQuery.of(context).size.width / 1.1,
                          child: TabBar(
                            indicator: BoxDecoration(
                                gradient: LinearGradient(colors: [
                                  Color(0XFF1e3c72),
                                  Color(0XFF2a5298)
                                ]),
                                borderRadius: BorderRadius.circular(50),
                                color: Colors.redAccent),
                            controller: _tabController,
                            isScrollable: true,
                            labelStyle: GoogleFonts.quattrocentoSans(
                                fontSize: 14,
                                fontWeight: FontWeight.bold,
                                color: Colors.black),
                            unselectedLabelStyle: GoogleFonts.quattrocentoSans(
                                fontSize: 13,
                                fontWeight: FontWeight.w400,
                                color: Color(0xFF898989)),
                            labelPadding:
                                EdgeInsets.only(left: 13.0, right: 13.0),
                            indicatorPadding:
                                EdgeInsets.only(left: 13.0, right: 13.0),
                            labelColor: Colors.white,
                            unselectedLabelColor: Color(0xFF898989),
                            indicatorSize: TabBarIndicatorSize.tab,
                            tabs: List<Widget>.generate(kategoriBuku.length,
                                (int index) {
                              return Tab(
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Text(kategoriBuku[index]['name']),
                                ),
                              );
                            }),
                          ),
                        ),
                      )),
                  Expanded(
                    child: Container(
                      width: MediaQuery.of(context).size.width / 1.1,
                      child: Align(
                        alignment: Alignment.topCenter,
                        child: TabBarView(
                          physics: NeverScrollableScrollPhysics(),
                          controller: _tabController,
                          children: List<Widget>.generate(kategoriBuku.length,
                              (int index) {
                            return HistoryTab(
                                idBuku: kategoriBuku[index]['id'].toString());
                          }),
                        ),
                      ),
                    ),
                  )
                ],
              )
            : Container(
                child: Center(child: CircularProgressIndicator()),
              ),
      ),
    );
  }
}
