import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';

class DetailImage extends StatefulWidget {
  final String image;
  DetailImage({this.image});
  @override
  _DetailImageState createState() => _DetailImageState();
}

class _DetailImageState extends State<DetailImage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Event Detail'),
        ),
        body: SafeArea(
            child: Container(
          // decoration: BoxDecoration(
          //     // color: Colors.black,
          //     image: DecorationImage(
          //   image: NetworkImage(widget.image),
          //   fit: BoxFit.contain,
          //   alignment: Alignment.center,
          //   // colorFilter: new ColorFilter.mode(
          //   //     Colors.black.withOpacity(0.5), BlendMode.dstATop),
          // )),
          child: PhotoView(
      imageProvider: NetworkImage(widget.image),
    ),
        )));
  }
}
