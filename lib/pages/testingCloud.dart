import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';

class CloudTesting extends StatefulWidget {
  @override
  _CloudTestingState createState() => _CloudTestingState();
}

class _CloudTestingState extends State<CloudTesting> {
  File _image;
  Uint8List _imageBytes;
  final picker = ImagePicker();

  void _getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        debugPrint(pickedFile.path);
        _image = File(pickedFile.path);
        _imageBytes = _image.readAsBytesSync();
      } else {
        debugPrint('No Image Selected');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('title'),
      ),
      body: Center(
        child: _imageBytes == null
            ? Text('No iMGAE SELECTED')
            : Stack(
                children: [Image.memory(_imageBytes)],
              ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _getImage,
        tooltip: 'Select image',
        child: Icon(Icons.add_a_photo),
      ),
    );
  }
}
