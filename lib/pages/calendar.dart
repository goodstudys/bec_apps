import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:photo_view/photo_view.dart';

class CalendarPage extends StatefulWidget {
  @override
  _CalendarPageState createState() => _CalendarPageState();
}

class _CalendarPageState extends State<CalendarPage> {

  List<String> calenders = [
    'https://becindonesia.org/calendar/cover.jpg',
    'https://becindonesia.org/calendar/feb2021.jpg',
    'https://becindonesia.org/calendar/mar2021.jpg',
    'https://becindonesia.org/calendar/apr2021.jpg',
    'https://becindonesia.org/calendar/may2021.jpg',
    'https://becindonesia.org/calendar/jun2021.jpg',
    'https://becindonesia.org/calendar/jul2021.jpg',
    'https://becindonesia.org/calendar/aug2021.jpg',
    'https://becindonesia.org/calendar/sep2021.jpg',
    'https://becindonesia.org/calendar/oct2021.jpg',
    'https://becindonesia.org/calendar/nov2021.jpg',
    'https://becindonesia.org/calendar/des2021.jpg',
    'https://becindonesia.org/calendar/jan2022.jpg',
  ];
  @override
  Widget build(BuildContext context) {
    MediaQueryData mediaQD = MediaQuery.of(context);
    return Scaffold(
      backgroundColor: Colors.black54,
      body: SafeArea(
        child: Stack(
          children: [
            PageView.builder(
              itemCount: calenders.length,
              itemBuilder: (BuildContext context, int index) => Container(
                width: mediaQD.size.width,
                height: mediaQD.size.height,
                child: PhotoView(
                  imageProvider: CachedNetworkImageProvider(
                    calenders[index],
                  ),
                  backgroundDecoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('assets/img/placeholder.png'),
                      fit: BoxFit.cover,
                    )
                  ),
                ),
              ),
            ),
            Container(
              // height: 60,
              width: mediaQD.size.width / 1.01,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  IconButton(
                    icon: Icon(
                      Platform.isIOS
                        ? Icons.arrow_back_ios
                        : Icons.arrow_back
                    ),
                    color: Colors.white,
                    onPressed: () {
                      Navigator.pop(context);
                    }),
                  Text(
                    '',
                    style: GoogleFonts.quattrocentoSans(
                        fontWeight: FontWeight.bold, fontSize: 20),
                  ),
                  Icon(
                    Icons.ac_unit,
                    color: Colors.transparent,
                  )
                ],
              ),
            ),
          ],
        )
      ),
    );
  }
}