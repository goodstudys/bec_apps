// import 'package:flutter/material.dart';
// import 'package:google_fonts/google_fonts.dart';

// class DonationPage extends StatefulWidget {
//   @override
//   _DonationPageState createState() => _DonationPageState();
// }

// class _DonationPageState extends State<DonationPage> {
//   /*
//   StreamSubscription _purchaseUpdatedSubscription;
//   StreamSubscription _purchaseErrorSubscription;
//   StreamSubscription _conectionSubscription;
//   String _platformVersion = 'Unknown';
//   double _value = 0;
//   */
//   final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

//   /*
//   final List<String> _productLists = [
//     // 'donasi.10000',
//     // 'donasi.20000',
//     // 'donasi.30000',
//     // 'donasi.40000',
//     // 'donasi.50000',
//     // 'donasi.60000',
//     // 'donasi.70000',
//     // 'donasi.80000',
//     // 'donasi.90000',
//     // 'donasi.100000',
//     'android.test.purchased'
//   ];
//   List<IAPItem> _items = [];
//   List<PurchasedItem> _purchases = [];
//   void showInSnackBar(String value, {SnackBarAction action}) {
//     _scaffoldKey.currentState.showSnackBar(new SnackBar(
//       content: new Text(value),
//       action: action,
//     ));
//   }

//   Future _getProduct() async {
//     List<IAPItem> items =
//         await FlutterInappPurchase.instance.getProducts(_productLists);
//     for (var item in items) {
//       debugPrint('${item.toString()}');
//       this._items.add(item);
//     }

//     setState(() {
//       this._items = items;
//       this._purchases = [];
//     });
//   }

//   Future<void> initPlatformState() async {
//     String platformVersion;
//     // Platform messages may fail, so we use a try/catch PlatformException.
//     try {
//       platformVersion = await FlutterInappPurchase.instance.platformVersion;
//     } on PlatformException {
//       platformVersion = 'Failed to get platform version.';
//     }

//     // prepare
//     var result = await FlutterInappPurchase.instance.initConnection;
//     debugPrint('result: $result');

//     // If the widget was removed from the tree while the asynchronous platform
//     // message was in flight, we want to discard the reply rather than calling
//     // setState to update our non-existent appearance.
//     if (!mounted) return;

//     setState(() {
//       _platformVersion = platformVersion;
//     });

//     // refresh items for android
//     try {
//       String msg = await FlutterInappPurchase.instance.consumeAllItems;
//       debugPrint('consumeAllItems: $msg');
//     } catch (err) {
//       debugPrint('consumeAllItems error: $err');
//     }

//     _conectionSubscription =
//         FlutterInappPurchase.connectionUpdated.listen((connected) {
//       debugPrint('connected: $connected');
//     });

//     _purchaseUpdatedSubscription =
//         FlutterInappPurchase.purchaseUpdated.listen((productItem) {
//       debugPrint('purchase-updated: $productItem');
//     });

//     _purchaseErrorSubscription =
//         FlutterInappPurchase.purchaseError.listen((purchaseError) {
//       debugPrint('purchase-error: $purchaseError');
//     });
//     await _getProduct();
//   }

//   @override
//   void initState() {
//     super.initState();
//     initPlatformState();
//     // FlutterInappPurchase.instance.initConnection;
//   }
//   */

//   @override
//   Widget build(BuildContext context) {
//     // MediaQueryData mediaQD = MediaQuery.of(context);
//     // ThemeData theme = Theme.of(context);
//     /*
//     final NumberFormat formatter = NumberFormat.currency(
//         symbol: 'Rp. ',
//         decimalDigits: 0,
//         locale: Localizations.localeOf(context).toString());
//         */
//     return Scaffold(
//       key: _scaffoldKey,
//       backgroundColor: Colors.grey[800],
//       appBar: AppBar(
//         title: Text('Support & Donation'),
//         centerTitle: true,
//       ),
//       body: SafeArea(
//         child: Container(
//           padding: EdgeInsets.only(left: 20.0, right: 20.0, top: 10.0, bottom: 20.0),
//           child: Column(
//             mainAxisAlignment: MainAxisAlignment.center,
//             crossAxisAlignment: CrossAxisAlignment.center,
//             children: [
//               Padding(
//                 padding: const EdgeInsets.only(top: 8.0),
//                 child: Text(
//                   'Show your support',
//                   textAlign: TextAlign.left,
//                   style: GoogleFonts.quattrocentoSans(
//                     fontSize: 30,
//                     fontWeight: FontWeight.bold,
//                     color: Colors.white
//                   ),
//                 ),
//               ),
//               Padding(
//                 padding: const EdgeInsets.only(top: 5.0, bottom: 20.0),
//                 child: Text(
//                   'Your donation can help Buddhist Education Centre Surabaya maintain this mobile app and deliver quality readings to read.',
//                   style: GoogleFonts.quattrocentoSans(
//                     fontSize: 16.5, color: Colors.white
//                   ),
//                 ),
//               ),
//               Card(
//                 shape: RoundedRectangleBorder(
//                   borderRadius: BorderRadius.circular(10),
//                   side: BorderSide(color: Colors.grey[600])
//                 ),
//                 color: Colors.grey[600],
//                 child: Column(
//                   children: [
//                     Padding(
//                       padding: const EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
//                       child: Text(
//                         'DANA DHARMA',
//                         style: GoogleFonts.quattrocentoSans(
//                           fontSize: 18, color: Colors.white, fontWeight: FontWeight.bold
//                         ),
//                       ),
//                     ),
//                     Padding(
//                       padding: const EdgeInsets.only(top: 15.0, left: 10.0, right: 10.0),
//                       child: Text(
//                         'Bank Central Asia (BCA)\n508 505 7879',
//                         textAlign: TextAlign.center,
//                         style: GoogleFonts.quattrocentoSans(
//                           fontSize: 16.5, color: Colors.white, fontWeight: FontWeight.bold
//                         ),
//                       ),
//                     ),
//                     Padding(
//                       padding: const EdgeInsets.only(top: 5.0, bottom: 10.0, left: 10.0, right: 10.0),
//                       child: Text(
//                         'Yayasan Dharma Rangsi Surabaya',
//                         textAlign: TextAlign.center,
//                         style: GoogleFonts.quattrocentoSans(
//                           fontSize: 16.5, color: Colors.white
//                         ),
//                       ),
//                     ),
//                   ],
//                 ),
//               )
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }
