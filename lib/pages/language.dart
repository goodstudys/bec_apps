import 'package:buddhist_educenter/auth/login.dart';
import 'package:buddhist_educenter/components/widgets/radioitem.dart';
import 'package:buddhist_educenter/locale/my_localization.dart';
import 'package:buddhist_educenter/locale/provider_localization.dart';
import 'package:buddhist_educenter/model/LanguageModel.dart';
import 'package:buddhist_educenter/storage/storage.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class LanguagePage extends StatefulWidget {
  @override
  _LanguagePageState createState() => _LanguagePageState();
}

class _LanguagePageState extends State<LanguagePage> {
  List isi;
  bool isLoading = false;
  bool _autovalidate = false;
  String localeid1, localeid2;
  @override
  Widget build(BuildContext context) {
    final providerLocalization = Provider.of<ProviderLocalization>(context);
    MediaQueryData mediaQD = MediaQuery.of(context);
    ThemeData theme = Theme.of(context);
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.only(left: 20.0, right: 20.0, top: 40.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                padding: EdgeInsets.symmetric(horizontal: 80.0, vertical: 20.0),
                child: Image(image: AssetImage('assets/img/logo_bec.jpg'))
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  MyLocalization.of(context).profileSelectLanguage,
                  style: GoogleFonts.quattrocentoSans(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 20),
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              Expanded(
                child: ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: listRadio.length,
                  itemBuilder: (BuildContext context, int index) {
                    // return RaisedButton(
                    //     onPressed: () {
                    //       setState(() {
                    //         // ignore: unnecessary_statements
                    //         listRadio[index].value;
                    //       });
                    //     },
                    //     child: RadioItem(listRadio[index]));
                    return InkWell(
                      //highlightColor: Colors.red,
                      splashColor: Colors.blueAccent,
                      onTap: () {
                        setState(() {
                          listRadio.forEach(
                              (element) => element.isSelected = false);
                          listRadio[index].isSelected = true;

                          setState(() {
                            isi = listRadio.where((f) => f.isSelected == true).toList();
                            print(isi.length);
                          });
                          for (var i = 0; i < isi.length; i++) {
                            localeid1 = isi[i].locale1;
                            localeid2 = isi[i].locale2;
                          }
                        });
                      },
                      child: RadioItem(listRadio[index]),
                    );
                  },
                ),
              ),
              _autovalidate == true ? isi == null ? Text(
                'Please choose your language',
                style: TextStyle(color: Colors.red, fontSize: 12),
              ) : Container() : Container(),
              Container(
                  padding: EdgeInsets.only(top: 23, bottom: 13),
                  child: Align(
                    alignment: Alignment.center,
                    child: InkWell(
                      onTap: () async {
                        DataStore dataStore = new DataStore();
                        String _name = await dataStore.getDataString("name");
                        setState(() {
                          if (isi == null) {
                            MyLocalization.load(Locale('en', 'US'));
                            providerLocalization
                                .setLanguage(Locale('en', 'US'));
                            dataStore.setDataString('localeid1', 'en');
                            dataStore.setDataString('localeid2', 'US');
                          } else {
                            MyLocalization.load(Locale(localeid1, localeid2));
                            dataStore.setDataString('localeid1', localeid1);
                            dataStore.setDataString('localeid2', localeid2);
                            providerLocalization
                                .setLanguage(Locale(localeid1, localeid2));
                          }
                        });
                        if (_name == 'Tidak ditemukan') {
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      LoginPage()));
                        } else {
                          Navigator.pop(context);
                        }
                      },
                      child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(
                                mediaQD.size.width / 28),
                            color: theme.primaryColor,
                          ),
                          height: 52,
                          width: mediaQD.size.width / 1.4,
                          child: Align(
                              alignment: Alignment.center,
                              child: Text(
                                MyLocalization.of(context).textConfirm,
                                style: GoogleFonts.quattrocentoSans(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20),
                              ))),
                    ),
                  )),
            ],
          )
        ),
      ),
    );
  }
}
