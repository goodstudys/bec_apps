/* import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:buddhist_educenter/locale/my_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:flutter_inapp_purchase/flutter_inapp_purchase.dart';
import 'package:cupertino_radio_choice/cupertino_radio_choice.dart';

class DonationPage extends StatefulWidget {
  @override
  _DonationPageState createState() => _DonationPageState();
}

class _DonationPageState extends State<DonationPage> {
  StreamSubscription _purchaseUpdatedSubscription;
  StreamSubscription _purchaseErrorSubscription;
  StreamSubscription _conectionSubscription;
  String _platformVersion = 'Unknown';
  double _value = 0;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  static final Map<String, String> donasiMap = {
    'bec.donasi1': '',
    'bec.donasi2': '',
    'bec.donasi3': '',
    'bec.donasi4': '',
    'bec.donasi5': '',
    'bec.donasi6': '',
  };

  // String _selectedDonasi = donasiMap.keys.first;
  String _selectedDonasi;
  final List<String> _productLists = Platform.isAndroid ? [
    'bec.donasi1',
    'bec.donasi2',
    'bec.donasi3',
    'bec.donasi5',
    'bec.donasi4',
    'bec.donasi6',
    // 'android.test.purchased'
  ] : ['com.cooni.point1000', 'com.cooni.point5000']; // ios iap
  List<IAPItem> _items = [];
  List<PurchasedItem> _purchases = [];
  void showInSnackBar(String value, {SnackBarAction action}) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(value),
      action: action,
    ));
  }

  Future _getProduct() async {
    List<IAPItem> items = await FlutterInappPurchase.instance.getProducts(_productLists);
    for (var item in items) {
      var cnv = json.decode(item.originalJson);
      donasiMap.update(item.productId, (value) => item.localizedPrice);
      this._items.add(item);
    }
    items.sort((a, b) => a.productId.compareTo(b.productId));
    print(items);
    setState(() {
      this._items = items;
      this._purchases = [];
    });
  }

  Future<void> initPlatformState() async {
    String platformVersion;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      platformVersion = await FlutterInappPurchase.instance.platformVersion;
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }

    // prepare
    var result = await FlutterInappPurchase.instance.initConnection;
    debugPrint('result: $result');

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _platformVersion = platformVersion;
    });

    // refresh items for android
    try {
      String msg = await FlutterInappPurchase.instance.consumeAllItems;
      debugPrint('consumeAllItems: $msg');
    } catch (err) {
      debugPrint('consumeAllItems error: $err');
    }

    _conectionSubscription =
        FlutterInappPurchase.connectionUpdated.listen((connected) {
      debugPrint('connected: $connected');
    });

    _purchaseUpdatedSubscription =
        FlutterInappPurchase.purchaseUpdated.listen((productItem) {
      debugPrint('purchase-updated: $productItem');
    });

    _purchaseErrorSubscription =
        FlutterInappPurchase.purchaseError.listen((purchaseError) {
      debugPrint('purchase-error: $purchaseError');
    });
    await _getProduct();
  }

  void onDonateSelected(String donasiKey) {
    print(donasiKey);
    setState(() {
      _selectedDonasi = donasiKey;
    });
  }

  @override
  void initState() {
    super.initState();
    initPlatformState();
    // FlutterInappPurchase.instance.initConnection;
  }

  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);
    MediaQueryData mediaQD = MediaQuery.of(context);
    final NumberFormat formatter = NumberFormat.currency(
        symbol: 'Rp. ',
        decimalDigits: 0,
        locale: Localizations.localeOf(context).toString());
    final genderSelectionTile = new Material(
      color: Colors.transparent,
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          const Padding(
            padding: EdgeInsets.only(bottom: 5.0),
          ),
          CupertinoRadioChoice(
              choices: donasiMap,
              onChange: onDonateSelected,
              initialKeyValue: _selectedDonasi)
        ],
      ),
    );

    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.grey[800],
      appBar: AppBar(
        title: Text(MyLocalization.of(context).donateAppbar),
        centerTitle: true,
      ),
      body: SafeArea(
        child: ListView(
          children: [
            Column(
              children: [
                // Padding(
                //   padding: const EdgeInsets.only(top: 23.0),
                //   child: Text(
                //     'Choose Donation',
                //     style: GoogleFonts.quattrocentoSans(
                //         fontSize: 18, color: Colors.black),
                //   ),
                // ),
                // Padding(
                //   padding: const EdgeInsets.all(8.0),
                //   child: Text(
                //     _selectedGender,
                //     textAlign: TextAlign.center,
                //     style: GoogleFonts.quattrocentoSans(
                //         fontSize: 30,
                //         fontWeight: FontWeight.bold,
                //         color: Colors.black),
                //   ),
                // )
              ],
            ),
            Container(
              height: mediaQD.size.height / 5,
              width: mediaQD.size.width,
              child: Image(
                image: AssetImage('assets/img/donate.jpg'),
                fit: BoxFit.cover,
              )
            ),
            Container(
              padding: EdgeInsets.only(left: 20.0, right: 20.0, top: 10.0, bottom: 20.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 15.0),
                    child: Text(
                      MyLocalization.of(context).donateTitle,
                      textAlign: TextAlign.left,
                      style: GoogleFonts.quattrocentoSans(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        color: Colors.white
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0, bottom: 20.0),
                    child: Text(
                      MyLocalization.of(context).donateDesc,
                      textAlign: TextAlign.left,
                      style: GoogleFonts.quattrocentoSans(
                        fontSize: 15.5,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                      side: BorderSide(color: Colors.grey[600])
                    ),
                    color: Colors.grey[600],
                    child: Container(
                      width: mediaQD.size.width,
                      child: Column(
                        children: [
                          Container(
                            width: double.maxFinite,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(10.0),
                                topRight: Radius.circular(10.0),
                              ),
                              color: Colors.white
                            ),
                            padding: const EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0, bottom: 5.0),
                            child: Text(
                              'GOOGLE PAY',
                              textAlign: TextAlign.center,
                              style: GoogleFonts.quattrocentoSans(
                                fontSize: 18, color: Colors.black87, fontWeight: FontWeight.bold
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0, bottom: 10.0),
                            child: Text(
                              'DANA DHARMA',
                              style: GoogleFonts.quattrocentoSans(
                                fontSize: 18, color: Colors.white, fontWeight: FontWeight.bold
                              ),
                            ),
                          ),
                          Container(
                            width: mediaQD.size.width / 1.3,
                            padding: const EdgeInsets.only(left: 10.0, right: 10.0),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton(
                                hint: Text(MyLocalization.of(context).donateAmount),
                                isExpanded: true,
                                value: _selectedDonasi,
                                items: _items.map((e) => DropdownMenuItem(
                                  value: e.productId,
                                  child: Text(e.localizedPrice),
                                )).toList(),
                                onChanged: onDonateSelected,
                              ),
                            ),
                          ),
                          Container(
                              padding: EdgeInsets.only(top: 10, bottom: 13),
                              child: Align(
                                alignment: Alignment.center,
                                child: InkWell(
                                  onTap: () async {
                                    await FlutterInappPurchase.instance.initConnection;
                                    debugPrint(_selectedDonasi);
                                    FlutterInappPurchase.instance.requestPurchase(_selectedDonasi);
                                  },
                                  child: Container(
                                    height: 52,
                                    decoration: BoxDecoration(
                                      color: theme.primaryColor,
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    width: mediaQD.size.width / 1.3,
                                    child: Align(
                                      alignment: Alignment.center,
                                      child: Text(
                                        MyLocalization.of(context).bottomNavigationDonate,
                                        style: TextStyle(color: Colors.white, fontSize: 16.5),
                                      )
                                    )
                                  ),
                                ),
                              )),
                        ],
                      ),
                    ),
                  ),
                  Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                      side: BorderSide(color: Colors.grey[600])
                    ),
                    color: Colors.grey[600],
                    child: Container(
                      width: mediaQD.size.width,
                      child: Column(
                        children: [
                          Container(
                            width: double.maxFinite,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(10.0),
                                topRight: Radius.circular(10.0),
                              ),
                              color: Colors.white
                            ),
                            padding: const EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0, bottom: 5.0),
                            child: Text(
                              MyLocalization.of(context).donateTransfer,
                              textAlign: TextAlign.center,
                              style: GoogleFonts.quattrocentoSans(
                                fontSize: 18, color: Colors.black87, fontWeight: FontWeight.bold
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
                            child: Text(
                              'DANA DHARMA',
                              style: GoogleFonts.quattrocentoSans(
                                fontSize: 18, color: Colors.white, fontWeight: FontWeight.bold
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 15.0, left: 10.0, right: 10.0),
                            child: Text(
                              'Bank Central Asia (BCA)\n508 505 7879',
                              textAlign: TextAlign.center,
                              style: GoogleFonts.quattrocentoSans(
                                fontSize: 16.5, color: Colors.white, fontWeight: FontWeight.bold
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 5.0, bottom: 10.0, left: 10.0, right: 10.0),
                            child: Text(
                              'Yayasan Dharma Rangsi Surabaya',
                              textAlign: TextAlign.center,
                              style: GoogleFonts.quattrocentoSans(
                                fontSize: 16.5, color: Colors.white
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
            // SliderTheme(
            //   data: SliderTheme.of(context).copyWith(
            //     activeTrackColor: Colors.blue[700],
            //     inactiveTrackColor: Colors.blue[100],
            //     trackShape: RoundedRectSliderTrackShape(),
            //     trackHeight: 4.0,
            //     thumbShape: RoundSliderThumbShape(enabledThumbRadius: 12.0),
            //     thumbColor: Colors.blueAccent,
            //     overlayColor: Colors.blue.withAlpha(32),
            //     overlayShape: RoundSliderOverlayShape(overlayRadius: 28.0),
            //     tickMarkShape: RoundSliderTickMarkShape(),
            //     activeTickMarkColor: Colors.blue[700],
            //     inactiveTickMarkColor: Colors.blue[100],
            //     valueIndicatorShape: PaddleSliderValueIndicatorShape(),
            //     valueIndicatorColor: Colors.blueAccent,
            //     valueIndicatorTextStyle: TextStyle(
            //       color: Colors.white,
            //     ),
            //   ),
            //   child: Slider(
            //     value: _value,
            //     min: 0,
            //     max: 100000,
            //     divisions: 10,
            //     label: formatter.format(_value.round()),
            //     onChanged: (value) {
            //       setState(
            //         () {
            //           _value = value;
            //         },
            //       );
            //     },
            //   ),
            // ),
          ],
        ),
      ),
    );
  }
}
 */