import 'package:buddhist_educenter/env.dart';
import 'package:buddhist_educenter/pages/detailbook.dart';
import 'package:buddhist_educenter/storage/storage.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class TabsAll extends StatefulWidget {
  final dynamic data;
  TabsAll({@required this.data});

  @override
  _TabsAllState createState() => _TabsAllState();
}

class _TabsAllState extends State<TabsAll> {
  DataStore store = DataStore();
  
  Future<String> senddataBook(String pointer) async {
    store.setDataString('BookPointer', pointer);

    Navigator.push(context,
        MaterialPageRoute(builder: (BuildContext context) => DetailBook()));
    return 'success!';
  }

  @override
  Widget build(BuildContext context) {
    MediaQueryData mediaQD = MediaQuery.of(context);
    return Container(
      child: ListView.builder(
        itemCount: widget.data.length,
        scrollDirection: Axis.horizontal,
        padding: EdgeInsets.only(left: 10.0),
        itemBuilder: (BuildContext context, int index) {
          return InkWell(
            onTap: () {
              senddataBook(widget.data[index]['row_pointer']);
            },
            child: Container(
              child: Stack(
                alignment: Alignment.bottomCenter,
                children: <Widget>[
                  Card(
                    elevation: 2.0,
                    color: Colors.white,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    child: Container(
                      height: mediaQD.size.width / 2.2,
                      width: mediaQD.size.width / 1.2,
                      child: Padding(
                        padding: EdgeInsets.only(top: 13.0, bottom: 13.0, left: mediaQD.size.width / 2.7, right: 13.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.only(bottom: 10.0),
                              child: Text(
                                widget.data[index]['title'],
                                maxLines: 3,
                                overflow: TextOverflow.ellipsis,
                                textAlign: TextAlign.left,
                                style: GoogleFonts.quattrocentoSans(
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black
                                ),
                              ),
                            ),
                            Text(
                              widget.data[index]['description'],
                              maxLines: 5,
                              overflow: TextOverflow.ellipsis,
                              style: GoogleFonts.quattrocentoSans(
                                fontSize: 12,
                                color: Colors.black87,
                                height: 1.2
                              ),
                            ),
                          ],
                        ),
                      )
                    ),
                  ),
                  Positioned(
                    left: 10.0,
                    bottom: 10.0,
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        image: DecorationImage(
                          image: AssetImage('assets/img/placeholder.png'),
                          fit: BoxFit.cover,
                          alignment: Alignment.center,
                        )
                      ),
                      height: mediaQD.size.width / 2,
                      width: mediaQD.size.width / 3,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(10.0),
                        child: CachedNetworkImage(
                          imageUrl: gcsurl + widget.data[index]['thumbnail'],
                          placeholder: (context, url) => Container(child: Center(child: CircularProgressIndicator()),),
                          errorWidget: (context, url, error) => Image.asset('assets/img/placeholder.png'),
                          fit: BoxFit.fitHeight
                        )
                      )
                    ),
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
