import 'dart:convert';
import 'dart:io';
import 'package:buddhist_educenter/bloc/global/BlocProvider.dart';
import 'package:buddhist_educenter/bloc/global/GlobalBloc.dart';
import 'package:buddhist_educenter/locale/my_localization.dart';
import 'package:buddhist_educenter/model/bookModelApi.dart';
import 'package:buddhist_educenter/model/readbookModel.dart';
import 'package:buddhist_educenter/pages/donation.dart';
import 'package:buddhist_educenter/pages/readbook.dart';
import 'package:buddhist_educenter/storage/storage.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:buddhist_educenter/env.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';

class DetailBook extends StatefulWidget {
  // final BookModel bookModel;
  // DetailBook({this.bookModel});
  @override
  _DetailBookState createState() => _DetailBookState();
}

class _DetailBookState extends State<DetailBook> {
  DataStore store = DataStore();
  bool _isLoading = true;
  BookModelApi detailBook;
  int bookpage = 1;

  void getBook() async {
    setState(() {
      _isLoading = true;
    });
    try {
      var tokenType = await store.getDataString('token_type');
      var accessToken = await store.getDataString('access_token');
      var pointer = await store.getDataString('BookPointer');
      var res = await http.get(url('api/book/' + pointer), headers: {'Authorization': tokenType + ' ' + accessToken});
      setState(() {
        var content = json.decode(res.body);
        detailBook = BookModelApi.fromJson(content['book']);
        // debugPrint(content['book']['title'].toString());
      });
      // Check if already exist
      int total = BlocProvider.of<GlobalBloc>(context).bookBloc.bookData.listContinue.length;
      if (total == 0) {
        await BlocProvider.of<GlobalBloc>(context).bookBloc.loadBooks();
      }
      List<ReadingBookModel> blocBooks = BlocProvider.of<GlobalBloc>(context).bookBloc.bookData.listContinue;
      if (blocBooks.length > 0) {
        // Check if exist
        ReadingBookModel exist = blocBooks.firstWhere((e) => e.id == detailBook.id, orElse: () => null);
        if (exist != null) {
          setState(() {
            bookpage = exist.pageRead;
          });
        }
      }
      setState(() {
        _isLoading = false;
      });
    } on SocketException catch (_) {
      setState(() {
        // errorMessage = 'Connection Timed Out';
        _isLoading = false;
      });
    } catch (e) {
      debugPrint(e.toString());
      // errorMessage = 'Failed to download data\nplease try again later or contact us';
      setState(() {
        _isLoading = false;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    getBook();
  }

  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);
    MediaQueryData mediaQD = MediaQuery.of(context);
    return _isLoading ? Scaffold(
      backgroundColor: theme.primaryColor,
      body: SafeArea(
        child: Container(
          child: Center(
            child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(theme.accentColor),
            ),
          ),
        )
      )
    ) : Scaffold(
      backgroundColor: theme.primaryColor,
      bottomNavigationBar: Container(
        color: Colors.grey[200],
        padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
        child: Row(
          children: [
            Expanded(
              child: MaterialButton(
                height: 50,
                minWidth: 50,
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(
                    builder: (BuildContext context) => ReadBook(
                      bookData: detailBook,
                      bookPage: bookpage,
                    ))
                  );
                },
                color: Colors.grey[600],
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    side: BorderSide(color: Colors.grey)),
                child: Text(MyLocalization.of(context).bottomNavigationRead,
                    style: GoogleFonts.quattrocentoSans(
                        fontSize: 18, color: Color(0xFFFCFCFC), fontWeight: FontWeight.bold)),
              ),
            ),
            SizedBox(
              width: 5.0,
            ),
            MaterialButton(
              height: 50,
              minWidth: 50,
              onPressed: () {
                Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (BuildContext context) =>
                                DonationPage()));
              },
              color: theme.primaryColor,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                  side: BorderSide(color: Colors.grey)),
              child: FaIcon(
                FontAwesomeIcons.donate,
                color: Colors.white,
              ),
            ),
          ],
        )
      ),
      body: SafeArea(
        child: Stack(
          alignment: Alignment.topCenter,
          children: <Widget>[
            ListView(
              children: <Widget>[
                Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.only(top: 15.0, bottom: 10.0),
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      image: DecorationImage(
                        image: AssetImage('assets/img/placeholder.png'),
                        fit: BoxFit.cover,
                        alignment: Alignment.center,
                      ),
                    ),
                    height: mediaQD.size.height / 3,
                    width: mediaQD.size.width / 2.2,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10.0),
                      child: CachedNetworkImage(
                        imageUrl: gcsurl + detailBook.thumbnail,
                        placeholder: (context, url) => Container(child: Center(child: CircularProgressIndicator()),),
                        errorWidget: (context, url, error) => Image.asset('assets/img/placeholder.png'),
                        fit: BoxFit.cover,
                        alignment: Alignment.center
                      )
                    )
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 13.0, left: 15.0, right: 15.0),
                  color: Colors.grey[800],
                  child: Text(
                    detailBook.title,
                    softWrap: true,
                    style: GoogleFonts.quattrocentoSans(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                      color: Colors.white
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(left: 15.0, right: 15.0, bottom: 15.0),
                  color: Colors.grey[800],
                  child: Text(
                    'Author: ' + detailBook.author,
                    softWrap: true,
                    style: GoogleFonts.quattrocentoSans(
                      fontSize: 16,
                      color: Colors.white
                    ),
                  ),
                ),
                Container(
                  height: mediaQD.size.height - 400,
                  color: Colors.white,
                  child: Align(
                    alignment: Alignment.center,
                    child: ListView(
                      padding: EdgeInsets.all(13.0),
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(bottom: 30.0),
                          width: mediaQD.size.width / 1.1,
                          child: Text(
                            detailBook.description,
                            overflow: TextOverflow.visible,
                            style: GoogleFonts.quattrocentoSans(
                              fontSize: 16,
                              color: Colors.black54
                            ),
                          ),
                        ),
                      ],
                    )
                  )
                )
              ],
            ),
            Container(
              // height: 60,
              width: mediaQD.size.width / 1.01,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  IconButton(
                    icon: Icon(
                      Platform.isIOS
                        ? Icons.arrow_back_ios
                        : Icons.arrow_back
                    ),
                    color: Colors.white,
                    onPressed: () {
                      Navigator.pop(context);
                    }),
                  Text(
                    '',
                    style: GoogleFonts.quattrocentoSans(
                        fontWeight: FontWeight.bold, fontSize: 20),
                  ),
                  Icon(
                    Icons.ac_unit,
                    color: Colors.transparent,
                  )
                ],
              ),
            ),
          ],
        )
      ),
    );
  }
}
