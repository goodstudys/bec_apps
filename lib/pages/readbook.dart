import 'dart:async';
import 'dart:io';
import 'package:buddhist_educenter/components/widgets/pdfViewer.dart';
import 'package:buddhist_educenter/env.dart';
import 'package:buddhist_educenter/model/bookModelApi.dart';
import 'package:buddhist_educenter/storage/storage.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';

class ReadBook extends StatefulWidget {
  final BookModelApi bookData;
  final int bookPage;
  ReadBook({
    @required this.bookData,
    @required this.bookPage
  });
  @override
  _ReadBookState createState() => _ReadBookState();
}

class _ReadBookState extends State<ReadBook> {
  DataStore store = DataStore();
  String pathPDF = "";
  String landscapePathPdf = "";
  String remotePDFpath = "";
  String corruptedPathPDF = "";
  bool _isLoading = false;
  int halamanBuku;

  @override
  void initState() {
    super.initState();
    /* fromAsset('assets/corrupted.pdf', 'corrupted.pdf').then((f) {
      setState(() {
        corruptedPathPDF = f.path;
      });
    });
    fromAsset('assets/demo-link.pdf', 'demo.pdf').then((f) {
      setState(() {
        pathPDF = f.path;
      });
    });
    fromAsset('assets/demo-landscape.pdf', 'landscape.pdf').then((f) {
      setState(() {
        landscapePathPdf = f.path;
      });
    }); */

    /* createFileOfPdfUrl().then((f) {
      print(f.path);
      setState(() {
        remotePDFpath = f.path;
      });
    }); */
  }

  Future<File> createFileOfPdfUrl() async {
    Completer<File> completer = Completer();
    // print("Start download file from internet!");
    setState(() {
      _isLoading = true;
    });
    try {
      /* SharedPreferences prefs = await SharedPreferences.getInstance();
      var tokenType = await store.getDataString('token_type');
      var accessToken = await store.getDataString('access_token');
      var pointer = await store.getDataString('BookPointer');
      var res = await http.get(url('api/book/' + pointer), headers: {'Authorization': tokenType + ' ' + accessToken});
      var content = json.decode(res.body);
      setState(() {
        detailBook = BookModelApi.fromJson(content['book']);
        halamanBuku = prefs.getInt(detailBook.rowPointer) ?? 1;
      }); */
      setState(() {
        halamanBuku = widget.bookData.rowPointer ?? 1;
      });
      final fileurl = gcsurl + widget.bookData.book;
      final filename = fileurl.substring(fileurl.lastIndexOf("/") + 1);
      var request = await HttpClient().getUrl(Uri.parse(fileurl));
      var response = await request.close();
      var bytes = await consolidateHttpClientResponseBytes(response);
      var dir = await getApplicationDocumentsDirectory();
      File file = File("${dir.path}/$filename");
      await file.writeAsBytes(bytes, flush: true);
      completer.complete(file);
    } catch (e) {
      throw Exception('Error parsing asset file!');
    }
    setState(() {
      _isLoading = false;
    });
    return completer.future;
  }

  Future<File> fromAsset(String asset, String filename) async {
    // To open from assets, you can copy them to the app storage folder, and the access them "locally"
    Completer<File> completer = Completer();

    try {
      var dir = await getApplicationDocumentsDirectory();
      File file = File("${dir.path}/$filename");
      var data = await rootBundle.load(asset);
      var bytes = data.buffer.asUint8List();
      await file.writeAsBytes(bytes, flush: true);
      completer.complete(file);
    } catch (e) {
      throw Exception('Error parsing asset file!');
    }

    return completer.future;
  }

  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(_isLoading ? '...' : widget.bookData.title),
        backgroundColor: theme.primaryColor,
      ),
      body: !_isLoading ? PDFScreen(
        path: gcsurl + widget.bookData.book.replaceAll(gcsurl, ''),
        halaman: widget.bookPage,
        idBuku: widget.bookData.rowPointer,
        bookModel: widget.bookData,
      ) : Container(
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            CircularProgressIndicator(),
            Text('Loading Book'),
          ],
        ),
      ),
    );
  }
}