import 'package:buddhist_educenter/bloc/book/bookBloc.dart';
import 'package:buddhist_educenter/bloc/global/BlocProvider.dart';
import 'package:buddhist_educenter/locale/provider_localization.dart';

class GlobalBloc implements BlocBase {
  BookBloc bookBloc;
  ProviderLocalization providerLocalization;

  GlobalBloc() {
    bookBloc = BookBloc();
    providerLocalization = ProviderLocalization();
  }

  void dispose() {
    bookBloc.dispose();
    providerLocalization.dispose();
  }
}
