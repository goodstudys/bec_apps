import 'dart:async';
import 'package:buddhist_educenter/bloc/book/book.dart';
import 'package:buddhist_educenter/bloc/global/BlocProvider.dart';
import 'package:buddhist_educenter/model/readbookModel.dart';
import 'package:rxdart/rxdart.dart';

class BookBloc implements BlocBase {
  BookData bookData = BookData();

  // Sinks
  Sink<ReadingBookModel> get addItem => itemAddItemController.sink;
  final itemAddItemController = StreamController<ReadingBookModel>();
  Sink<ReadingBookModel> get removereadingbook =>
      itemRemovereadingbookController.sink;
  final itemRemovereadingbookController = StreamController<ReadingBookModel>();
  Sink<String> get search => itemsearchController.sink;
  final itemsearchController = StreamController<String>();
  Sink<dynamic> get addsearchItem => itemAddSearchItemController.sink;
  final itemAddSearchItemController = StreamController<dynamic>();

  Sink<dynamic> get sendBook => itemAddSendBookController.sink;
  final itemAddSendBookController = StreamController<dynamic>();
  Stream<BookData> get bookStream => _book.stream;
  final _book = BehaviorSubject<BookData>();

  BookBloc() {
    itemAddItemController.stream.listen(addBookItem);
    itemRemovereadingbookController.stream.listen(removeBookItem);
    itemAddSendBookController.stream.listen(sendListBook);
    itemsearchController.stream.listen(searchBook);
    itemAddSearchItemController.stream.listen(searchItem);
  }

  loadBooks() {
    bookData.loadBooks();
    _book.add(bookData);
  }

  addBookItem(ReadingBookModel p) {
    bookData.addBookItem(p);
    _book.add(bookData);
  }

  sendListBook(dynamic p) {
    bookData.sendBook(p);
    _book.add(bookData);
  }

  removeBookItem(ReadingBookModel p) {
    bookData.removeBookItem(p);
    _book.add(bookData);
  }

  searchBook(String p) {
    bookData.searchBookItem(p);
    _book.add(bookData);
  }

  searchItem(dynamic p) {
    bookData.searchItemBook(p);
    _book.add(bookData);
  }

  @override
  void dispose() {
    itemAddItemController.close();
    itemRemovereadingbookController.close();
    itemsearchController.close();
    itemAddSearchItemController.close();
    itemAddSendBookController.close();
  }
}
