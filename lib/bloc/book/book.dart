import 'package:buddhist_educenter/env.dart';
import 'package:buddhist_educenter/model/bookList.dart';
import 'package:buddhist_educenter/model/bookModelApi.dart';
import 'package:buddhist_educenter/model/readbookModel.dart';
import 'package:buddhist_educenter/storage/storage.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class BookData {
  List<ReadingBookModel> listContinue = [];
  List<BookModel> listSearch = [];
  String listJson;
  DataStore store = DataStore();

  void sendBook(dynamic a) {
    listContinue = a;
  }

  void saveBooks(String listJson) async {
    String email = await store.getDataString('email') ?? '';
    String phone = await store.getDataString('phone') ?? '';
    String id = 'bookmark-' + email + '-' + phone;
    store.setDataString(id, listJson);
  }

  void removeBooks() async {
    String email = await store.getDataString('email') ?? '';
    String phone = await store.getDataString('phone') ?? '';
    String id = 'bookmark-' + email + '-' + phone;
    store.removeData(id);
  }

  void loadBooks() async {
    String email = await store.getDataString('email') ?? '';
    String phone = await store.getDataString('phone') ?? '';
    String id = 'bookmark-' + email + '-' + phone;
    bool check = await store.checkData(id);
    if (check) {
      var data = await store.getDataString(id);
      var json = jsonDecode(data);
      listContinue = List<ReadingBookModel>.from(json.map((item) => ReadingBookModel.fromJson(item)));
    }
  }

  void addBookItem(ReadingBookModel p) async {
    var id = listContinue.indexWhere((item) => item.id == p.id);
    if (id == -1) {
      listContinue.add(
        ReadingBookModel(
          id: p.id,
          img: p.img,
          booklink: p.booklink,
          description: p.description,
          pageRead: p.pageRead,
          rowPointer: p.rowPointer,
          title: p.title,
          author: p.author,
          totalPage: p.totalPage
        )
      );
    } else {
      debugPrint('bloc update page');
      listContinue[id].pageRead = p.pageRead;
    }
    listJson = jsonEncode(listContinue);
    saveBooks(listJson);
  }

  void removeBookItem(ReadingBookModel p) async {
    listContinue.remove(p);
    if (listContinue == null || listContinue.length == 0) {
      removeBooks();
    } else {
      listJson = jsonEncode(listContinue);
      saveBooks(listJson);
    }
  }

  void searchBookItem(String p) async {
    List<BookModel> dummyListData = List<BookModel>();
    List<BookModelApi> dataBukuApi = List<BookModelApi>();
    var tokenType = await store.getDataString('token_type');
    var accessToken = await store.getDataString('access_token');
    var res = await http.get(url('api/book/search/' + p), headers: {'Authorization': tokenType + ' ' + accessToken});
    var content = json.decode(res.body);
    if (content['data'] != 0) {
      dataBukuApi = List<BookModelApi>.from(content['data'].map((item) => BookModelApi.fromJson(item)));
      dataBukuApi.forEach((item) {
        if (item.title.toLowerCase().contains(p.toLowerCase())) {
          dummyListData.add(BookModel(
            booklink: item.book,
            description: item.description,
            id: item.id,
            img: item.thumbnail,
            title: item.title,
            author: item.author
          ));
        }
      });
    }
    listSearch.clear();
    listSearch.addAll(dummyListData);
  }

  void searchItemBook(dynamic p) {
    if (p == 0) {
      listSearch.clear();
    } else {
      listSearch.clear();
      p.forEach((a) {
        listSearch.add(BookModel(
            booklink: a.book,
            description: a.description,
            id: a.id,
            img: a.thumbnail,
            title: a.title,
            author: a.author,
            pointer: a.rowPointer));
      });
    }
  }
}
