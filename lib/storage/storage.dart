import 'package:shared_preferences/shared_preferences.dart';

class DataStore {
  Future<bool> setDataString(String name, String value) async {
    final SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString('BECApp/$name', value);
    return true;
  }

  Future<String> getDataString(String name) async {
    final SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.getString('BECApp/$name') ?? "Tidak ditemukan";
  }

  Future<bool> setDataInteger(String name, int value) async {
    final SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setInt('BECApp/$name', value);
    return true;
  }

  Future<int> getDataInteger(String name) async {
    final SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.getInt('BECApp/$name') ?? 0;
  }

  Future<bool> setDataBool(String name, bool value) async {
    final SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setBool('BECApp/$name', value);
    return true;
  }

  Future<bool> getDataBool(String name) async {
    final SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.getBool('BECApp/$name') ?? false;
  }
  Future<bool> removeData(String name) async {
    final SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.remove('BECApp/$name');
    return true;
  }
  Future<bool> checkData(String name) async {
    final SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.containsKey('BECApp/$name') ?? false;
  }
  Future<bool> clearData() async {
    final SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.clear();
    return true;
  }

  /* static Future<String> getImageFromPreferences(String name) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return store.getDataString('$name') ?? null;
  }

  static Future<bool> saveImageToPreferences(String name, String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return store.setDataString('BECApp/$name', value);
  }

  static Image imageFromBase64String(String base64String) {
    return Image.memory(
      base64Decode(base64String),
      fit: BoxFit.fill,
    );
  }

  static Uint8List dataFromBase64String(String base64String) {
    return base64Decode(base64String);
  }

  static String base64String(Uint8List data) {
    return base64Encode(data);
  } */
}