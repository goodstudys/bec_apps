import 'dart:convert';
import 'dart:io';
import 'package:buddhist_educenter/env.dart';
import 'package:buddhist_educenter/storage/storage.dart';
import 'package:flutter/material.dart';

class ServiceApi {
  DataStore store = DataStore();
  static ServiceApi _instance = new ServiceApi.internal();
  ServiceApi.internal();
  factory ServiceApi() => _instance;
  final baseUrl = host;

  Future<dynamic> getApi(String url) async {
    var tokenType = await store.getDataString('token_type');
    var accessToken = await store.getDataString('access_token');
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.getUrl(Uri.parse('$baseUrl$url'));
    request.headers.set('Authorization', tokenType + ' ' + accessToken);
    request.headers.set('content-type', 'application/json');
    // request.add(utf8.encode(json.encode(jsonMap)));
    HttpClientResponse response = await request.close();
    debugPrint(response.statusCode.toString());
    var reply = await response.transform(utf8.decoder).join();
    var res = json.decode(reply);
    httpClient.close();
    return res;
  }
}
